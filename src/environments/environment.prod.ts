export const environment = {
  production: true,
    pusher: {
        key: '28a75e9def4522a9931b',
        cluster: 'eu',
    },
    adminRoles: ['Admin', 'Finance', 'Direction'],
    notification: 'L\'application doit être mis à jour et maintenue pour des raison de sécurité. Contactez l\'administration SVP.'
};
