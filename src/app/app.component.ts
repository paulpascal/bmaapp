import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {AuthenticationService, NgTUtilService} from './_services';
import {NotifierService} from 'angular-notifier';

import { environment } from '../environments/environment';

@Component({
    // tslint:disable-next-line
    selector: 'body',
    template: '<router-outlet></router-outlet> <notifier-container></notifier-container>'
})
export class AppComponent implements OnInit {

    private readonly notifier: NotifierService;

    constructor(private router: Router, notifierService: NotifierService,
                private authService: AuthenticationService) {
        this.notifier = notifierService;
    }

    ngOnInit() {
        this.router.events.subscribe((evt) => {
            if (!(evt instanceof NavigationEnd)) {
                return;
            }
            window.scrollTo(0, 0);
        });
    }
}
