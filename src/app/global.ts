import {Injectable} from '@angular/core';

@Injectable()
export class GlobalProvider {
    // public serverUrl = 'http://192.168.1.113:3333';
    public serverUrl = 'http://127.0.0.1:3333';
    // public apiUrl = 'http://192.168.1.113:3333/api/v1';
    public apiUrl = `${this.serverUrl}/api/v1`;


    getRandomId() {
        const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        let text = '';
        for (let i = 0; i < 2; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }

    uploadUrl = () => `${this.apiUrl}/files`;

    uploadedFilePath = () => `${this.serverUrl}/uploads`;
}
