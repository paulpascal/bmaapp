import { Component, Input } from '@angular/core';
import { NAV } from '../../_nav';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../_services';
import {Role, User} from '../../_models';

@Component({
    selector: 'app-dashboard',
    templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent {
    public navItems: any;
    public sidebarMinimized = true;
    private changes: MutationObserver;
    public element: HTMLElement = document.body;
    currentUser: User;

    constructor (
        private router: Router,
        private authService: AuthenticationService
    ) {

        this.changes = new MutationObserver((mutations) => {
            this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
        });

        this.changes.observe(<Element>this.element, {
            attributes: true
        });

        this.authService.currentUser.subscribe(x => this.currentUser = x);
        console.log(this.currentUser);

        if (this.currentUser.roles.length === 1) {
            // Render sidebar menu according to logged user's role
            switch (this.currentUser.roles[0].label) {
                case Role.Accounting: this.navItems = NAV.accounting; break;
                case Role.Admin: this.navItems = NAV.admin; break;
                case Role.Direction: this.navItems = NAV.direction; break;
                case Role.It: this.navItems = NAV.it; break;
                case Role.Operation: this.navItems = NAV.operation; break;
                case Role.Secretariat: this.navItems = NAV.secretariat; break;
            }
        } else {
            const roles = this.currentUser.roles.map(r => r.label);
            if (roles.includes(Role.Accounting) && roles.includes(Role.Operation)) {
                this.navItems = NAV.ops_acc;
            } else if (roles.includes(Role.Accounting) && (roles.includes(Role.InvoiceValidator))) {
                this.navItems = NAV.accounting;
            } else if (roles.includes(Role.Operation) && (roles.includes(Role.FolderCloser) || roles.includes(Role.FolderValidator))) {
                this.navItems = NAV.operation;
            } else if (roles.includes(Role.Admin)) {
                this.navItems = NAV.admin;
            }
        }
    }

    logout() {
        this.authService.logout();
        this.router.navigate(['/login']);
    }

    gotToProfile() {
        this.router.navigate(['/profil']);
    }
}
