import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit-calendar',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    providers: [ToasterService]
})
export class EditCalendarComponent implements OnInit {

    calendarForm: FormGroup;
    calendar: any;
    error = '';
    submitted = false;
    loading = false;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private toasterService: ToasterService, private route: ActivatedRoute, private router: Router) {
    }

    get f() { return this.calendarForm.controls; }

    ngOnInit() {
        this.getClinic(this.route.snapshot.params['id']);
        this.calendarForm = this.formBuilder.group({
            name: ['', Validators.required],
            tel: [''],
            address: [''],
            email: ['']
        });
    }

    getClinic(calendarId) {
        this.apiService.getClinic(calendarId)
            .pipe(first())
            .subscribe((calendar: any[]) => {
                this.calendar = calendar;
                // Setting values
                this.calendarForm.setValue({
                    name: this.calendar.name,
                    tel: this.calendar.tel,
                    dueDateDelay: this.calendar.dueDateDelay,
                    address: this.calendar.address,
                    description: this.calendar.description
                });
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/calendars/list']);
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.calendarForm.invalid) {
            return;
        }
        this.loading = true;
        const calendarData = {
            name: this.f.name.value,
            tel: this.f.tel.value,
            dueDateDelay: this.f.dueDateDelay.value,
            address: this.f.address.value,
            description: this.f.description.value,
        };
        this.apiService.editClinic(this.calendar.id, calendarData)
            .pipe(first())
            .subscribe(calendar => {
                    console.log(calendar);
                    this.error = '';
                    this.loading = false;
                    this.submitted = false;
                    this.showSuccess();
                },
                error => {
                    console.log(error);
                    this.error = error;
                    this.loading = false;
                });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Client modifié avec succès!');
    }
}
