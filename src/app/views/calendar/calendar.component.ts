import {ChangeDetectionStrategy, Component, Input, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {first} from 'rxjs/operators';
import {ApiService} from '../../_services';
import {Router} from '@angular/router';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {
    CalendarDateFormatter,
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarView,
    DAYS_OF_WEEK
} from 'angular-calendar';
import {
    isSameMonth,
    isSameDay,
    startOfMonth,
    endOfMonth,
    startOfWeek,
    endOfWeek,
    startOfDay,
    endOfDay,
    format, subDays, addDays, addHours,
} from 'date-fns';
import {Observable, Subject} from 'rxjs';
import { colors } from './utils/colors';
import {CustomDateFormatter} from './utils/custom-date-formatter.provider';


interface Film {
    id: number;
    title: string;
    release_date: string;
}

function getTimezoneOffsetString(date: Date): string {
    const timezoneOffset = date.getTimezoneOffset();
    const hoursOffset = String(
        Math.floor(Math.abs(timezoneOffset / 60))
    ).padStart(2, '0');
    const minutesOffset = String(Math.abs(timezoneOffset % 60)).padEnd(2, '0');
    const direction = timezoneOffset > 0 ? '-' : '+';

    return `T00:00:00${direction}${hoursOffset}:${minutesOffset}`;
}


@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [ToasterService, {
        provide: CalendarDateFormatter,
        useClass: CustomDateFormatter,
    }]
})
export class CalendarComponent implements OnInit {

    error: any;
    public clinics: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;
    showAddEventModal = false;

    @Input() locale: String = 'fr';

    showCreateClinicModal = false;

    weekStartsOn: number = DAYS_OF_WEEK.MONDAY;

    weekendDays: number[] = [DAYS_OF_WEEK.FRIDAY, DAYS_OF_WEEK.SATURDAY];

    view: CalendarView = CalendarView.Month;

    viewDate: Date = new Date();

    modalData: {
        action: string;
        event: CalendarEvent;
    };

    actions: CalendarEventAction[] = [
        // {
        //     label: 'Modifier <i class="fas fa-fw fa-pencil-alt"></i>  ',
        //     onClick: ({ event }: { event: CalendarEvent }): void => {
        //         this.handleEvent('Edited', event);
        //     },
        // },
        {
            label: '   Supprimer <i class="fas fa-fw fa-trash-alt"></i>',
            onClick: ({ event }: { event: CalendarEvent }): void => {
                this.events = this.events.filter((iEvent) => iEvent !== event);
                this.handleEvent('Deleted', event);
            },
        },
    ];

    refresh: Subject<any> = new Subject();

    events: CalendarEvent[] = [
        // {
        //     start: subDays(startOfDay(new Date()), 1),
        //     end: addDays(new Date(), 1),
        //     title: 'A 3 day event',
        //     color: colors.red,
        //     actions: this.actions,
        //     allDay: true,
        //     resizable: {
        //         beforeStart: true,
        //         afterEnd: true,
        //     },
        //     draggable: true,
        // },
        // {
        //     start: startOfDay(new Date()),
        //     title: 'An event with no end date',
        //     color: colors.yellow,
        //     actions: this.actions,
        // },
        // {
        //     start: subDays(endOfMonth(new Date()), 3),
        //     end: addDays(endOfMonth(new Date()), 3),
        //     title: 'A long event that spans 2 months',
        //     color: colors.blue,
        //     allDay: true,
        // },
        // {
        //     start: addHours(startOfDay(new Date()), 2),
        //     end: addHours(new Date(), 2),
        //     title: 'A draggable and resizable event',
        //     color: colors.yellow,
        //     actions: this.actions,
        //     resizable: {
        //         beforeStart: true,
        //         afterEnd: true,
        //     },
        //     draggable: true,
        // },
    ];

    activeDayIsOpen: Boolean = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private http: HttpClient, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fetchEvents();
    }

    fetchEvents(): void {

        this.apiService.getUserEvents().subscribe((evts: any[]) => {
            this.events = [];
            for (const evt of evts) {
                this.events.push(
                    {
                        start:  startOfDay(new Date(evt.event_start_date)),
                        end:  endOfDay(new Date(evt.event_end_date)),
                        title: evt.title,
                        color: {
                            primary: `${evt.color}`,
                            secondary: `${evt.color}50`,
                        },
                        actions: this.actions,
                        allDay: true,
                        resizable: {
                            beforeStart: true,
                            afterEnd: true,
                        },
                        draggable: false,
                    });
            }

            console.table(this.events);
        });

        const getStart: any = {
            month: startOfMonth,
            week: startOfWeek,
            day: startOfDay,
        }[this.view];

        const getEnd: any = {
            month: endOfMonth,
            week: endOfWeek,
            day: endOfDay,
        }[this.view];

    }


    toggleAddEvent = () => {
        this.showAddEventModal = !this.showAddEventModal;
        return true;
    }

    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
            }
            this.viewDate = date;
        }
    }

    eventTimesChanged({
                          event,
                          newStart,
                          newEnd,
                      }: CalendarEventTimesChangedEvent): void {
        this.events = this.events.map((iEvent) => {
            if (iEvent === event) {
                return {
                    ...event,
                    start: newStart,
                    end: newEnd,
                };
            }
            return iEvent;
        });
        this.handleEvent('Dropped or resized', event);
    }

    handleEvent(action: string, event: CalendarEvent): void {
        this.modalData = { event, action };
        console.log('OPEN modal for: ' + action , event );
    }

    addEvent($event): void {
        this.events = [
            ...this.events,
            {
                title: $event.title,
                start: startOfDay(new Date()),
                end: endOfDay(new Date()),
                color: colors.red,
                draggable: true,
                resizable: {
                    beforeStart: true,
                    afterEnd: true,
                },
            },
        ];
    }

    deleteEvent(eventToDelete: CalendarEvent) {
        this.events = this.events.filter((event) => event !== eventToDelete);
    }

    setView(view: CalendarView) {
        this.view = view;
    }

    closeOpenMonthViewDay() {
        this.activeDayIsOpen = false;
    }


    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

}
