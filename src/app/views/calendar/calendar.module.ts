import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import {CalendarComponent} from './calendar.component';
import {EditCalendarComponent} from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule} from 'angular2-toaster';
import {DataTableModule} from 'angular2-datatable';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {SharedModule} from '../../shared.module';
import {ModalsModule} from '../modals';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import {CalendarUtilsModule} from './utils/module';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {EventFormModule} from '../modals/event/event-form.module';

registerLocaleData(localeFr);

@NgModule({
    declarations: [
        CalendarComponent,
        EditCalendarComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        CalendarRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        BsDatepickerModule.forRoot(),
        DataTableModule,
        SweetAlert2Module,
        CalendarModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory,
        }),
        CalendarUtilsModule,
        EventFormModule,
    ]
})
export class CalendarTaskModule { }
