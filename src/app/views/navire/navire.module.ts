import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavireRoutingModule } from './navire-routing.module';
import {NavireComponent} from './navire.component';
import {EditNavireComponent} from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule} from 'angular2-toaster';
import {DataTableModule} from 'angular2-datatable';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {SharedModule} from '../../shared.module';
import {ModalsModule} from '../modals';

@NgModule({
    declarations: [
        NavireComponent,
        EditNavireComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        NavireRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        DataTableModule,
        SweetAlert2Module,
        ModalsModule
    ]
})
export class NavireModule { }
