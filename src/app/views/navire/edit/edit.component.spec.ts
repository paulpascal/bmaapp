import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditNavireComponent } from './edit.component';

describe('EditNavireComponent', () => {
  let component: EditNavireComponent;
  let fixture: ComponentFixture<EditNavireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditNavireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditNavireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
