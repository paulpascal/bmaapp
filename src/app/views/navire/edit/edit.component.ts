import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    providers: [ToasterService]
})
export class EditNavireComponent implements OnInit {

    shipForm: FormGroup;
    ship: any;
    error = '';
    submitted = false;
    loading = false;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private toasterService: ToasterService, private route: ActivatedRoute, private router: Router) {
    }

    get f() { return this.shipForm.controls; }

    ngOnInit() {
        this.getShip(this.route.snapshot.params['id']);
        this.shipForm = this.formBuilder.group({
            name: ['', Validators.required],
            loa: [''],
            beam: [''],
            email: [''],
            immarsat: [''],
            local_agent: [''],
            dwt: [''],
            nbt: [''],
            breath: [''],
            depth: [''],
            flag: [''],
            call_sign: ['']
        });
    }

    getShip(shipId) {
        this.apiService.getShip(shipId)
            .pipe(first())
            .subscribe((ship: any[]) => {
                this.ship = ship;
                // Setting values
                this.shipForm.setValue({
                    'name': this.ship.name,
                    'loa': this.ship.loa,
                    'beam': this.ship.beam,
                    'email': this.ship.email,
                    'immarsat': this.ship.immarsat,
                    'local_agent': this.ship.local_agent,
                    'dwt': this.ship.dwt,
                    'nbt': this.ship.nbt,
                    'breath': this.ship.breath,
                    'depth': this.ship.depth,
                    'flag': this.ship.flag,
                    'call_sign': this.ship.call_sign,
                });
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/ships/list']);
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.shipForm.invalid) {
            return;
        }
        this.loading = true;
        const shipData = {
            name: this.f.name.value,
            loa: this.f.loa.value,
            beam: this.f.beam.value,
            email: this.f.email.value,
            immarsat: this.f.immarsat.value,
            local_agent: this.f.local_agent.value,
            dwt: this.f.dwt.value,
            nbt: this.f.nbt.value,
            breath: this.f.breath.value,
            depth: this.f.depth.value,
            flag: this.f.flag.value,
            call_sign: this.f.call_sign.value,
        };
        this.apiService.editShip(this.ship.id, shipData)
            .pipe(first())
            .subscribe(ship => {
                    console.log(ship);
                    this.error = '';
                    this.loading = false;
                    this.submitted = false;
                    this.showSuccess();
                },
                error => {
                    console.log(error);
                    this.error = error;
                    this.loading = false;
                });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Navire modifié avec succès!');
    }
}
