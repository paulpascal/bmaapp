import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NavireComponent} from './navire.component';
import {EditNavireComponent} from './edit/edit.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
    },
    {
        path: '', children: [
            { path: 'list', component: NavireComponent },
            { path: 'edit/:id', component: EditNavireComponent },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NavireRoutingModule { }
