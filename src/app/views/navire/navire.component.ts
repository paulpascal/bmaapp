import {Component, OnInit, ViewChild} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {first} from 'rxjs/operators';
import {ApiService} from '../../_services';
import {Router} from '@angular/router';

@Component({
    selector: 'app-navire',
    templateUrl: './navire.component.html',
    styleUrls: ['./navire.component.scss'],
    providers: [ToasterService]
})
export class NavireComponent implements OnInit {

    error: any;
    public ships: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    showCreateShipModal = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fecth();
    }

    public editShip(ship) {
        this.router.navigate([`/ships/edit/${ship.id}`]);
    }

    public confirmDeleteShip(ship) {
        this.deleteSwal.options = {
            title: 'Supprimer cet navire?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteShip(ship);
            }
        };
        this.deleteSwal.show();
    }

    deleteShip(ship) {
        this.apiService.deleteShip(ship.id)
            .subscribe(() => {
                const index = this.ships.indexOf(ship, 0);
                if (index > -1) {
                    this.ships.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Navire supprimé avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    toggleCreateShip = () => {
        this.showCreateShipModal = !this.showCreateShipModal;
    }
    setShip($event) {
        console.log('Ship got: ', $event);
        this.fecth();
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getShips()
            .pipe(first())
            .subscribe(
                (ships: any[]) => {
                    // ..
                    this.ships = ships;
                    this.loading = false;
                }
            );
    }
}
