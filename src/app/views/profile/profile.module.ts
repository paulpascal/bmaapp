import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';
import {ProfileComponent} from './profile.component';
import {TabsModule} from 'ngx-bootstrap';
import {AvatarModule} from 'ngx-avatar';

@NgModule({
    declarations: [
        ProfileComponent
    ],
    imports: [
        CommonModule,
        ProfileRoutingModule,
        TabsModule,
        AvatarModule
    ]
})
export class ProfileModule { }
