import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService, AuthenticationService} from '../../_services';
import {first} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

    folders: any[];
    isAdmin = false;
    user = null;

    constructor(private apiProvider: ApiService, private authService: AuthenticationService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        if (this.route.snapshot.params['id']) {
            this.isAdmin = true;
            this.apiProvider.getUser(this.route.snapshot.params['id'])
                .pipe(first())
                .subscribe((user: any[]) => {
                   this.user = user;
                });
            this.apiProvider
                .getFoldersForUser(this.route.snapshot.params['id'])
                .pipe(first())
                .subscribe((folders: any[]) => {
                    this.folders = folders;
                });
        } else {
            this.user = this.authService.currentUserObject;
            this.apiProvider
                .getFoldersForUser(this.authService.currentUserObject.id)
                .pipe(first())
                .subscribe((folders: any[]) => {
                    this.folders = folders;
                });
        }
    }

}
