import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui-pro/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import {ApiService, NgTUtilService} from '../../_services';
import {first} from 'rxjs/operators';

@Component({
  templateUrl: 'dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
    nbFolders;
    nbShips;
    nbCustomers;
    nbInvestmentSheet;
    nbCheckoutSheet;
    date = new Date();

    constructor (private apiService: ApiService) {
        this.apiService.getCheckoutSheets()
            .pipe(first())
            .subscribe((sheets: any[]) => {
                this.nbCheckoutSheet = sheets.length;
            });
        this.apiService.getInvestementSheets()
            .pipe(first())
            .subscribe((sheets: any[]) => {
                this.nbInvestmentSheet = sheets.length;
            });
        this.apiService.getFolders()
            .pipe(first())
            .subscribe((folders: any[]) => {
                this.nbFolders = folders.length;
            });
        this.apiService.getShips()
            .pipe(first())
            .subscribe((ships: any[]) => {
                this.nbShips = ships.length;
            });
        this.apiService.getCustomers()
            .pipe(first())
            .subscribe((cust: any[]) => {
                this.nbCustomers = cust.length;
            });
    }

    ngOnInit(): void {
    }

    ngOnDestroy() {
    }
}
