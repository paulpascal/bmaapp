import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-select-customer-modal',
  templateUrl: './select-customer.component.html',
  styleUrls: ['./select-customer.component.scss']
})
export class SelectCustomerComponent implements OnInit {

    loading = false;
    submitted = false;
    customers: any[];
    customerForm: FormGroup;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Output() onSelect = new EventEmitter<any>();

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);

    constructor( private formBuilder: FormBuilder, private apiService: ApiService) { }

    ngOnInit() {
        this.apiService.getCustomers()
            .pipe(first())
            .subscribe((customers: any[]) => {
                this.customers = customers;
            });

        this.customerForm = this.formBuilder.group({
            customer: [null, Validators.required]
        });
    }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.customerForm.invalid) {
            return ;
        }

        this.onSelect.emit(this.f.customer.value);
        this.closeCallback();
    }

    get f() { return this.customerForm.controls; }

}
