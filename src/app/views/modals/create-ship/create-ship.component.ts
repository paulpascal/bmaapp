import {
    AfterViewChecked, AfterViewInit,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChange,
    SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {first} from 'rxjs/operators';
import {ApiService} from '../../../_services';

@Component({
    selector: 'app-create-ship-modal',
    templateUrl: './create-ship.component.html',
    styleUrls: ['./create-ship.component.scss']
})
export class CreateShipComponent implements OnInit {

    shipForm: FormGroup;
    error = false;
    submitted = false;
    loading = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Output() onCreate = new EventEmitter<any>();

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.shipForm = this.formBuilder.group({
            name: ['', Validators.required],
            loa: [''],
            beam: [''],
            email: [''],
            immarsat: [''],
            local_agent: [''],
            dwt: [''],
            nbt: [''],
            breath: [''],
            depth: [''],
            flag: [''],
            call_sign: ['']
        });
    }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.shipForm.invalid) {
            return ;
        }

        this.loading = true;
        const shipData = {
            name: this.f.name.value,
            loa: this.f.loa.value,
            beam: this.f.beam.value,
            email: this.f.email.value,
            immarsat: this.f.immarsat.value,
            local_agent: this.f.local_agent.value,
            dwt: this.f.dwt.value,
            nbt: this.f.nbt.value,
            breath: this.f.breath.value,
            depth: this.f.depth.value,
            flag: this.f.flag.value,
            call_sign: this.f.call_sign.value,
        };
        this.apiProvider.saveShip(shipData)
            .pipe(first())
            .subscribe(ship => {
                    console.log(ship);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Navire ajouté avec succès!');
                    this.error = null;
                    this.shipForm.reset();
                    this.onCreate.emit(ship.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    get f() { return this.shipForm.controls; }
}
