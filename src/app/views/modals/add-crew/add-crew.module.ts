import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddCrewComponent } from './add-crew.component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {LaddaModule} from 'angular2-ladda';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {HttpClientModule} from '@angular/common/http';

import {CountryPickerModule, CountryPickerService} from 'ngx-country-picker';

@NgModule({
    declarations: [AddCrewComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot(),
        NgSelectModule,
        ToasterModule,
        LaddaModule,
        HttpClientModule,
        CountryPickerModule.forRoot({
            filename: '',
            baseUrl: 'assets/'
        }),
    ],
    exports: [AddCrewComponent],
    providers: [
        ToasterService,
        CountryPickerService
    ]
})
export class AddCrewModule { }
