import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
declare const $;

@Component({
  selector: 'app-add-crew',
  templateUrl: './add-crew.component.html',
  styleUrls: ['./add-crew.component.scss']
})
export class AddCrewComponent implements OnInit {

    // tslint:disable-next-line:max-line-length
    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    get f() { return this.crewForm.controls; }

    crewForm: FormGroup;
    error = false;
    submitted = false;
    loading = false;
    crewRanks: any[];

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() folderId = null;
    @Input() customClass = '';

    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);

    ngOnInit() {
        console.log('PASSS' , this.folderId);

        $('#nationality').countrySelect({
            responsiveDropdown: true
        });

        $('#nationality').on('change', () => {
            this.onNationalitySelected();
        });

        this.apiProvider.getCrewTypes()
            .pipe(first())
            .subscribe((ranks: any[]) => {
                this.crewRanks = ranks;
            });

        this.crewForm = this.formBuilder.group({
            name: ['', Validators.required],
            rank: [null, Validators.required],
            nationality: [null, Validators.required],
            arrivalDate: [null, ],
            scheduledDepartureDate: [null, ],
            onSigner: [false],
            visa: [false],
            oktb: [false],
        });
    }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.crewForm.invalid) {
            return ;
        }

        this.loading = true;
        const crewData = {
            name: this.f.name.value,
            crew_type_id: this.f.rank.value,
            nationality: this.f.nationality.value,
            arrivalDate: this.f.arrivalDate.value,
            scheduledDepartureDate: this.f.scheduledDepartureDate.value,
            onSigner: this.f.onSigner.value,
            visa: this.f.visa.value,
            oktb: this.f.oktb.value,
            folder_id: this.folderId,
        };
        console.log(crewData);

        this.apiProvider.saveCrew(crewData)
            .pipe(first())
            .subscribe(crew => {
                    console.log(crew);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Crew ajouté avec succès!');
                    this.error = null;
                    this.crewForm.reset();
                    this.onCreate.emit(crew.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    onNationalitySelected() {
        const choice = $('#nationality').countrySelect('getSelectedCountryData');
        if (choice) {
            this.crewForm.controls['nationality'].setValue(choice.name);
        }
    }
}
