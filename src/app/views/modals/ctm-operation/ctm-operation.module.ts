import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CtmOperationComponent } from './ctm-operation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterService} from 'angular2-toaster';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {FileUploadModule} from 'ng2-file-upload';
import {AddVesselModule} from '../add-vessel/add-vessel.module';

@NgModule({
  declarations: [CtmOperationComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        LaddaModule,
        NgSelectModule,
        BsDatepickerModule,
        FileUploadModule,
        AddVesselModule,
    ], exports: [CtmOperationComponent], providers: [
        ToasterService
    ]
})
export class CtmOperationModule { }
