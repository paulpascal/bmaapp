import {
    AfterContentChecked,
    AfterContentInit, AfterViewChecked,
    AfterViewInit,
    Component, DoCheck,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';
import {GlobalProvider} from '../../../global';

@Component({
  selector: 'app-ctm-operation-modal',
  templateUrl: './ctm-operation.component.html',
  styleUrls: ['./ctm-operation.component.scss']
})
export class CtmOperationComponent implements OnInit {

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService,
    private global: GlobalProvider, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    get f() {
        return this.ctmOperationForm.controls;
    }

    error = false;
    submitted = false;
    loading = false;
    ctmOperationForm: FormGroup;


    vessels: any[];
    uploader: FileUploader;
    uploadUrl;
    response;

    showAddVesselModal = false;



    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() folderId = null;

    @Output() onClose: EventEmitter<boolean> = new EventEmitter();
    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);

    toggleAddVessel = () => {
        this.showAddVesselModal = !this.showAddVesselModal;
    }

    ngOnInit() {


        console.log(
            `FOLDER ID ${this.folderId} - GB`
        );

        this.uploader =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });

         this.uploader.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            this.ctmOperationForm.controls.deliverReceipt.setValue(files[0]['id']);
            console.log(this.ctmOperationForm.controls);
        } );

         this.fetchVessels();

        this.ctmOperationForm = this.formBuilder.group({
            vessel: [null, Validators.required],
            amount: [null, Validators.required],
            distance: [null, Validators.required],
            deliverReceipt: [],
        });
    }

    fetchVessels() {
        this.apiProvider.getVessels().subscribe((data: any[]) => {
            this.vessels = data;
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.ctmOperationForm.invalid) {
            return ;
        }

        this.loading = true;
        const ctmData = {
            boat_id: this.f.vessel.value,
            amount: this.f.amount.value,
            folder_id: this.folderId,
            deliver_receipt_file_id: this.f.deliverReceipt.value,
        };

        console.log(ctmData);

        this.apiProvider.saveCTM(ctmData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Operation de ctm ajouté au dossier avec succès!');
                    this.error = null;
                    this.ctmOperationForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    addVessel($event) {}

    closeModal() {
        this.onClose.emit(true);
    }
}
