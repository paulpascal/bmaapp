import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-clinic-modal',
  templateUrl: './add-clinic.component.html',
  styleUrls: ['./add-clinic.component.scss']
})
export class AddClinicComponent implements OnInit {

    clinicForm: FormGroup;
    error = false;
    submitted = false;
    loading = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);

    @Output() onCreate = new EventEmitter<any>();

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.clinicForm = this.formBuilder.group({
            name: ['', Validators.required],
            tel: [null, Validators.required],
            address: [null, Validators.required],
            email: [null, Validators.required],
        });
    }

    get f () { return this.clinicForm.controls; }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.clinicForm.invalid) {
            return ;
        }

        this.loading = true;
        const clinicData = {
            name: this.f.name.value,
            phone: this.f.tel.value,
            address: this.f.address.value,
            email: this.f.email.value,
        };
        console.log(clinicData);

        this.apiProvider.saveClinic(clinicData)
            .pipe(first())
            .subscribe(clinic => {
                    console.log(clinic);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Clinique ajouté avec succès!');
                    this.error = null;
                    this.clinicForm.reset();
                    this.onCreate.emit(clinic.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }
}
