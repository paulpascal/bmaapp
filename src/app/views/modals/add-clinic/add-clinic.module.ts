import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddClinicComponent } from './add-clinic.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {ToasterModule, ToasterService} from 'angular2-toaster';

@NgModule({
  declarations: [AddClinicComponent],
  imports: [
    CommonModule,
      FormsModule,
      ReactiveFormsModule,
      LaddaModule,
      ToasterModule
  ], exports: [AddClinicComponent], providers: [
      ToasterService
    ]
})
export class AddClinicModule { }
