import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddVesselComponent } from './add-vessel.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {LaddaModule} from 'angular2-ladda';

@NgModule({
  declarations: [AddVesselComponent],
  imports: [
    CommonModule,
      FormsModule,
      ReactiveFormsModule,
      NgSelectModule,
      ToasterModule,
      LaddaModule
  ], exports: [AddVesselComponent], providers: [
      ToasterService
    ]
})
export class AddVesselModule { }
