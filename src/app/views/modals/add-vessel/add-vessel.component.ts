import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../_services';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {first} from 'rxjs/operators';

@Component({
    selector: 'app-add-vessel',
    templateUrl: './add-vessel.component.html',
    styleUrls: ['./add-vessel.component.scss']
})
export class AddVesselComponent implements OnInit {

    @Input() show = false;
    @Input() folderId = null;
    @Input() customClass = '';

    existingVessels: any[];
    addVessel: any;
    assignVesselForm: FormGroup;
    newVesselForm: FormGroup;
    error: any;
    submitted = false;
    vesselTypes: any;
    selectForm = false;
    loading = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);


    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }


    ngOnInit() {
        this.fetchVesselTypes();

        this.assignVesselForm = this.formBuilder.group({
            vessel: [null, Validators.required],
        });

        this.newVesselForm = this.formBuilder.group({
            name: [null, Validators.required],
            type: [null, Validators.required],
            contentCapacity: [null, Validators.required],
            crewsCapacity: [null, Validators.required],
            isBoatRentByBma: [true, ],
            details: [null, ],
        });
    }

    fetchVesselTypes() {
        this.apiProvider.getVesselTypes().subscribe((data: any[]) => {
            this.vesselTypes = data;
        });
    }

    onSubmit() {

        console.log(this.f);

        this.submitted = true;
        if (this.newVesselForm.invalid) {
            return ;
        }

        this.loading = true;
        const vesselData = {
            name: this.f.name.value,
            boat_type_id: this.f.type.value,
            contentCapacity: this.f.contentCapacity.value,
            crewsCapacity: this.f.crewsCapacity.value,
            isBoatRentByBma: this.f.isBoatRentByBma.value,
            details: this.f.details.value,
            folder_id: this.folderId,
        };
        console.log(vesselData);

        this.apiProvider.saveVessel(vesselData)
            .pipe(first())
            .subscribe(vessel => {
                    console.log(vessel);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Boat ajouté avec succès!');
                    this.error = null;
                    this.newVesselForm.reset();
                    this.onCreate.emit(vessel.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    get f() {
        return this.newVesselForm.controls;
    }

    get g() {
        return this.assignVesselForm.controls;
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    toogleForm(select) {
        this.selectForm = select;
    }
}
