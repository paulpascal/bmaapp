import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisaOperationComponent } from './visa-operation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterService} from 'angular2-toaster';
import {LaddaModule} from 'angular2-ladda';

@NgModule({
  declarations: [VisaOperationComponent],
  imports: [
    CommonModule,
      NgSelectModule,
      FormsModule,
      ReactiveFormsModule,
      LaddaModule
  ], exports: [VisaOperationComponent], providers: [
        ToasterService
    ]
})
export class VisaOperationModule { }
