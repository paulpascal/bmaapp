import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

@Component({
  selector: 'app-visa-operation',
  templateUrl: './visa-operation.component.html',
  styleUrls: ['./visa-operation.component.scss']
})
export class VisaOperationComponent implements OnInit {

    loading =  false;
    submitted =  false;
    error =  false;
    selectAll = false;
    crewsSelectForm: FormGroup;

    availableCrews: any[];
    selectedCrews: any[];

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);
    @Input() folderId = null;
    @Input() crewChangeId = null;
    @Output() onCreate = new EventEmitter<any>();

    constructor(private apiProvider: ApiService, private formBuilder: FormBuilder, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.fetchFolderCrews();
        this.crewsSelectForm = this.formBuilder.group({
            'selectedCrews': [null, Validators.compose([Validators.required])],
        });
    }

    fetchFolderCrews() {
        this.apiProvider.getFolderCrews(this.folderId).subscribe((crews: any[]) => {
            this.availableCrews = crews;
        });
    }

    get f() {
        return this.crewsSelectForm.controls;
    }

    toogleSelectAll() {
        this.selectAll = !this.selectAll;
        if (this.selectAll) {
            this.crewsSelectForm.controls['selectedCrews'].setValue([...this.availableCrews.map(c => c['id'])]);
        }
        console.log(this.f);
    }

    onSubmit() {
        this.submitted = true;

        if (!this.f.selectedCrews.value) {
            return ;
        }

        this.loading = true;
        const oktbData = {
            crews: this.f.selectedCrews.value,
            folder_id: this.folderId,
            crew_change_operation_id: this.crewChangeId,
        };

        console.log(oktbData);

        this.apiProvider.saveVisa(oktbData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Visa ajouté au dossier avec succès!');
                    this.error = null;
                    this.crewsSelectForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

}
