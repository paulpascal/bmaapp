import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisaOperationComponent } from './visa-operation.component';

describe('VisaOperationComponent', () => {
  let component: VisaOperationComponent;
  let fixture: ComponentFixture<VisaOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisaOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisaOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
