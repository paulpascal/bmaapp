import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {LandingOperationModalComponent } from './landing-operation-modal.component';

describe('LandingOperationModalComponent', () => {
  let component: LandingOperationModalComponent;
  let fixture: ComponentFixture<LandingOperationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingOperationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingOperationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
