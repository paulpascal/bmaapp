import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisembarkOperationComponent } from './transport-operation.component';

describe('TransportOperationComponent', () => {
  let component: DisembarkOperationComponent;
  let fixture: ComponentFixture<DisembarkOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisembarkOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisembarkOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
