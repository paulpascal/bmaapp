import {
    AfterContentChecked,
    AfterContentInit, AfterViewChecked,
    AfterViewInit,
    Component, DoCheck,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import {GlobalProvider} from '../../../global';
import {FileUploader} from 'ng2-file-upload';

@Component({
  selector: 'app-disembark-operation',
  templateUrl: './disembark-operation.component.html',
  styleUrls: ['./disembark-operation.component.scss']
})
export class DisembarkOperationComponent implements OnInit,
    // OnChanges,
    OnChanges {

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService,
                 private global: GlobalProvider, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    get f() {
        return this.disembarkationOperationForm.controls;
    }

    error = false;
    submitted = false;
    loading = false;
    disembarkationOperationForm: FormGroup;

    availableCrews: any[];

    vessels: any[];
    ships: any[];
    selectedCrews: any[];
    selectedVessels: any[];


    uploader: FileUploader;
    uploadUrl;
    response;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });


    @Input() show = false;
    @Input() customClass = '';
    @Input() folderId = null;
    @Input() crewChangeId = null;

    @Output() onClose: EventEmitter<boolean> = new EventEmitter();
    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);

    ngOnInit() {
        console.log(
            `FOLDER ID ${this.folderId} - CROP ID ${this.crewChangeId} - TRANSPORT`
        );

        this.fetchShips();
        this.fetchAvalaibleCrews();
        this.fetchVessels();

        this.uploader =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });
        this.uploader.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            this.disembarkationOperationForm.controls.boatNote.setValue(files[0]['id']);
            // console.log(this.deliveryOperationForm.controls);
        } );


        this.disembarkationOperationForm = this.formBuilder.group({
            crews: [null, Validators.required],
            arrangedBoatId: [null, Validators.required],
            departure_ship: [null, Validators.required],
            distance: [],
            boatNote: [],
            description: [],
            isBoatSpecial: [false, ]
        });



    }

    ngOnChanges(changes: SimpleChanges) {
        // // tslint:disable-next-line:forin
        // for (const key in changes) {
        //     console.log(`${key} changed.
        //     Current: ${changes[key].currentValue}.
        //     Previous: ${changes[key].previousValue}`);
        // }
        if (changes['show'].currentValue) {
            this.fetchShips();
            this.fetchAvalaibleCrews();
            this.fetchVessels();
        }
    }

    fetchVessels() {
        this.apiProvider.getVessels().subscribe((data: any[]) => {
            this.vessels = data;
            console.log('vessel', this.vessels);
        });
    }

    fetchAvalaibleCrews() {
        this.apiProvider.getFolderCrews(this.folderId).subscribe((data: any[]) => {
            this.availableCrews = data;
        });
    }

    fetchShips() {
        this.apiProvider.getShips()
            .subscribe((data: any[]) => {
                this.ships = data;
            });
    }


    onSubmit() {
        this.submitted = true;

        if (this.disembarkationOperationForm.invalid) {
            return ;
        }

        this.loading = true;
        const disembarkationData = {
            departure_ship_id: this.f.departure_ship.value,
            crews: this.f.crews.value,
            boat_id: this.f.arrangedBoatId.value,
            isBoatSpecial: this.f.isBoatSpecial.value,
            description: this.f.description.value,
            folder_id: this.folderId,
            distance: this.f.distance.value,
            crew_change_operation_id: this.crewChangeId,
            boat_note_file_id: this.f.boatNote.value,

        };

        console.log(disembarkationData);

        this.apiProvider.saveDisembarkation(disembarkationData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Désembarquement ajouté au dossier avec succès!');
                    this.error = null;
                    this.disembarkationOperationForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    closeModal() {
        this.onClose.emit(true);
    }
}
