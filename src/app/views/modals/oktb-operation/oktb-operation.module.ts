import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OktbOperationComponent } from './oktb-operation.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {LaddaModule} from 'angular2-ladda';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterService} from 'angular2-toaster';
import {FileUploadModule} from 'ng2-file-upload';

@NgModule({
  declarations: [OktbOperationComponent],
    imports: [
        CommonModule,
        NgSelectModule,
        LaddaModule,
        ReactiveFormsModule,
        FormsModule,
        FileUploadModule,
    ], exports: [OktbOperationComponent], providers: [
        ToasterService
    ]
})
export class OktbOperationModule { }
