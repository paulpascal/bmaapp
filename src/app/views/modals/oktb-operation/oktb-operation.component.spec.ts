import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OktbOperationComponent } from './oktb-operation.component';

describe('OktbOperationComponent', () => {
  let component: OktbOperationComponent;
  let fixture: ComponentFixture<OktbOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OktbOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OktbOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
