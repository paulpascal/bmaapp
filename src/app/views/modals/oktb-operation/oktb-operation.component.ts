import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {FileUploader} from 'ng2-file-upload';
import {GlobalProvider} from '../../../global';

@Component({
    selector: 'app-oktb-operation',
    templateUrl: './oktb-operation.component.html',
    styleUrls: ['./oktb-operation.component.scss']
})
export class OktbOperationComponent implements OnInit {

    loading =  false;
    submitted =  false;
    error =  false;
    selectAll = false;
    crewsSelectForm: FormGroup;

    availableCrews: any[];
    selectedCrews: any[];

    uploader1: FileUploader;
    uploader2: FileUploader;
    uploadUrl;
    response;



    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() folderId = null;
    @Input() crewChangeId = null;
    @Input() closeCallback = () => (false);
    @Output() onCreate = new EventEmitter<any>();

    constructor(private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService,
                private global: GlobalProvider, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.fetchFolderCrews();
        this.crewsSelectForm = this.formBuilder.group({
            'selectedCrews': [null, Validators.compose([Validators.required])],
        });
        this.uploader1 =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });
        this.uploader1.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            // this.deliveryOperationForm.controls.delieverReceipt.setValue(files[0]['id']);
            // console.log(this.deliveryOperationForm.controls);
        } );
        this.uploader2 =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });
        this.uploader2.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            // this.deliveryOperationForm.controls.delieverReceipt.setValue(files[0]['id']);
            // console.log(this.deliveryOperationForm.controls);
        } );
    }

    fetchFolderCrews() {
        this.apiProvider.getFolderCrews(this.folderId).subscribe((crews: any[]) => {
            this.availableCrews = crews;
        });
    }

    get f() {
        return this.crewsSelectForm.controls;
    }

    toogleSelectAll() {
        this.selectAll = !this.selectAll;
        if (this.selectAll) {
            this.crewsSelectForm.controls['selectedCrews'].setValue([...this.availableCrews.map(c => c['id'])]);
        }
        console.log(this.f);
    }

    onSubmit() {
        this.submitted = true;

        if (!this.f.selectedCrews.value) {
            return ;
        }

        this.loading = true;
        const oktbData = {
            crews: this.f.selectedCrews.value,
            folder_id: this.folderId,
            crew_change_operation_id: this.crewChangeId,
        };

        console.log(oktbData);

        this.apiProvider.saveOktb(oktbData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'OKTB ajouté au dossier avec succès!');
                    this.error = null;
                    this.crewsSelectForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

}
