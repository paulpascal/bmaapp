import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {first} from 'rxjs/operators';
import {ApiService} from '../../../_services';

@Component({
  selector: 'app-create-customer-modal',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent implements OnInit {

    customerForm: FormGroup;
    error = false;
    submitted = false;
    loading = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Output() onCreate = new EventEmitter<any>();

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.customerForm = this.formBuilder.group({
            name: ['', Validators.required],
            tel: [''],
            dueDateDelay: ['2'],
            address: [''],
            description: ['']
        });
    }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.customerForm.invalid) {
            return ;
        }

        this.loading = true;
        const customerData = {
            name: this.f.name.value,
            address: this.f.address.value,
            tel: this.f.tel.value,
            description: this.f.description.value,
            dueDateDelay: this.f.dueDateDelay.value,
        };
        this.apiProvider.saveCustomer(customerData)
            .pipe(first())
            .subscribe(customer => {
                    console.log(customer);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Client ajouté avec succès!');
                    this.error = null;
                    this.customerForm.reset();
                    this.onCreate.emit(customer.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    get f() { return this.customerForm.controls; }
}
