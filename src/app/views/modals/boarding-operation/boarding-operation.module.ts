import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BoardingOperationComponent } from './boarding-operation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterService} from 'angular2-toaster';
import {FileUploadModule} from 'ng2-file-upload';

@NgModule({
  declarations: [BoardingOperationComponent],
  imports: [
    CommonModule,
      FormsModule,
      ReactiveFormsModule,
      LaddaModule,
      NgSelectModule,
      FileUploadModule,

  ], exports: [BoardingOperationComponent], providers: [
        ToasterService
    ]
})
export class BoardingOperationModule { }
