import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardingOperationComponent } from './boarding-operation.component';

describe('BoardingOperationComponent', () => {
  let component: BoardingOperationComponent;
  let fixture: ComponentFixture<BoardingOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardingOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardingOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
