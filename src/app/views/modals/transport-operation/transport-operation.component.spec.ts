import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportOperationComponent } from './transport-operation.component';

describe('TransportOperationComponent', () => {
  let component: TransportOperationComponent;
  let fixture: ComponentFixture<TransportOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
