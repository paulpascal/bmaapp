import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransportOperationComponent } from './transport-operation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterService} from 'angular2-toaster';

@NgModule({
  declarations: [TransportOperationComponent],
  imports: [
    CommonModule,
      FormsModule,
      ReactiveFormsModule,
      LaddaModule,
      NgSelectModule,
  ], exports: [TransportOperationComponent], providers: [
        ToasterService
    ]
})
export class TransportOperationModule { }
