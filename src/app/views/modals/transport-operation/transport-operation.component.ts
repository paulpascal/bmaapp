import {
    AfterContentChecked,
    AfterContentInit, AfterViewChecked,
    AfterViewInit,
    Component, DoCheck,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-transport-operation',
  templateUrl: './transport-operation.component.html',
  styleUrls: ['./transport-operation.component.scss']
})
export class TransportOperationComponent implements OnInit,
    // OnChanges,
    OnChanges {

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    get f() {
        return this.transportOperationForm.controls;
    }

    error = false;
    submitted = false;
    loading = false;
    transportOperationForm: FormGroup;

    availableDrivers: any[];
    availableCrews: any[];
    availableHotels: any[];
    availableClinics: any[];

    vessels: any[];
    carriers: any[];
    transportTypes: any[];
    selectedDrivers: any[];
    selectedCrews: any[];
    selectedVessels: any[];

    showClinics = false;
    showHotels = false;
    showShips = false;


    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });


    @Input() show = false;
    @Input() customClass = '';
    @Input() folderId = null;
    @Input() crewChangeId = null;

    @Output() onClose: EventEmitter<boolean> = new EventEmitter();
    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);

    ngOnInit() {
        console.log(
            `FOLDER ID ${this.folderId} - CROP ID ${this.crewChangeId} - TRANSPORT`
        );

        this.fetchTransportTypes();
        this.fetchTransportCarrier();
        this.fetchDrivers();
        this.fetchHotels();
        this.fetchClinics();
        this.fetchAvalaibleCrews();
        this.fetchVessels();


        this.transportOperationForm = this.formBuilder.group({
            crews: [null, Validators.required],
            drivers: [null, Validators.required],
            fromShip: [null, Validators.required],
            toShip: [null, Validators.required],
            arrangedBoatId: [null, Validators.required],
            transport: [null, Validators.required],
            hotel: [],
            clinic: [],
            description: [],
            carrier: [null, Validators.required],
            isBoatSpecial: [false, ]
        });


    }

    ngOnChanges(changes: SimpleChanges) {
        // // tslint:disable-next-line:forin
        // for (const key in changes) {
        //     console.log(`${key} changed.
        //     Current: ${changes[key].currentValue}.
        //     Previous: ${changes[key].previousValue}`);
        // }
        if (changes['show'].currentValue) {
            this.fetchTransportTypes();
            this.fetchTransportCarrier();
            this.fetchDrivers();
            this.fetchHotels();
            this.fetchClinics();
            this.fetchAvalaibleCrews();
            this.fetchVessels();
        }
    }


    fetchVessels() {
        this.apiProvider.getVessels().subscribe((data: any[]) => {
            this.vessels = data;
        });
    }

    fetchAvalaibleCrews() {
        this.apiProvider.getFolderCrews(this.folderId).subscribe((data: any[]) => {
            this.availableCrews = data;
        });
    }

    fetchTransportTypes() {
        this.apiProvider.getTransportTypes()
            .subscribe((data: any[]) => {
            this.transportTypes = data;
        });
    }

    fetchHotels() {
        this.apiProvider.getHotels()
            .subscribe((hotels: any[]) => {
                this.availableHotels = hotels;
            });
    }

    fetchClinics() {
        this.apiProvider.getClinics()
            .subscribe((hotels: any[]) => {
                this.availableClinics = hotels;
            });
    }

    fetchDrivers() {
        this.apiProvider.getDrivers()
            .subscribe((ranks: any[]) => {
                this.availableDrivers = ranks;
            });
    }

    fetchTransportCarrier() {
        this.apiProvider.getTransportCarriers()
            .subscribe((data: any[]) => {
            this.carriers = data;
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.transportOperationForm.invalid) {
            return ;
        }

        this.loading = true;
        const transportData = {
            transport: this.f.transport.value.id,
            fromShip: this.f.fromShip,
            toShip: this.f.toShip,
            crews: this.f.crews.value,
            arranged_boat_id: this.f.arrangedBoatId.value,
            isBoatSpecial: this.f.isBoatSpecial.value,
            carrier: this.f.carrier.value,
            drivers: this.f.drivers.value,
            hotel: this.f.hotel.value,
            clinic: this.f.clinic.value,
            description: this.f.description.value,
            folder_id: this.folderId,
            crew_change_operation_id: this.crewChangeId,
        };

        console.log(transportData);

        this.apiProvider.saveTransport(transportData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Transport ajouté au dossier avec succès!');
                    this.error = null;
                    this.transportOperationForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    transportTypeChanged() {
        console.log(this.f.transport.value);
        if (this.f.transport.value) {
            const transportType = this.f.transport.value.name;

            if (transportType) {
                this.showClinics = !!transportType.includes('HOSPITAL');
                //
                this.showHotels = !!transportType.includes('HOTEL');
                //
                this.showShips = !!transportType.includes('SHIP');
            }
        } else {
            this.showHotels = false;
            this.showClinics = false;
            this.showShips = false;
        }
    }

    closeModal() {
        this.onClose.emit(true);
    }
}
