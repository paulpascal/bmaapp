import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-add-hotel-modal',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.scss']
})
export class AddHotelComponent implements OnInit {

    hotelForm: FormGroup;
    error = false;
    submitted = false;
    loading = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);

    @Output() onCreate = new EventEmitter<any>();

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.hotelForm = this.formBuilder.group({
            name: ['', Validators.required],
            tel: [null, Validators.required],
            address: [null, Validators.required],
            email: [null, Validators.required],
        });
    }

    get f () { return this.hotelForm.controls; }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.hotelForm.invalid) {
            return ;
        }

        this.loading = true;
        const hotelData = {
            name: this.f.name.value,
            phone: this.f.tel.value,
            address: this.f.address.value,
            email: this.f.email.value,
        };
        console.log(hotelData);

        this.apiProvider.saveHotel(hotelData)
            .pipe(first())
            .subscribe(hotel => {
                    console.log(hotel);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Hotel ajouté avec succès!');
                    this.error = null;
                    this.hotelForm.reset();
                    this.onCreate.emit(hotel.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }
}
