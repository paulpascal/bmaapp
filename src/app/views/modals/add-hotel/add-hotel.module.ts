import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddHotelComponent } from './add-hotel.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {ToasterModule, ToasterService} from 'angular2-toaster';

@NgModule({
  declarations: [AddHotelComponent],
  imports: [
    CommonModule,
      FormsModule,
      ReactiveFormsModule,
      LaddaModule,
      ToasterModule
  ], exports: [AddHotelComponent], providers: [
      ToasterService
    ]
})
export class AddHotelModule { }
