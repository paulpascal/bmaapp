import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventFormComponent } from './event-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';

@NgModule({
    declarations: [EventFormComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BsDropdownModule,
        BsDatepickerModule.forRoot(),
        LaddaModule,
        ToasterModule
    ], exports: [EventFormComponent], providers: [
        ToasterService
    ]
})
export class EventFormModule { }
