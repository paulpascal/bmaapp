import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-event-form-modal',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

    eventForm: FormGroup;
    error = false;
    submitted = false;
    loading = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);

    @Output() onCreate = new EventEmitter<any>();

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.eventForm = this.formBuilder.group({
            title: ['', Validators.required],
            description: ['', Validators.required],
            event_start_date: [null, Validators.required],
            event_end_date: [null, Validators.required],
            event_start_time: [null, Validators.required],
            event_end_time: [null, Validators.required],
            color: [null, Validators.required],
        });
    }

    get f () { return this.eventForm.controls; }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.eventForm.invalid) {
            return ;
        }

        this.loading = true;
        const eventData = {
            title: this.f.title.value,
            description: this.f.description.value,
            event_start_date: this.f.event_start_date.value,
            event_end_date: this.f.event_end_date.value,
            event_start_time: this.f.event_start_time.value,
            event_end_time: this.f.event_end_time.value,
            color: this.f.color.value,
        };
        console.log(eventData);

        this.apiProvider.saveUserEvent(eventData)
            .pipe(first())
            .subscribe(event => {
                    console.log(event);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Evenement ajouté au calendrier avec succès!');
                    this.error = null;
                    this.eventForm.reset();
                    this.onCreate.emit(event);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }
}
