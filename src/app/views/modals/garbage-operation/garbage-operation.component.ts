import {
    AfterContentChecked,
    AfterContentInit, AfterViewChecked,
    AfterViewInit,
    Component, DoCheck,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';
import {GlobalProvider} from '../../../global';

@Component({
  selector: 'app-garbage-operation-modal',
  templateUrl: './garbage-operation.component.html',
  styleUrls: ['./garbage-operation.component.scss']
})
export class GarbageOperationComponent implements OnInit {

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService,
    private global: GlobalProvider, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    get f() {
        return this.garbageOperationForm.controls;
    }

    error = false;
    submitted = false;
    loading = false;
    garbageOperationForm: FormGroup;


    vessels: any[];
    removalProducts;
    uploader1: FileUploader;
    uploader2: FileUploader;
    uploadUrl;
    response;

    showAddVesselModal = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() folderId = null;

    @Output() onClose: EventEmitter<boolean> = new EventEmitter();
    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);

    toggleAddVessel = () => {
        this.showAddVesselModal = !this.showAddVesselModal;
    }

    ngOnInit() {


        console.log(
            `FOLDER ID ${this.folderId} - GB`
        );

        this.uploader1 =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });

        this.uploader2 =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });

        this.response = '';
        this.uploader1.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            this.garbageOperationForm.controls.certificate.setValue(files[0]['id']);
            console.log(this.garbageOperationForm.controls);
        } );

        this.uploader2.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            this.garbageOperationForm.controls.certificate_des.setValue(files[0]['id']);
            console.log(this.garbageOperationForm.controls);
        } );

        this.fetchRemovalProducts();
        this.fetchVessels();

        this.garbageOperationForm = this.formBuilder.group({
            vessel: [null, Validators.required],
            product: [null, Validators.required],
            qty: [null, Validators.required],
            distance: [null, Validators.required],
            certificate: [],
            certificate_des: [],
            expiry: [null, Validators.required],
        });
    }

    fetchVessels() {
        this.apiProvider.getVessels().subscribe((data: any[]) => {
            this.vessels = data;
        });
    }

    fetchRemovalProducts() {
        this.apiProvider.getRemovalProducts().subscribe((data: any[]) => {
            this.removalProducts = data;
        });
    }


    onSubmit() {
        this.submitted = true;

        // if (this.garbageOperationForm.invalid) {
        //     return ;
        // }

        this.loading = true;
        const garbageData = {
            product_id: this.f.product.value,
            boat_id: this.f.vessel.value,
            distance: this.f.distance.value,
            qty: this.f.qty.value,
            expiry_med: this.f.expiry.value,
            folder_id: this.folderId,
            certificate_file_id: this.f.certificate.value,
            certificate_des_file_id: this.f.certificate_des.value,
        };

        console.log(garbageData);

        this.apiProvider.saveGarbageRemoval(garbageData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Operation de garbage ajouté au dossier avec succès!');
                    this.error = null;
                    this.garbageOperationForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    addVessel($event) {}


    closeModal() {
        this.onClose.emit(true);
    }
}
