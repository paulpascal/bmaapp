import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GarbageOperationComponent } from './transport-operation.component';

describe('TransportOperationComponent', () => {
  let component: GarbageOperationComponent;
  let fixture: ComponentFixture<GarbageOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GarbageOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GarbageOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
