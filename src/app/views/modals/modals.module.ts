import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateShipComponent } from './create-ship/create-ship.component';
import { CreateCustomerComponent } from './create-customer/create-customer.component';
import { SelectCustomerComponent } from './select-customer/select-customer.component';
import { SelectShipComponent } from './select-ship/select-ship.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    declarations: [ CreateShipComponent, CreateCustomerComponent, SelectCustomerComponent, SelectShipComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        LaddaModule,
        NgSelectModule
    ],
    exports: [
        CreateShipComponent, CreateCustomerComponent, SelectCustomerComponent, SelectShipComponent
    ]
})
export class ModalsModule { }
