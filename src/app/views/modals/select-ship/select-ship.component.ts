import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-select-ship-modal',
  templateUrl: './select-ship.component.html',
  styleUrls: ['./select-ship.component.scss']
})
export class SelectShipComponent implements OnInit {

    loading = false;
    submitted = false;
    ships: any[];
    shipForm: FormGroup;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Output() onSelect = new EventEmitter<any>();

    @Input() show = false;
    @Input() customClass = '';
    @Input() closeCallback = () => (false);

    constructor( private formBuilder: FormBuilder, private apiService: ApiService) { }

    ngOnInit() {
        this.apiService.getShips()
            .pipe(first())
            .subscribe((ships: any[]) => {
                this.ships = ships;
            });

        this.shipForm = this.formBuilder.group({
            ship: [null, Validators.required]
        });
    }

    onSubmit() {
        console.log(this.f);

        this.submitted = true;
        if (this.shipForm.invalid) {
            return ;
        }

        this.onSelect.emit(this.f.ship.value);
        this.closeCallback();
    }

    get f() { return this.shipForm.controls; }
}
