import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransfertOperationComponent } from './transfert-operation.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterService} from 'angular2-toaster';
import {FileUploadModule} from 'ng2-file-upload';

@NgModule({
  declarations: [TransfertOperationComponent],
  imports: [
    CommonModule,
      FormsModule,
      ReactiveFormsModule,
      LaddaModule,
      NgSelectModule,
      FileUploadModule,
  ], exports: [TransfertOperationComponent], providers: [
        ToasterService
    ]
})
export class TransfertOperationModule { }
