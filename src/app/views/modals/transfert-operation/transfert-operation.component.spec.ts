import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransfertOperationComponent } from './transfert-operation.component';

describe('TransportOperationComponent', () => {
  let component: TransfertOperationComponent;
  let fixture: ComponentFixture<TransfertOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransfertOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransfertOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
