import {
    AfterContentChecked,
    AfterContentInit, AfterViewChecked,
    AfterViewInit,
    Component, DoCheck,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output, SimpleChanges
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';
import {GlobalProvider} from '../../../global';

@Component({
  selector: 'app-certificate-renewal-operation-modal',
  templateUrl: './certificate-renewal-operation.component.html',
  styleUrls: ['./certificate-renewal-operation.component.scss']
})
export class CertificateRenewalComponent implements OnInit {

    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService,
    private global: GlobalProvider, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    get f() {
        return this.certificateRenewalOperationForm.controls;
    }

    error = false;
    submitted = false;
    loading = false;
    certificateRenewalOperationForm: FormGroup;


    vessels: any[];
    types;
    uploader: FileUploader;
    uploadUrl;
    response;

    showAddVesselModal = false;


    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() folderId = null;

    @Output() onClose: EventEmitter<boolean> = new EventEmitter();
    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);

    toggleAddVessel = () => {
        this.showAddVesselModal = !this.showAddVesselModal;
    }

    ngOnInit() {


        console.log(
            `FOLDER ID ${this.folderId} - GB`
        );

        this.uploader =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });

        this.uploader.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            this.certificateRenewalOperationForm.controls.certificate.setValue(files[0]['id']);
            console.log(this.certificateRenewalOperationForm.controls);
        } );

        this.fetchCertificateTypes();
        this.fetchVessels();

        this.certificateRenewalOperationForm = this.formBuilder.group({
            vessel: [null, Validators.required],
            type: [null, Validators.required],
            distance: [null, Validators.required],
            certificate: [],
        });
    }

    fetchVessels() {
        this.apiProvider.getVessels().subscribe((data: any[]) => {
            this.vessels = data;
        });
    }

    fetchCertificateTypes() {
        this.apiProvider.geCertificateTypes().subscribe((data: any[]) => {
            this.types = data;
        });
    }

    addVessel($event) {
        // console.log($event);
        this.fetchVessels();
        this.f.vessel.setValue($event);
    }

    onSubmit() {
        this.submitted = true;

        if (this.certificateRenewalOperationForm.invalid) {
            return ;
        }

        this.loading = true;
        const certificateRenewalData = {
            type_id: this.f.type.value,
            boat_id: this.f.vessel.value,
            distance: this.f.distance.value,
            folder_id: this.folderId,
            certificate_file_id: this.f.certificate.value,
        };

        console.log(certificateRenewalData);

        this.apiProvider.saveCertificateRenewal(certificateRenewalData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Operation de certificateRenewal ajouté au dossier avec succès!');
                    this.error = null;
                    this.certificateRenewalOperationForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }


    closeModal() {
        this.onClose.emit(true);
    }
}
