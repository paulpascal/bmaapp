import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {CertificateRenewalComponent} from './certificate-renewal-operation.component';


describe('CertificateRenewalComponent', () => {
  let component: CertificateRenewalComponent;
  let fixture: ComponentFixture<CertificateRenewalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificateRenewalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateRenewalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
