import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService, AuthenticationService} from '../../../_services';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {GlobalProvider} from '../../../global';
import {FileUploader} from 'ng2-file-upload';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-delivery-operation-modal',
  templateUrl: './delivery-operation-modal.component.html',
  styleUrls: ['./delivery-operation-modal.component.scss']
})
export class DeliveryOperationModalComponent implements OnInit {


    constructor( private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService,
                 private global: GlobalProvider, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    get f() {
        return this.deliveryOperationForm.controls;
    }

    error = false;
    submitted = false;
    loading = false;
    deliveryOperationForm: FormGroup;


    vessels: any[];
    uploader: FileUploader;
    uploadUrl;
    response;

    showAddVesselModal = false;


    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Input() show = false;
    @Input() customClass = '';
    @Input() folderId = null;

    @Output() onClose: EventEmitter<boolean> = new EventEmitter();
    @Output() onCreate = new EventEmitter<any>();
    @Input() closeCallback = () => (false);

    toggleAddVessel = () => {
        this.showAddVesselModal = !this.showAddVesselModal;
    }


    ngOnInit() {


        console.log(
            `FOLDER ID ${this.folderId} - DL`
        );

        this.uploader =  new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            // headers: [
            //     {name: 'Accept', value: 'application/json'},
            //     { name: 'Access-Control-Allow-Origin', value: 'http://localhost:4200' },
            //     { name: 'Access-Control-Allow-Credentials', value: 'true' },
            // ],
            autoUpload: true,
        });

        this.uploader.response.subscribe( res => {
            const files = JSON.parse(res);
            console.log(res);
            this.deliveryOperationForm.controls.delieverReceipt.setValue(files[0]['id']);
            console.log(this.deliveryOperationForm.controls);
        } );

        this.fetchVessels();

        this.deliveryOperationForm = this.formBuilder.group({
            vessel: [null, Validators.required],
            product: [null, Validators.required],
            qty: [null, Validators.required],
            distance: [null, Validators.required],
            details: [null, Validators.required],
            delieverReceipt: [null, Validators.required],
            isBoatSpecial: [false, ]
        });
    }

    fetchVessels() {
        this.apiProvider.getVessels().subscribe((data: any[]) => {
            this.vessels = data;
        });
    }

    addVessel($event) {
        // console.log($event);
        this.fetchVessels();
        this.f.vessel.setValue($event);
    }

    onSubmit() {
        this.submitted = true;

        if (this.deliveryOperationForm.invalid) {
            return ;
        }

        this.loading = true;
        const deliveryData = {
            product: this.f.product.value,
            boat_id: this.f.vessel.value,
            distance: this.f.distance.value,
            qty: this.f.qty.value,
            details: this.f.details.value,
            isBoatSpecial: this.f.isBoatSpecial.value,
            folder_id: this.folderId,
            deliver_receipt_file_id: this.f.delieverReceipt.value,
        };

        console.log(deliveryData);

        this.apiProvider.saveDelivery(deliveryData)
            .pipe(first())
            .subscribe(ops => {
                    console.log(ops);
                    this.submitted = false;
                    this.loading = false;
                    this.showSuccess('success', 'Opération réussie!', 'Operation de certificateRenewal ajouté au dossier avec succès!');
                    this.error = null;
                    this.deliveryOperationForm.reset();
                    this.onCreate.emit(ops.id);
                    this.closeCallback();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }


    closeModal() {
        this.onClose.emit(true);
    }
}
