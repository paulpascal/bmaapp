import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryOperationModalComponent } from './delivery-operation-modal.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {NgSelectModule} from '@ng-select/ng-select';
import {BsDatepickerModule} from 'ngx-bootstrap';
import {FileUploadModule} from 'ng2-file-upload';
import {ToasterService} from 'angular2-toaster';
import {AddVesselModule} from '../add-vessel/add-vessel.module';

@NgModule({
  declarations: [DeliveryOperationModalComponent],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        LaddaModule,
        NgSelectModule,
        BsDatepickerModule,
        FileUploadModule,
        AddVesselModule,
    ],  exports: [DeliveryOperationModalComponent], providers: [
        ToasterService
    ]
})
export class DeliveryOperationModalModule { }
