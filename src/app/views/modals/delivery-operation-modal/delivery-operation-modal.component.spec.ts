import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryOperationModalComponent } from './delivery-operation-modal.component';

describe('DeliveryOperationModalComponent', () => {
  let component: DeliveryOperationModalComponent;
  let fixture: ComponentFixture<DeliveryOperationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryOperationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryOperationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
