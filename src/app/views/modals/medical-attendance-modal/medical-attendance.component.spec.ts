import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalAttendanceComponent } from './medical-attendance-operation.component';

describe('MedicalAttendanceComponent', () => {
  let component: MedicalAttendanceComponent;
  let fixture: ComponentFixture<MedicalAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
