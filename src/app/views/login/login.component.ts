import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';

import {AuthenticationService} from '../../_services';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    returnUrl: string;
    loading = false;
    submitted = false;
    error = '';
    not = null;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthenticationService
    ) {
        // redirect to dashboard if is already logged in
        if (this.authService.currentUserObject) {
            this.router.navigate(['/']);
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            uid: ['', Validators.required],
            password: ['', Validators.required],
        });

        this.getNotification();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        this.authService.login(this.f.uid.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                    if (error.status !== 404) {
                        this.error = 'Impossible de se connecter au serveur. Réessayez svp ou vérifiez votre connexion.';
                    } else {
                        this.error = error.error.message;
                    }
                    this.loading = false;
                }
            );
    }

    getNotification() {
        const param = this.router.parseUrl(this.router.url);
        this.not = param.queryParams['not'];
    }
}
