import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SettingRoutingModule } from './setting-routing.module';
import {SettingComponent} from './setting.component';
import {SharedModule} from '../../shared.module';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { WjGridModule } from 'wijmo/wijmo.angular2.grid';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
    declarations: [
        SettingComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        SettingRoutingModule,
        ToasterModule,
        FormsModule,
        ReactiveFormsModule,
        WjGridModule,
        NgSelectModule,
    ],
    providers: [
        ToasterService
    ]
})
export class SettingModule { }
