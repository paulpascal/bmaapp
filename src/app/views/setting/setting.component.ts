import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../_services';
import {first} from 'rxjs/operators';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

import * as wjcGrid from 'wijmo/wijmo.grid';
import * as wjcGridXlsx from 'wijmo/wijmo.grid.xlsx';

@Component({
    selector: 'app-setting',
    templateUrl: './setting.component.html',
    styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {

    form1: FormGroup;
    form2: FormGroup;
    fileChoosed: any;

    loading = false;
    submitted1 = false;
    submitted2 = false;

    settings: any;
    customers: any;

    data: any[];
    fileName = '';
    includeColumnHeader = true;
    customContent = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @ViewChild('flex') flex: wjcGrid.FlexGrid;

    files = [
        {name: 'Copie_de_Husbandry',
            path: 'assets/files/Copie_de_Husbandry.xlsx' },
        {name: 'Husbandry-NEXTMARITIME_2019_RATE_UPDATED',
            path: 'assets/files/Husbandry-NEXTMARITIME_2019_RATE_UPDATED.xlsx' },
        {name: 'Husbandry_Services_Lome-MARVEL_2018_RATE',
            path: 'assets/files/Husbandry_Services_Lome-MARVEL_2018_RATE.xlsx'},
        {name: 'Husbandry_Services_Lome-TRANSCOMA_2018_RATE',
            path: 'assets/files/Husbandry_Services_Lome-TRANSCOMA_2018_RATE.xlsx'},
    ];

    constructor(private formBuilder: FormBuilder, private apiProvider: ApiService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.initForm();
        this.apiProvider.getSettings()
            .pipe(first())
            .subscribe(settings => {
                this.settings = settings;
            });

        this.apiProvider.getCustomers()
            .pipe(first())
            .subscribe(customers => {
                this.customers = customers;
            });

        this.fileChoosed = this.files[0].path;
        this.fileChanged();
    }

    toggleLoading() {
        this.loading = !this.loading;
    }
    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    get g () { return this.form2.controls; }

    initForm() {
        this.form1 = this.formBuilder.group({
            customer: [null, Validators.required],
            dueDateDelay: [null, Validators.required]
        });
        this.form2 = this.formBuilder.group({
            services: this.formBuilder.array([])
        });
    }
    onSubmit1() {
        const selectedCustomer = this.form1.controls['customer'].value;
        const editedDueDateDelay = this.form1.controls['dueDateDelay'].value;

        if (selectedCustomer.dueDateDelay !== editedDueDateDelay) {
            selectedCustomer.dueDateDelay = editedDueDateDelay;
            this.toggleLoading();
            this.apiProvider.editCustomer(selectedCustomer.id, selectedCustomer)
                .pipe(first())
                .subscribe(customer => {
                    this.toggleLoading();
                    this.showSuccess('success', 'Notification', 'Due date delay modifié avec succès.');
                });
        }
    }
    onSubmit2() {
        console.log(this.form2.controls.services);
    }

    filter(e: any) {
        const filter = (<HTMLInputElement>e.target).value.toLowerCase();
        console.log(this.flex);
        this.flex.collectionView.filter = (item: any) => {
            return filter.length === 0 || item.country.toLowerCase().indexOf(filter) > -1;
        };
    }

    initializeFlexSheet(flex: wjcGrid.FlexGrid) {
        const groupNames = ['Product', 'Country', 'Amount'];
        let cv;

        // get the collection view, start update
        cv = flex.collectionView;
        cv.beginUpdate();

        // clear existing groups
        cv.groupDescriptions.clear();

        // done updating
        cv.endUpdate();
    }

    //
    onLoadedRows(grid: wjcGrid.FlexGrid) {
        this.autoSizeRowsAsync(grid);
    }
    //
    onCellEditEnded(grid: wjcGrid.FlexGrid, e: wjcGrid.CellEditEndingEventArgs) {
        if (grid.columns[e.col].wordWrap) {
            this.autoSizeRowsAsync(grid, e.row);
        }
    }
    //
    onScrollPositionChanged(grid: wjcGrid.FlexGrid, e: wjcGrid.CellRangeEventArgs) {
        const vr = grid.viewRange;
        for (let r = vr.topRow; r <= vr.bottomRow; r++) {
            if (grid.rows[r].height == null) {
                grid.autoSizeRows(r, r);
                console.log('autosized row ' + r);
            }
        }
    }
    //
    onResizedColumn(grid: wjcGrid.FlexGrid, e: wjcGrid.CellRangeEventArgs) {
        if (grid.columns[e.col].wordWrap) {
            this.autoSizeRowsAsync(grid);
        }
    }
    //
    onAsSyncClick() {
        const start = Date.now();
        this.flex.autoSizeRows();
        alert('AutoSized all rows in ' + (Date.now() - start) + 'ms');
    }
    //
    autoSizeRowsAsync(grid: wjcGrid.FlexGrid, rowIndex?: number) {
        if (rowIndex != null) {
            grid.rows[rowIndex].height = null;
        } else {
            grid.rows.forEach(function (row) {
                row.height = null;
            });
        }
        setTimeout(function () {
            grid.onScrollPositionChanged();
        });
    }

    fileChanged() {
        console.log(this.fileChoosed);
        this.toggleLoading();
        this.apiProvider.getFile(`${this.fileChoosed}`)
            .pipe(first())
            .subscribe(blob => {
                wjcGridXlsx.FlexGridXlsxConverter.loadAsync(this.flex, blob, { includeColumnHeaders: true });
                this.toggleLoading();
            }, error => {
                console.log(error);
                this.toggleLoading();
            });
    }

    customerChanged($event) {
        this.form1.controls['dueDateDelay'].setValue($event.dueDateDelay);
        console.log($event);
    }
}
