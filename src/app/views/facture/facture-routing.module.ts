import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'liste',
        pathMatch: 'full',
    },
    {
        path: '',
        data: {
            title: 'Factures'
        },
        children: [
            {
                path: 'ajouter',
                loadChildren: './new/facture-new.module#FactureNewModule'
            },
            {
                path: 'ajouter/:id',
                loadChildren: './new/facture-new.module#FactureNewModule'
            },
            {
                path: 'afficher/:id',
                loadChildren: './show/facture-show.module#FactureShowModule'
            },
            {
                path: 'liste',
                loadChildren: './list/facture-list.module#FactureListModule'
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FactureRoutingModule { }
