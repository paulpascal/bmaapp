import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// Angular 2 Input Mask
import { TextMaskModule } from 'angular2-text-mask';

// Timepicker
import {TimepickerModule} from 'ngx-bootstrap';

// Datepicker
import { BsDatepickerModule } from 'ngx-bootstrap';

// Ng2-select
import { SelectModule } from 'ng-select';

// Routing
import { FactureListRoutingModule } from './facture-list-routing.module';

import { FactureListComponent } from './facture-list.component';
import {DataTableModule} from 'angular2-datatable';
import {MomentModule} from 'ngx-moment';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {SharedModule} from '../../../shared.module';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        FactureListRoutingModule,
        TextMaskModule,
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        SelectModule,
        DataTableModule,
        MomentModule,
        SweetAlert2Module,
        ToasterModule
    ],
    declarations: [
        FactureListComponent,
    ],
    providers: [
        ToasterService
    ]
})
export class FactureListModule { }
