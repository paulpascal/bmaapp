import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FactureListComponent } from './facture-list.component';

const routes: Routes = [
  {
    path: '',
    component: FactureListComponent,
    data: {
      title: 'Liste des factures'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FactureListRoutingModule {}
