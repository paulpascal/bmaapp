import {Component, OnInit, ViewChild} from '@angular/core';

import { SwalComponent } from '@toverux/ngx-sweetalert2';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

import * as moment from 'moment';
import {ApiService} from '../../../_services';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
moment.locale('fr');

@Component({
    templateUrl: 'facture-list.component.html',
    providers: [ToasterService]
})
export class FactureListComponent implements OnInit {

    error: any;
    public invoices: any[];
    public filterQuery = '';
    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router,  toasterService: ToasterService,
                ) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.fecth();
    }

    public goToAddInvoice() {
        this.router.navigate(['/facture/ajouter']);
    }

    showInvoice(invoice: any) {
        this.router.navigate([`/facture/afficher/${invoice.id}`]);
    }

    public editInvoices(invoice) {
        // this.router.navigate([`/admin/invoices/edit/${invoice.id}`]);
    }

    public confirmDeleteInvoice(invoice) {
        this.deleteSwal.options = {
            title: 'Supprimer cette facture?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteInvoices(invoice);
            }
        };
        this.deleteSwal.show();
    }

    deleteInvoices(invoice) {
        this.apiService.deleteInvoice(invoice.id)
            .subscribe(() => {
                const index = this.invoices.indexOf(invoice, 0);
                if (index > -1) {
                    this.invoices.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Fiche supprimée avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getInvoices()
            .pipe(first())
            .subscribe(
                (invoices: any[]) => {
                    // ..
                    this.loading = false;
                    this.invoices = invoices;
                }
            );
    }

    submitInvoice(invoice: any) {
        this.loading = true;
        this.apiService.submitInvoice(invoice.id)
            .pipe(first())
            .subscribe(
                (res: any[]) => {
                    // ..
                    this.loading = false;
                    this.fecth();
                    this.showAlert('success', 'Notification', res['message']);
                }
            );
    }
}
