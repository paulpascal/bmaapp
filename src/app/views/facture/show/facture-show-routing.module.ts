import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FactureShowComponent} from './facture-show.component';

const routes: Routes = [
    {
        path: '',
        component: FactureShowComponent,
        data: {
            title: 'Afficher un facture'
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FactureShowRoutingModule { }
