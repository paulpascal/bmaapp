import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactureShowComponent } from './facture-show.component';

describe('FactureShowComponent', () => {
  let component: FactureShowComponent;
  let fixture: ComponentFixture<FactureShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FactureShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactureShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
