import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FactureShowRoutingModule } from './facture-show-routing.module';
import {FactureShowComponent} from './facture-show.component';
import {PDFExportModule} from '@progress/kendo-angular-pdf-export';
import {ToasterModule, ToasterService} from 'angular2-toaster';

@NgModule({
    declarations: [FactureShowComponent],
    imports: [
        CommonModule,
        FactureShowRoutingModule,
        PDFExportModule,
        ToasterModule
    ],
    providers: [
        ToasterService
    ]
})
export class FactureShowModule { }
