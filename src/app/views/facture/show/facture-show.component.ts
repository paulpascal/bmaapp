import { Component, OnInit } from '@angular/core';
import * as JSPDF from 'jspdf';
import * as writtenForm from 'written-number';
import html2canvas from 'html2canvas';
import {first} from 'rxjs/operators';
import {ApiService, AuthenticationService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-show',
    templateUrl: './facture-show.component.html',
    styleUrls: ['./facture-show.component.scss'],
    providers: [ToasterService],
})
export class FactureShowComponent implements OnInit {

    invoice: any;
    total = 0;
    private toasterService: ToasterService;

    showValidButton = false;
    showUnValidButton = false;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private apiService: ApiService, private route: ActivatedRoute,  toasterService: ToasterService,
                private router: Router, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.getInvoice(this.route.snapshot.params['id']);
    }

    public print() {
        const data = document.getElementById('printable');
        html2canvas(data).then(canvas => {
            // Few necessary setting options
            const imgWidth = 200;
            const pageHeight = 295;
            const imgHeight = canvas.height * imgWidth / canvas.width;
            const heightLeft = imgHeight;

            const contentDataURL = canvas.toDataURL('image/png');
            const pdf = new JSPDF('p', 'mm', 'a4'); // A4 size page of PDF
            const position = 5;
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
            pdf.save('facture' + this.getMoment() + '.pdf'); // Generated PDF
        });
    }

    getInvoice(invoiceId) {
        this.apiService.getInvoice(invoiceId)
            .pipe(first())
            .subscribe((invoice: any) => {
                this.invoice = invoice;
                this.total = 0;
                for (const i of this.invoice.invoice_details) {
                    this.total += parseFloat(i.total);
                }
                console.table([...this.invoice.invoice_details]);
                console.log(this.total);

                this.toggleButton();
            }, error => {
                    if (error.status === 404) {
                        this.router.navigate(['/facture/liste']);
                    }
                }
            )
        ;
    }

    validateInvoice(invoiceId) {
        this.apiService.validateInvoice(invoiceId)
            .pipe(first())
            .subscribe((invoice: any) => {
                this.showToast('success', 'Notification', 'Validation ajoutée avec succès.');
                this.invoice = invoice;
                this.total = 0;
                for (const i of this.invoice.invoice_details) {
                    this.total += parseFloat(i.total);
                }
                console.table([...this.invoice.invoice_details]);
                console.log(this.total);
                this.toggleButton();
                }, error => {
                    if (error.status === 404) {
                        this.router.navigate(['/facture/liste']);
                    }
                }
            )
        ;
    }

    unvalidateInvoice(invoiceId) {
        this.apiService.unvalidateInvoice(invoiceId)
            .pipe(first())
            .subscribe((invoice: any) => {
                this.showToast('success', 'Notification', 'Validation suprrimée avec succès.');
                this.invoice = invoice;
                this.total = 0;
                for (const i of this.invoice.invoice_details) {
                    this.total += parseFloat(i.total);
                }
                console.table([...this.invoice.invoice_details]);
                console.log(this.total);
                this.toggleButton();
                }, error => {
                    if (error.status === 404) {
                        this.router.navigate(['/facture/liste']);
                    }
                }
            )
        ;
    }

    getMoment() {
        return Date.now();
    }

    toLetter(digit) {
        return writtenForm(digit, { lang: 'en', noAnd: true });
    }

    showToast(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    toggleButton() {
        if (environment.adminRoles.includes(this.authService.currentUserObject['role']['label'])) {
            if (this.invoice.validations.length === 0) {
                this.showValidButton = true;
                this.showUnValidButton = false;
            } else {
                this.showUnValidButton = true;
                this.showValidButton = false;
            }
        }
    }
}
