import {Component, OnInit, ViewEncapsulation, Input, OnChanges, ViewChild} from '@angular/core';
import {first} from 'rxjs/operators';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import * as writtenForm from 'written-number';
import {ActivatedRoute} from '@angular/router';

import * as ToWords from 'with_decimal';
import * as moment from 'moment';
moment.locale('en');
registerLocaleData(localeFr, 'fr');

export class InvoiceItem {
    description = '';
    bas = '';
    qty = '';
    number_and_ref = '';
    total = 0;
}

declare var $: any;

@Component({
    templateUrl: 'facture-new2.component.html',
    styleUrls: [
        'facture-new.component.scss',
        '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss',
        '../../../../scss/vendors/ng-select/ng-select.scss',
        '../../../../../node_modules/ladda/dist/ladda-themeless.min.css',
        '../../../../scss/vendors/toastr/toastr.scss'
    ],
    providers: [ToasterService],
    encapsulation: ViewEncapsulation.None
})
export class FactureNewComponent implements OnInit {

    error = '';
    invoicesMissionsTags = [];
    invoiceTags = new Set();
    loading = false;
    showModal = false;

    logoSrc = 'assets/logo.png';

    private toasterService: ToasterService;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    data = [ ];
    invoices: any[] = [];
    folder: any;
    config: any;
    total: any;
    archivedMissionIds = [];
    excludedMissionIds = []; // this is different cause.. we may have archivedmissions included to invoice in some cases
    notToPrintIds = [];

    selectedMissions = false;

    constructor(
        private formBuilder: FormBuilder, toasterService: ToasterService,
        private apiService: ApiService, private authService: AuthenticationService, private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.fetchFolder(this.route.snapshot.params['id']);

        this.fetchInvoiceNo(this.route.snapshot.params['id']);
    }


    fetchFolder(id) {
        this.toggleLoading();
        this.apiService.getFolder(id)
            .pipe(first())
            .subscribe((folder: any[]) => {
                this.folder = folder[0];
                this.getArchivedMissions(folder[0]);

                if (this.archivedMissionIds.length) {
                    this.openModal();
                } else {
                    this.handleMissions();
                    this.selectedMissions = true;
                }

                console.log(this.folder);
                this.toggleLoading();
            }
            , error => {
                    this.toggleLoading();
            });
    }
    fetchInvoiceNo (folderId) {
        this.apiService.getInvoiceNo(folderId)
            .pipe(first())
            .subscribe(
                (resp: any) => {
                    // ..
                    this.config = resp;
                }
            );
    }

    // $P$BcjyWc.W0z4V2l65q3J4hiXOP2P1Sb0 x001 contact@afreak.net
    searchFromArray(arr, regex) {
        const matches = [];
        let i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].match(regex)) {
                matches.push(arr[i]);
            }
        }
        return matches;
    }

    getNextInvoiceMissionLetter(invoiceId, index): String {
        if (!index) {
            this.invoicesMissionsTags.push(new Set());
        }
        const char: String = 'A';
        let code = char.charCodeAt(0);
        code += index;
        this.invoicesMissionsTags[invoiceId].add(String.fromCharCode(code));
        console.log(this.invoicesMissionsTags[invoiceId]);
        return String.fromCharCode(code);
    }

    getNextInvoiceLetter(index) {
        const char: String = 'A';
        let code = char.charCodeAt(0);
        code += index;
        this.invoiceTags.add(String.fromCharCode(code));
        console.log(this.invoiceTags);
        return String.fromCharCode(code);
    }

    concatLettersForInvoice(invoiceId) {
        return Array.from(this.invoicesMissionsTags[invoiceId].values()).join(' + ');
    }

    format(string) {
        const date = moment(string);
        return date.format('Do MMMM YYYY');
    }
    toLetter(amount, curr) {
        console.log(curr);
        return ToWords(amount, {currency: true, currencyLabel: curr});
    }

    genFolNo(fol) {
        return `BMA-${fol.number}/${(new Date()).getFullYear()}`;
    }
    toggleLoading() {
        this.loading = !this.loading;
    }
    toggleSelectedMission() {
        this.selectedMissions = !this.selectedMissions;
    }
    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    handleMissions() {
        for (let i = 0; i < this.folder.missions.length; ++i) {

            if (this.excludedMissionIds.includes(this.folder.missions[i].id)) {
                continue; // continue to next mission
            }

            const invoice = {no: '', name: '', conc: '', missions: [], total: 0.0, count: 0};
            let mission;

            // first
            invoice.name = this.folder.missions[i].mission_type.type;
            mission = {
                name: this.folder.missions[i].name,
                items: this.folder.missions[i].operations};
            this.computeMissionTotal(mission);
            invoice.missions.push(mission);
            if (this.folder.missions[i].operations.length) {
                invoice.count += this.folder.missions[i].operations.length;
            }

            // same family directly nested
            for (let k = i + 1; k < this.folder.missions.length; ++k) {

                if (this.excludedMissionIds.includes(this.folder.missions[k].id)) {
                    continue; // continue to next mission
                }

                if (this.folder.missions[i].mission_type.type === this.folder.missions[k].mission_type.type && invoice.count < 10
                    && this.folder.missions[k].operations && (invoice.count + this.folder.missions[k].operations.length) <= 10) {
                    mission = {
                        name: this.folder.missions[k].name,
                        items: this.folder.missions[k].operations};
                    this.computeMissionTotal(mission);
                    invoice.missions.push(
                        mission
                    );
                    invoice.count += this.folder.missions[k].operations.length;
                    i = k;
                } else { break; }
            }

            this.invoices.push(invoice);
            this.computeInvoiceTotal(invoice);
        }
        console.log(this.invoices);
    }

    computeItemTotal(item) {
        let tot = 0.00;
        if (item.qty) {
            tot = parseFloat(item.qty) * parseFloat(item.bas);
        } else {
            tot = parseFloat(item.bas);
        }
        return tot.toFixed(2);
    }

    computeMissionTotal(mission) {
        let missionTotal = 0.00;
        for (let j = 0; j < mission.items.length; ++j) {
            missionTotal += parseFloat(this.computeItemTotal(mission.items[j]));
        }
        mission['total'] = missionTotal.toFixed(2);
        return missionTotal.toFixed(2);
    }

    computeInvoiceTotal(invoice) {
        let invoiceTotal = 0.0;
        for (let j = 0; j < invoice.missions.length; ++j) {
            invoiceTotal += parseFloat(this.computeMissionTotal(invoice.missions[j]));
        }
        invoice['total'] = invoiceTotal.toFixed(2);
        return invoiceTotal.toFixed(2);
    }

    closeModal() {
        this.showModal = false;
    }
    openModal() {
        this.showModal = true;
    }

    getArchivedMissions(folder) {

        for (let i = 0; i < this.folder.missions.length; ++i) {
            if (this.folder.missions[i].isArchived) {
                this.archivedMissionIds.push(this.folder.missions[i].id);
            }
        }
        this.excludedMissionIds.push(...this.archivedMissionIds);
    }

    handleCheck(evt: Event, am: any) {
        console.log(evt.target['checked'], am);

        console.log(this.excludedMissionIds);
        console.log(this.archivedMissionIds);

        // if has been checked ... -> delete from excluded
        if (evt.target['checked']) {
            const index = this.excludedMissionIds.indexOf(am.id, 0);
            if (index > -1) {
                this.excludedMissionIds.splice(index, 1);
            }
        } else {
            // else add to excluded
            const index = this.excludedMissionIds.indexOf(am.id, 0);
            if (index === -1) {
                this.excludedMissionIds.push(am.id);
            }
        }
        console.log(this.excludedMissionIds);
        console.log(this.archivedMissionIds);
    }

    continue() {
        this.handleMissions();
        this.selectedMissions = true;
        this.closeModal();
    }
}
