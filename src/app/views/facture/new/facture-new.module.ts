import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// Angular 2 Input Mask
import { TextMaskModule } from 'angular2-text-mask';

// Angular Autocomplete (Angular 2 +)
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

// Timepicker
import {BsDropdownModule, CollapseModule, ModalModule, TimepickerModule} from 'ngx-bootstrap';

// Datepicker
import { BsDatepickerModule } from 'ngx-bootstrap';

// Routing
import { FactureNewRoutingModule } from './facture-new-routing.module';
import { FactureNewComponent } from './facture-new.component';
import {LaddaModule} from 'angular2-ladda';
import {ToasterModule} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {SharedModule} from '../../../shared.module';

// Flex
import { FlexLayoutModule } from '@angular/flex-layout';

import { polyfill as keyboardEventKeyPolyfill } from 'keyboardevent-key-polyfill';
import { TextInputAutocompleteModule } from 'angular-text-input-autocomplete';

keyboardEventKeyPolyfill();


import {NgxPrintModule} from 'ngx-print';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        LaddaModule,
        ToasterModule,
        FactureNewRoutingModule,
        ModalModule,
        NgxPrintModule
    ],
    declarations: [
        FactureNewComponent,
    ]
})
export class FactureNewModule { }
