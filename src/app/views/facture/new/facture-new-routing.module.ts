import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FactureNewComponent } from './facture-new.component';

const routes: Routes = [
  {
    path: '',
    component: FactureNewComponent,
    data: {
      title: 'Ajouter une nouvelle facture'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FactureNewRoutingModule {}
