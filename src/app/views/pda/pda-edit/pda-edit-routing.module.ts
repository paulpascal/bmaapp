import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PdaEditComponent} from './pda-edit.component';

const routes: Routes = [
    {
        path: '',
        component: PdaEditComponent,
        data: {
            title: 'Editer un PDA'
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PdaEditRoutingModule { }
