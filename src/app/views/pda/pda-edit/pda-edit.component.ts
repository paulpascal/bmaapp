import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import {MissionItem} from '../pda-new/pda-new.component';
import {ActivatedRoute, Router} from '@angular/router';
import {pipe} from 'rxjs';
import {GlobalProvider} from '../../../global';

declare const $;

@Component({
  selector: 'app-pda-edit',
  templateUrl: './pda-edit.component.html',
  styleUrls: ['./pda-edit.component.scss']
})
export class PdaEditComponent implements OnInit {

    form: FormGroup;

    error = false;
    loading = false;
    submitted = false;
    pda: any;
    date = new Date();
    curs = [{name: 'EUR'}, {name: 'USD'}, {name: 'F CFA'}];

    accounting = false;
    isPdaValidator = false;

    operation_patterns: any[];
    mission_types: any[];
    results: any;
    lastkeydown2 = 0;
    isANotToShowFile = {};

    savedMissions: any[];

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @ViewChild('shipChoiceModal') shipChoiceModal;

    constructor(private formBuilder: FormBuilder, private apiProvider: ApiService, private authService: AuthenticationService,
                toasterService: ToasterService, private route: ActivatedRoute, private router: Router, private global: GlobalProvider) {
        this.toasterService = toasterService;
        this.accounting = this.authService.hasRole('Accounting');
        this.isPdaValidator = this.authService.hasRole('PdaValidator');
    }

    ngOnInit() {
        this.initForm();
        this.fetchPda(this.route.snapshot.params['id']);
        this.fetchMissionTypes();
        this.fecthOperationPatterns();
        //
    }

    fetchPda(id) {
        this.apiProvider.getPda(id)
            .pipe(first())
            .subscribe((pda: any[]) => {
                    this.pda = pda[0];
                    this.initComponent();
                }, err => {
                    console.log(err);
                    this.showSuccess('error', 'Notification', err.error.message);
                    this.router.navigate(['/pda/liste']);
            });
    }

    fetchMissionTypes() {
        this.apiProvider.getMissionTypes()
            .pipe(first())
            .subscribe((data: any[]) => {
                this.mission_types = data;
                // console.log(this.mission_types);
            });
    }
    fecthOperationPatterns() {
        this.apiProvider.getOperationPatternsNames()
            .pipe(first())
            .subscribe(
                (operation_patterns: any[]) => {
                    // ..
                    this.operation_patterns = operation_patterns;
                }
            );
    }

    initForm() {
        this.form = this.formBuilder.group({
            currency: [null, Validators.required],
            missions: this.formBuilder.array([])
        });
    }

    initComponent() {
        this.form.controls.currency.setValue(this.pda.folder.currency);
        this.savedMissions = this.pda.missions;
        for (let i = 0; i < this.savedMissions.length; ++i) {
            const mission = this.savedMissions[i];

            // ..
            this.missions.controls.push(this.formBuilder.group({
                name: mission.name,
                id: mission.id,
                items: this.formBuilder.array([]),
                user: mission.user
            }));

            console.log(this.missions);

            for (let j = 0; j < mission.operations.length; ++j) {
                // @ts-ignore
                this.missions.controls[i].controls.items.push(this.formBuilder.group(mission.operations[j]));
                // @ts-ignore
                // tslint:disable-next-line:max-line-length
                console.log(mission.operations[j]);
                // @ts-ignore
                console.log(this.missions.controls[i].controls.items.controls[j]);
            }

            this.listener(i + 1);
        }
    }

    onSubmit() {
        this.submitted = true;
        this.toggleLoading();

        if (this.form.invalid) {
            console.log(this.form);
            this.toggleLoading();
            return ;
        }

        const missions = this.missions.getRawValue();
        console.log(missions);
        const length = missions.length;

        if (!length) {
            this.toggleLoading();
            this.showSuccess('warning', 'Notification', 'Le PDA ne contient aucune misison! Veuillez en ajouter.');
            return;
        }
        for (const mission of this.missions.controls) {
            // return ;
            // @ts-ignore
            for (const item of mission.controls.items.controls) {

                if (item.controls.bas.errors) {
                    console.log(item);
                    this.toggleLoading();
                    return ;
                }
            }
        }

        for (let i = 0; i < length; ++i) {
            // missions[i]['name'] = document.getElementById(`dynamicMissions${i}`)['value'];
            for (let j = 0; j < missions[i]['items'].length; ++j) {
                console.log(missions[i]['items'][j]['name'], document.getElementById(`dynamicServices${i}${j}`)['value']);
                missions[i]['items'][j]['name'] = document.getElementById(`dynamicServices${i}${j}`)['value'];
            }
        }

        const pdaData = {
            folder_id: this.pda.folder.id,
            currency: this.f.currency.value,
            missions: missions,
            user_id: this.authService.currentUserObject['id'],
        };
        console.log(pdaData);

        this.apiProvider.editPda(this.pda.id, pdaData)
            .pipe(first())
            .subscribe(pda => {
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('success', 'Notification', 'PDA modifié avec succès!');
                this.form.reset();
                window.location.reload();
            },
                    err => {
                        this.toggleLoading();
                        this.submitted = false;
                        this.showSuccess('error', 'Notification', err.error.message);
                        console.log(err.error.message);
            });
    }

    get f() { return this.form.controls; }

    get missions(): FormArray {
        return this.form.get('missions') as FormArray;
    }
    getMissionItems(mission): FormArray {
        return mission.get('items') as FormArray;
    }

    removeMission(mission) {
        const i = this.missions.controls.indexOf(mission);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            // this.missions.controls.splice(i, 1);
            if (mission.controls.id) {
                this.apiProvider.deletePdaMission(mission.controls.id.value)
                    .pipe(first())
                    .subscribe(resp => {
                        this.showSuccess('success', 'Notification', 'Mission supprimée avec succès.');
                        this.ngOnInit();
                    }, err => {
                        console.log(err);
                        this.showSuccess('error', 'Notification', 'Action impossible.');
                    });
            } else {
                this.missions.controls.splice(i, 1);
            }
        }
    }

    addMissionItem(mission, i) {
        // console.log(mission);
        // console.log(this.missions);
        const k = mission.controls.items.controls.length;

        mission.controls.items.push(this.formBuilder.group(new MissionItem()));
        // console.table(mission['controls']);
    }
    removeMissionItem(mission, item) {
        const i = mission.controls.items.controls.indexOf(item);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            // mission.controls.items.controls.splice(i, 1);
            if (item.controls.id) {
                this.apiProvider.deletePdaOperation(item.controls.id.value)
                    .pipe(first())
                    .subscribe(resp => {
                        this.showSuccess('success', 'Notification', 'Opération supprimée avec succès.');
                        this.ngOnInit();
                    }, err => {
                        console.log(err);
                        this.showSuccess('error', 'Notification', 'Action impossible.');
                    });

            } else {
                mission.controls.items.controls.splice(i, 1);
            }
        }
    }

    toggleLoading() {
        this.loading = !this.loading;
    }

    listener(i) {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${i - 1}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 500);
    }

    getServices($event, i, j) {
        console.log(i);
        const name = (<HTMLInputElement>document.getElementById(`dynamicServices${i}${j}`)).value;
        this.results = [];

        if (name.length > 0) {
            if ($event.timeStamp - this.lastkeydown2 > 200) {
                this.results = this.searchFromArray(this.operation_patterns, name.toUpperCase());
                $(`#dynamicServices${i}${j}`).autocomplete({
                    source: this.results,
                    messages: {
                        noResults: '',
                        results: function () { }
                    }
                });
            }
        }
    }

    searchFromArray(arr, regex) {
        const matches = [];
        let i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].match(regex)) {
                matches.push(arr[i]);
            }
        }
        return matches;
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    generatePDAFile() {
        if (!this.pda.missions.length) {
            this.showSuccess('warning', 'Notification', 'Le PDA ne contient aucune mission veuillez d\'abord l\'éditer.');
            return;
        }
        this.router.navigate(['/pda/detail/' + this.pda.id]);
    }

    hasError(i: number, j: number) {
        // @ts-ignore
        return this.submitted && this.f.missions.controls[i].controls.items.controls[j].controls.bas.errors;
    }

    getError(i: number, j: number) {
        // @ts-ignore
        return this.f.missions.controls[i].controls.items.controls[j].controls.bas.errors;
    }

    genTemplateFor(missionType) {
        const i = this.missions.length;

        this.missions.push(this.formBuilder.group({
            name: [missionType.name, Validators.required],
            mission_type_id: [missionType.id],
            items: this.formBuilder.array([])
        }));

        // creating ops array
        for (let k = 0; k < missionType.operation_patterns.length; ++k) {
            // @ts-ignore
            this.missions.controls[i].controls
                .items.push(this.formBuilder.group(new MissionItem(missionType.operation_patterns[k].name)));

        }
        console.log(this.missions);

        this.listener(this.missions.length);
    }
}
