import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdaEditComponent } from './pda-edit.component';

describe('EditComponent', () => {
  let component: PdaEditComponent;
  let fixture: ComponentFixture<PdaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
