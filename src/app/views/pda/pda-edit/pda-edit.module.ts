import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PdaEditRoutingModule } from './pda-edit-routing.module';
import {PdaEditComponent} from './pda-edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {BsDropdownModule} from 'ngx-bootstrap';
import {ModalsModule} from '../../modals';

@NgModule({
    declarations: [
        PdaEditComponent
    ],
    imports: [
        CommonModule,
        PdaEditRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        NgSelectModule,
        BsDropdownModule,
        ModalsModule,
    ],
    providers: [
        ToasterService,
    ]
})
export class PdaEditModule { }
