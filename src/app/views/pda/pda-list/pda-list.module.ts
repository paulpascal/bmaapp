import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PdaListRoutingModule } from './pda-list-routing.module';
import {PdaListComponent} from './pda-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../../shared.module';
import {SelectModule} from 'ng-select';
import {DataTableModule} from 'angular2-datatable';
import {MomentModule} from 'ngx-moment';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {ToasterModule, ToasterService} from 'angular2-toaster';

@NgModule({
    declarations: [
        PdaListComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        PdaListRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule,
        DataTableModule,
        MomentModule,
        SweetAlert2Module,
        ToasterModule
    ],
    providers: [
        ToasterService
    ]
})
export class PdaListModule { }
