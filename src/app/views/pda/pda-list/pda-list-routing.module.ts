import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PdaListComponent} from './pda-list.component';

const routes: Routes = [
    {
        path: '',
        component: PdaListComponent,
        data: {
            title: 'Liste des dossiers'
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PdaListRoutingModule { }
