import {Component, OnInit, ViewChild} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {ApiService, AuthenticationService} from '../../../_services';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';

@Component({
    selector: 'app-pda-list',
    templateUrl: './pda-list.component.html',
    styleUrls: ['./pda-list.component.scss']
})
export class PdaListComponent implements OnInit {

    error: any;
    public pdas: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    isPdaDeleter = false;
    accounting = false;
    isPdaValidator = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService,
                private authService: AuthenticationService) {
        this.accounting = this.authService.hasRole('Accounting');
        this.isPdaValidator = this.authService.hasRole('PdaValidator');
        this.isPdaDeleter = this.authService.hasRole('PdaDeleter');
    }

    ngOnInit() {
        this.fecth();
    }

    public goToAddPda() {
        this.router.navigate(['/pda/creer']);
    }

    public editPda(pda) {
        this.router.navigate([`/pda/editer/${pda.id}`]);
    }

    public showPda(pda) {
        this.router.navigate([`/pda/detail/${pda.id}`]);
    }

    public confirmDeletePda(pda) {
        this.deleteSwal.options = {
            title: 'Supprimer cet PDA?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deletePda(pda);
            }
        };
        this.deleteSwal.show();
    }

    deletePda(pda) {
        this.apiService.deletePda(pda.id)
            .subscribe(() => {
                const index = this.pdas.indexOf(pda, 0);
                if (index > -1) {
                    this.pdas.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'PDA supprimé avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getPdas()
            .pipe(first())
            .subscribe(
                (pdas: any[]) => {
                    // ..
                    this.pdas = pdas;
                    this.loading = false;
                }
            );
    }

    generatePDAFile(pda) {
        if (!pda.missions.length) {
            this.showAlert('warning', 'Notification', 'Le PDA ne contient aucune mission veuillez d\'abord l\'éditer.');
            return;
        }
        this.router.navigate(['/pda/detail/' + pda.id]);
    }
}
