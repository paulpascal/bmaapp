import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PdaDetailComponent} from './pda-detail.component';

const routes: Routes = [
    {
        path: '',
        component: PdaDetailComponent,
        data: {
            title: 'Detail PDA'
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PdaDetailRoutingModule { }
