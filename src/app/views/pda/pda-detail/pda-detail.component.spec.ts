import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdaDetailComponent } from './pda-detail.component';

describe('DetailComponent', () => {
  let component: PdaDetailComponent;
  let fixture: ComponentFixture<PdaDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdaDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdaDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
