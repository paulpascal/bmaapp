import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {FormBuilder} from '@angular/forms';
import {ApiService, AuthenticationService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';

import * as ToWords from 'with_decimal';
import * as moment from 'moment';
import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
moment.locale('en');
registerLocaleData(localeFr, 'fr');

@Component({
  selector: 'app-detail',
  templateUrl: './pda-detail.component.html',
  styleUrls: ['./pda-detail.component.scss',
      '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss',
      '../../../../scss/vendors/ng-select/ng-select.scss',
      '../../../../../node_modules/ladda/dist/ladda-themeless.min.css',
      '../../../../scss/vendors/toastr/toastr.scss']
})
export class PdaDetailComponent implements OnInit {

    error = '';
    pdaMissionTags = new Set();
    loading = false;
    showModal = false;

    logoSrc = 'assets/logo.png';

    private toasterService: ToasterService;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    data = [ ];
    pda: any;
    config: any;
    total: any;

    constructor(
        private formBuilder: FormBuilder, toasterService: ToasterService,
        private apiService: ApiService, private authService: AuthenticationService, private route: ActivatedRoute, private router: Router
    ) {
    }

    ngOnInit() {
        this.fetchPda(this.route.snapshot.params['id']);

        this.fetchPdaNo(this.route.snapshot.params['id']);
    }

    fetchPda(id) {
        this.toggleLoading();
        this.apiService.getPda(id)
            .pipe(first())
            .subscribe((pda: any[]) => {
                    this.pda = pda[0];

                    this.toggleLoading();
                    console.log(this.pda);
                }
                , error => {
                    this.toggleLoading();
                });
    }
    fetchPdaNo (folderId) {
        this.apiService.getInvoiceNo(folderId)
            .pipe(first())
            .subscribe(
                (resp: any) => {
                    // ..
                    this.config = resp;
                }
            );
    }

    // $P$BcjyWc.W0z4V2l65q3J4hiXOP2P1Sb0 x001 contact@afreak.net
    searchFromArray(arr, regex) {
        const matches = [];
        let i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].match(regex)) {
                matches.push(arr[i]);
            }
        }
        return matches;
    }

    getNextPdaMissionLetter(index): String {
        const char: String = 'A';
        let code = char.charCodeAt(0);
        code += index;
        this.pdaMissionTags.add(String.fromCharCode(code));
        console.log(this.pdaMissionTags);
        return String.fromCharCode(code);
    }

    concatLettersFoPda(pdaId) {
        return Array.from(this.pdaMissionTags.values()).join(' + ');
    }

    format(string) {
        const date = moment(string);
        return date.format('Do MMMM YYYY');
    }
    toLetter(amount, curr) {
        return ToWords(amount, {currency: true, currencyLabel: curr});
    }

    toggleLoading() {
        this.loading = !this.loading;
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    computeItemTotal(item) {
        let tot = 0.00;
        if (item.qty) {
            tot = parseFloat(item.qty) * parseFloat(item.bas);
        } else {
            tot = parseFloat(item.bas);
        }
        return tot.toFixed(2);
    }

    computeMissionTotal(mission) {
        let missionTotal = 0.00;
        for (let j = 0; j < mission.operations.length; ++j) {
            missionTotal += parseFloat(this.computeItemTotal(mission.operations[j]));
        }
        mission['total'] = missionTotal.toFixed(2);
        return missionTotal.toFixed(2);
    }

    computePdaTotal(pda) {
        let pdaTotal = 0.0;
        for (let j = 0; j < pda.missions.length; ++j) {
            pdaTotal += parseFloat(this.computeMissionTotal(pda.missions[j]));
        }
        pda['total'] = pdaTotal.toFixed(2);
        return pdaTotal.toFixed(2);
    }

    closeModal() {
        this.showModal = false;
    }
    openModal() {
        this.showModal = true;
    }

    genPdaNo(pda) {
        return `BMA-${pda.folder.number}/${(new Date()).getFullYear()}`;
    }

    getNextPdaLetter(pda: any) {
        return 'A';
    }

    updatePda() {
        if (!this.pda.isValidated) {
            this.apiService.validatePda(this.pda.id)
                .subscribe(resp => {
                    console.log(resp);
                }, err => {
                    console.log(err);
                });
        }
    }
}
