import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PdaDetailRoutingModule } from './pda-detail-routing.module';
import {PdaDetailComponent} from './pda-detail.component';
import {ModalModule} from 'ngx-bootstrap';
import {NgxPrintModule} from 'ngx-print';
import {SharedModule} from '../../../shared.module';
import {ToasterModule, ToasterService} from 'angular2-toaster';

@NgModule({
    declarations: [
        PdaDetailComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        PdaDetailRoutingModule,
        ModalModule,
        ToasterModule,
        NgxPrintModule
    ],
    providers: [
        ToasterService,
    ]
})
export class PdaDetailModule { }
