import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'liste',
        pathMatch: 'full',
    },
    {
        path: '',
        data: {
            title: 'PDA'
        },
        children: [
            {
                path: 'creer/:folderId',
                loadChildren: './pda-new/pda-new.module#PdaNewModule'
            },
            {
                path: 'editer/:id',
                loadChildren: './pda-edit/pda-edit.module#PdaEditModule'
            },
            {
                path: 'detail/:id',
                loadChildren: './pda-detail/pda-detail.module#PdaDetailModule'
            },
            {
                path: 'liste',
                loadChildren: './pda-list/pda-list.module#PdaListModule'
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PdaRoutingModule { }
