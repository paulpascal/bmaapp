import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdaNewComponent } from './pda-new.component';

describe('NewComponent', () => {
  let component: PdaNewComponent;
  let fixture: ComponentFixture<PdaNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdaNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdaNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
