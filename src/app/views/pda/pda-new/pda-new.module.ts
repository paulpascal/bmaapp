import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PdaNewRoutingModule } from './pda-new-routing.module';
import {PdaNewComponent} from './pda-new.component';
import {SharedModule} from '../../../shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {LaddaModule} from 'angular2-ladda';
import {ModalsModule} from '../../modals';
import {AutoCompleteModule} from 'ng5-auto-complete';
import {BsDropdownModule} from 'ngx-bootstrap';

@NgModule({
    declarations: [
        PdaNewComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        NgSelectModule,
        LaddaModule,
        PdaNewRoutingModule,
        ModalsModule,
        AutoCompleteModule,
        BsDropdownModule,
        // AngularFileUploaderModule
],
    providers: [
        ToasterService
    ]
})
export class PdaNewModule { }
