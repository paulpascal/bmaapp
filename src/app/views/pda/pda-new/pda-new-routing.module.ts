import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PdaNewComponent} from './pda-new.component';

const routes: Routes = [
    {
        path: '',
        component: PdaNewComponent,
        data: {
            title: 'Créer un nouveau PDA'
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PdaNewRoutingModule { }
