import {AfterViewInit, Component, Directive, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ActivatedRoute, Router} from '@angular/router';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';

import {GlobalProvider} from '../../../global';

declare var $: any;

export class MissionItem {
    name = '';
    qty = null;
    bas = null;
    description = '';

    constructor(name: string = '') {
        this.name = name;
    }
}

@Component({
    selector: 'app-pda-new',
    templateUrl: './pda-new.component.html',
    styleUrls: [
        './pda-new.component.scss',
    ]
})
export class PdaNewComponent implements OnInit {

    form: FormGroup;

    error = false;
    loading = false;
    submitted = false;
    ships: any[];
    ship: any;
    customer: any;
    date = new Date();

    showCreateShipModal = false;
    showSelectShipModal = false;
    showCreateCustomerModal = false;
    showSelectCustomerModal = false;

    operation_patterns: any[];
    results: any;
    results2: any;
    lastkeydown2 = 0;

    mission_types: any[];
    curs = [{name: 'EUR'}, {name: 'USD'}, {name: 'F CFA'}];
    folder;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @ViewChild('shipChoiceModal') shipChoiceModal;

    constructor(private formBuilder: FormBuilder, private apiProvider: ApiService, private authService: AuthenticationService,
                toasterService: ToasterService, private router: Router, private route: ActivatedRoute, private global: GlobalProvider) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.initForm();
        this.fetchFolder(this.route.snapshot.params['folderId']);
        this.fecthOperationPatterns();
        this.fetchMissionTypes();
        //
    }


    fetchFolder(id) {
        this.apiProvider.getFolder(id)
            .pipe(first())
            .subscribe((folder: any[]) => {
                this.folder = folder[0];
                this.f.currency.setValue(this.folder.currency);
            }, err => {
                console.log(err);
                this.showSuccess('error', 'Notification', err.error.message);
                this.router.navigate(['/pda/liste']);
            });
    }
    fetchMissionTypes() {
        this.apiProvider.getMissionTypes()
            .pipe(first())
            .subscribe((data: any[]) => {
                this.mission_types = data;
                console.log(this.mission_types);
            });
    }
    fecthOperationPatterns() {
        this.apiProvider.getOperationPatternsNames()
            .pipe(first())
            .subscribe(
                (operation_patterns: any[]) => {
                    // ..
                    this.operation_patterns = operation_patterns;
                }
            );
    }

    initForm() {
        this.form = this.formBuilder.group({
            currency: [null, Validators.required],
            missions: this.formBuilder.array([])
        });
    }

    onSubmit() {
        this.submitted = true;
        this.toggleLoading();

        if (this.form.invalid) {
            console.log(this.form);
            this.toggleLoading();
            return ;
        }

        const missions = this.missions.getRawValue();
        console.log(missions);
        const length = missions.length;

        if (!length) {
            this.toggleLoading();
            this.showSuccess('warning', 'Notification', 'Le PDA ne contient aucune misison! Veuillez en ajouter.');
            return;
        }
        for (let i = 0; i < length; ++i) {
            // missions[i]['name'] = document.getElementById(`dynamicMissions${i}`)['value'];
            for (let j = 0; j < missions[i]['items'].length; ++j) {
                console.log(missions[i]['items'][j]['name'], document.getElementById(`dynamicServices${i}${j}`)['value']);
                missions[i]['items'][j]['name'] = document.getElementById(`dynamicServices${i}${j}`)['value'];
            }
        }

        const pdaData = {
            folder_id: this.folder.id,
            currency: this.f.currency.value,
            missions: missions,
            user_id: this.authService.currentUserObject['id'],
        };
        console.log(pdaData);

        this.apiProvider.savePda(pdaData)
            .pipe(first())
            .subscribe(pda => {
                console.log(pda);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('success', 'Notification', 'PDA enregistré avec succès!');
                this.form.reset();
                this.router.navigate([`/pda/editer/${pda[0].id}`]);
            }, err => {
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('error', 'Notification', err.error.message);
                console.log(err.error.message);
            });
    }

    get f() { return this.form.controls; }

    get missions(): FormArray {
        return this.form.get('missions') as FormArray;
    }
    getMissionItems(mission): FormArray {
        return mission.get('items') as FormArray;
    }

    removeMission(mission) {
        const i = this.missions.controls.indexOf(mission);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            this.missions.controls.splice(i, 1);
            const items = this.form.get('missions') as FormArray;
            const data = {missions: items};
            // this.updateForm(data);
        }
    }

    addMissionItem(missionId, mission) {
        console.log(mission);
        console.log(this.missions);

        mission.controls.items.push(this.formBuilder.group(new MissionItem()));

        console.table(mission['controls']);
    }
    removeMissionItem(mission, item) {
        console.log(mission);
        const i = mission.controls.items.controls.indexOf(item);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            mission.controls.items.controls.splice(i, 1);
        }
    }

    toggleLoading() {
        this.loading = !this.loading;
    }

    listener(i) {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${i - 1}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 500);
    }

    getServices($event, i, j) {
        console.log(i);
        const name = (<HTMLInputElement>document.getElementById(`dynamicServices${i}${j}`)).value;
        this.results = [];

        if (name.length > 0) {
            if ($event.timeStamp - this.lastkeydown2 > 200) {
                this.results = this.searchFromArray(this.operation_patterns, name.toUpperCase());
                $(`#dynamicServices${i}${j}`).autocomplete({
                    source: this.results,
                    messages: {
                        noResults: '',
                        results: function () { }
                    }
                });
            }
        }
    }

    searchFromArray(arr, regex) {
        const matches = [];
        let i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].match(regex)) {
                matches.push(arr[i]);
            }
        }
        return matches;
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    genTemplateFor(missionType) {
        const i = this.missions.length;

        this.missions.push(this.formBuilder.group({
            name: [missionType.name, Validators.required],
            mission_type_id: [missionType.id],
            items: this.formBuilder.array([])
        }));

        // creating ops array
        for (let k = 0; k < missionType.operation_patterns.length; ++k) {

            // @ts-ignore
            const j = this.missions.controls[i].controls.items.length;

            // @ts-ignore
            this.missions.controls[i].controls
                .items.push(this.formBuilder.group(new MissionItem(missionType.operation_patterns[k].name)));

        }
        // console.log(this.missions);

        this.listener(this.missions.length);
    }
}
