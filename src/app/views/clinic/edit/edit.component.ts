import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit-clinic',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    providers: [ToasterService]
})
export class EditClinicComponent implements OnInit {

    clinicForm: FormGroup;
    clinic: any;
    error = '';
    submitted = false;
    loading = false;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private toasterService: ToasterService, private route: ActivatedRoute, private router: Router) {
    }

    get f() { return this.clinicForm.controls; }

    ngOnInit() {
        this.getClinic(this.route.snapshot.params['id']);
        this.clinicForm = this.formBuilder.group({
            name: ['', Validators.required],
            tel: [''],
            address: [''],
            email: ['']
        });
    }

    getClinic(clinicId) {
        this.apiService.getClinic(clinicId)
            .pipe(first())
            .subscribe((clinic: any[]) => {
                this.clinic = clinic;
                // Setting values
                this.clinicForm.setValue({
                    name: this.clinic.name,
                    tel: this.clinic.tel,
                    dueDateDelay: this.clinic.dueDateDelay,
                    address: this.clinic.address,
                    description: this.clinic.description
                });
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/clinics/list']);
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.clinicForm.invalid) {
            return;
        }
        this.loading = true;
        const clinicData = {
            name: this.f.name.value,
            tel: this.f.tel.value,
            dueDateDelay: this.f.dueDateDelay.value,
            address: this.f.address.value,
            description: this.f.description.value,
        };
        this.apiService.editClinic(this.clinic.id, clinicData)
            .pipe(first())
            .subscribe(clinic => {
                    console.log(clinic);
                    this.error = '';
                    this.loading = false;
                    this.submitted = false;
                    this.showSuccess();
                },
                error => {
                    console.log(error);
                    this.error = error;
                    this.loading = false;
                });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Client modifié avec succès!');
    }
}
