import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClinicRoutingModule } from './clinic-routing.module';
import {ClinicComponent} from './clinic.component';
import {EditClinicComponent} from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule} from 'angular2-toaster';
import {DataTableModule} from 'angular2-datatable';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {SharedModule} from '../../shared.module';
import {ModalsModule} from '../modals';
import {AddClinicModule} from '../modals/add-clinic/add-clinic.module';

@NgModule({
    declarations: [
        ClinicComponent,
        EditClinicComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        ClinicRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        DataTableModule,
        SweetAlert2Module,
        ModalsModule,
        AddClinicModule,
    ]
})
export class ClinicModule { }
