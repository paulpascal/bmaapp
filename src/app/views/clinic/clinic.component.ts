import {Component, OnInit, ViewChild} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {first} from 'rxjs/operators';
import {ApiService} from '../../_services';
import {Router} from '@angular/router';

@Component({
    selector: 'app-clinic',
    templateUrl: './clinic.component.html',
    styleUrls: ['./clinic.component.scss'],
    providers: [ToasterService]
})
export class ClinicComponent implements OnInit {

    error: any;
    public clinics: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    showCreateClinicModal = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fecth();
    }

    public editClinic(clinic) {
        this.router.navigate([`/clinics/edit/${clinic.id}`]);
    }

    public confirmDeleteClinic(clinic) {
        this.deleteSwal.options = {
            title: 'Supprimer cet clinic?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteClinic(clinic);
            }
        };
        this.deleteSwal.show();
    }

    deleteClinic(clinic) {
        this.apiService.deleteClinic(clinic.id)
            .subscribe(() => {
                const index = this.clinics.indexOf(clinic, 0);
                if (index > -1) {
                    this.clinics.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Clinic supprimé avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    toggleCreateClinic = () => {
        this.showCreateClinicModal = !this.showCreateClinicModal;
    }
    setClinic($event) {
        console.log('Clinic got: ', $event);
        this.fecth();
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getClinics()
            .pipe(first())
            .subscribe(
                (clinics: any[]) => {
                    // ..
                    this.clinics = clinics;
                    this.loading = false;
                }
            );
    }
}
