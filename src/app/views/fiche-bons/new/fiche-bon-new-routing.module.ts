import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FicheBonNewComponent } from './fiche-bon-new.component';

const routes: Routes = [
  {
    path: '',
    component: FicheBonNewComponent,
    data: {
      title: 'Ajouter une nouvelle fiche de bons'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FicheBonNewRoutingModule {}
