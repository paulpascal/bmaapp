import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {DatePipe} from '@angular/common';

@Component({
    templateUrl: 'fiche-bon-new.component.html',
    styleUrls: [
        '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss',
        '../../../../scss/vendors/ng-select/ng-select.scss',
        '../../../../../node_modules/ladda/dist/ladda-themeless.min.css',
        '../../../../scss/vendors/toastr/toastr.scss'
    ],
    providers: [ToasterService],
    encapsulation: ViewEncapsulation.None
})
export class FicheBonNewComponent implements OnInit {

    // Datepicker
    minDate = new Date(2017, 5, 10);
    maxDate = new Date(2018, 9, 15);
    bsValue: Date = new Date();
    bsRangeValue: any = [new Date(2017, 7, 4), new Date(2017, 7, 20)];
    // ..
    ships: any[];
    folders: any[];
    investmentTypes: any[];
    items: any[] = [];
    itemsFoldersNumber: any[] = [];
    amount = 0;

    // Forms
    sheetForm: FormGroup;

    // trigger-variable for Ladda
    loading = false;
    submitted = false;
    showCreateShipModal = false;
    error = '';

    private toasterService: ToasterService;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(
        private formBuilder: FormBuilder, toasterService: ToasterService, private apiService: ApiService,
        private authService: AuthenticationService
    ) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.fecthShips();
        this.fetchInvestmentType();
        // ..
        this.sheetForm = this.formBuilder.group({
            date: [this.bsValue, Validators.required],
            from: [null, Validators.required],
            to: [null, Validators.required],
            checkOrder: [null, Validators.required],
            type: [null, Validators.required],
            ship: [null],
            folder: [null],
            description: [null],
            amountRequested: [null],
            amountAwarded: [null],
            observations: [null],
        });

    }

    toggleLoading() {
        this.loading = !this.loading;
    }
    toggleCreateShip = () => {
        this.showCreateShipModal = !this.showCreateShipModal;
    }

    addItem() {
        if (this.g.amountRequested.value && this.g.amountAwarded.value && this.g.description.value) {
            this.itemsFoldersNumber.push(this.g.folder.value.numberString);
            this.items.push({
                folder_id: this.g.folder.value.id,
                description: this.g.description.value,
                amountRequested: this.g.amountRequested.value,
                amountAwarded: this.g.amountAwarded.value,
                observations: this.g.observations.value,
            });
            this.clearItem();
        } else {
            if (!this.g.amountRequested.value) {
                alert('Entrer le montant demandé.');
            } else if (!this.g.description.value) {
                alert('Entrer la description.');
            } else if (!this.g.amountAwarded.value) {
                alert('Entrer le montant accordé.');
            }
        }
    }

    clearItem() {
        this.g.folder.setValue(null);
        this.g.description.setValue(null);
        this.g.amountRequested.setValue(null);
        this.g.amountAwarded.setValue(null);
        this.g.observations.setValue(null);
    }

    // convenience getter for easy access to form fields
    get g() { return this.sheetForm.controls; }

    setShip($event) {
        console.log('Ship got: ', $event);
        this.fecthShips();
        this.sheetForm.controls['ship'].setValue($event);
    }

    onFormSubmit() {
        this.submitted = true;

        if (this.sheetForm.invalid) {
            console.log(this.sheetForm);
            return ;
        }

        this.toggleLoading();
        // ..
        const pipe = new DatePipe('en-US');
        const formattedDate = pipe.transform(new Date(this.g.date.value), 'yyyy-MM-dd hh:mm:ss');

        const sheetData = {
            from: this.g.from.value,
            to: this.g.to.value,
            date: formattedDate,
            ship_id: this.g.ship.value,
            checkOrder: this.g.checkOrder.value,
            investment_type_id: this.g.type.value,
            user_id: this.authService.currentUserObject.id,
            items: this.items
        };

        this.apiService.saveInvestementSheet(sheetData)
            .pipe(first())
            .subscribe(sheet => {
                console.log(sheet);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('success', 'Notification', 'Fiche d\'investissement enregistrée avec succès!');
                this.sheetForm.reset();
                this.items = [];
                this.ngOnInit();
            }, err => {
                console.log(err.error.message);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('error', 'Notification', err.error.message);
            });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    fecthShips() {
        this.apiService.getShips()
            .pipe(first())
            .subscribe(
                (ships: any[]) => {
                    // ..
                    this.ships = ships;
                }
            );
    }

    fecthFoldersForShip(shipId) {
        this.apiService.getFoldersForShip(shipId)
            .pipe(first())
            .subscribe(
                (folders: any[]) => {
                    // ..
                    this.folders = folders;
                }
            );
    }

    fetchInvestmentType() {
        this.apiService.getInvestementTypes()
            .pipe(first())
            .subscribe(
                (types: any[]) => {
                    // ..
                    this.investmentTypes = types;
                }
            );
    }

    shipSelected(ev) {
        console.log(ev);
        this.fecthFoldersForShip(ev.id);
        this.clearItem();
    }
}
