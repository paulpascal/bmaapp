import { Component, OnInit } from '@angular/core';
import * as JSPDF from 'jspdf';
import * as writtenForm from 'written-number';
import html2canvas from 'html2canvas';
import {first} from 'rxjs/operators';
import {ApiService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
    selector: 'app-show',
    templateUrl: './fiche-bon-show.component.html',
    styleUrls: ['./fiche-bon-show.component.scss']
})
export class FicheBonShowComponent implements OnInit {

    sheet: any;
    totalReq;
    totalAwarded;

    constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        this.getSheet(this.route.snapshot.params['id']);
    }

    public print() {
        const data = document.getElementById('printable');
        html2canvas(data).then(canvas => {
            // Few necessary setting options
            const imgWidth = 210;
            const pageHeight = 295;
            const imgHeight = canvas.height * imgWidth / canvas.width;
            const heightLeft = imgHeight;

            const contentDataURL = canvas.toDataURL('image/png');
            const pdf = new JSPDF('p', 'mm', 'a4'); // A4 size page of PDF
            const position = 2;
            pdf.setFontSize(25);
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
            pdf.save('fiche_bons' + this.getMoment() + '.pdf'); // Generated PDF
        });
    }

    computeTotals() {
        this.totalReq = 0;
        this.totalAwarded = 0;

        this.sheet.items.forEach(item => {
            this.totalReq += parseFloat(item.amountRequested);
            this.totalAwarded += parseFloat(item.amountAwarded);
        });
    }

    getSheet(sheetId) {
        this.apiService.getInvestementSheet(sheetId)
            .pipe(first())
            .subscribe((sheet: any) => {
                this.sheet = sheet;
                this.computeTotals();
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/fiche-bons/liste']);
                }
            });
    }

    getMoment() {
        return Date.now();
    }

    toLetter(digit) {
        return writtenForm(digit, { lang: 'fr', noAnd: true });
    }
}
