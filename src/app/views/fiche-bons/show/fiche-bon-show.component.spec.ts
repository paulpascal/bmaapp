import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheBonShowComponent } from './fiche-bon-show.component';

describe('FicheBonShowComponent', () => {
  let component: FicheBonShowComponent;
  let fixture: ComponentFixture<FicheBonShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheBonShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheBonShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
