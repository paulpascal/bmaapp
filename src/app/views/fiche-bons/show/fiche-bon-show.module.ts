import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FicheBonShowRoutingModule } from './fiche-bon-show-routing.module';
import {FicheBonShowComponent} from './fiche-bon-show.component';
import {NgxPrintModule} from 'ngx-print';

@NgModule({
    declarations: [FicheBonShowComponent],
    imports: [
        CommonModule,
        FicheBonShowRoutingModule,
        NgxPrintModule
    ]
})
export class FicheBonShowModule { }
