import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FicheBonShowComponent} from './fiche-bon-show.component';

const routes: Routes = [
    {
        path: '',
        component: FicheBonShowComponent,
        data: {
            title: 'Afficher une fiche de bons'
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FicheBonShowRoutingModule { }
