import { NgModule } from '@angular/core';

// Routing
import { FicheBonsRoutingModule } from './fiche-bons-routing.module';

@NgModule({
  imports: [
    FicheBonsRoutingModule
  ],
  declarations: []
})
export class FicheBonsModule { }
