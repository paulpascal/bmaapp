import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'liste',
        pathMatch: 'full',
    },
    {
        path: '',
        data: {
            title: 'Fiches de bons'
        },
        children: [
            {
                path: 'ajouter',
                loadChildren: './new/fiche-bon-new.module#FicheBonNewModule'
            },
            {
                path: 'detail/:id',
                loadChildren: './show/fiche-bon-show.module#FicheBonShowModule'
            },
            {
                path: 'liste',
                loadChildren: './list/fiche-bon-list.module#FicheBonListModule'
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FicheBonsRoutingModule {}
