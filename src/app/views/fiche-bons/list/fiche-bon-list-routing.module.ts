import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FicheBonListComponent } from './fiche-bon-list.component';

const routes: Routes = [
  {
    path: '',
    component: FicheBonListComponent,
    data: {
      title: 'Liste des fiches de bons'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FicheBonListRoutingModule {}
