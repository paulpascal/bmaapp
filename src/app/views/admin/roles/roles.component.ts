import {Component, OnInit, ViewChild} from '@angular/core';

import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

import { SwalComponent } from '@toverux/ngx-sweetalert2';
import {ToasterConfig, ToasterService} from 'angular2-toaster';


@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss'],
    providers: [ToasterService]
})
export class RolesComponent implements OnInit {

    error: any;
    public roles: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fecth();
    }

    public goToAddRole() {
        this.router.navigate(['/admin/roles/new']);
    }

    public editRole(role) {
        this.router.navigate([`/admin/roles/edit/${role.id}`]);
    }

    public confirmDeleteRole(role) {
        this.deleteSwal.options = {
            title: 'Supprimer cet rôle?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteRole(role);
            }
        };
        this.deleteSwal.show();
    }

    deleteRole(role) {
        this.apiService.deleteRole(role.id)
            .subscribe(() => {
                const index = this.roles.indexOf(role, 0);
                if (index > -1) {
                    this.roles.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Rôle supprimé avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getRoles()
            .pipe(first())
            .subscribe(
                (roles: any[]) => {
                    // ..
                    this.roles = roles;
                    this.loading = false;
                }
            );
    }
}
