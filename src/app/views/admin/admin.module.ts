import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {DataTableModule} from 'angular2-datatable';
import {DataFilterPipe} from '../../_helpers';
import {AdminComponent} from './admin.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AvatarModule} from 'ngx-avatar';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterModule} from 'angular2-toaster';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import {MomentModule} from 'ngx-moment';

import {UsersComponent} from './users/users.component';
import {UsersAddComponent} from './users-add/users-add.component';
import {RolesComponent} from './roles/roles.component';
import {RolesAddComponent} from './roles-add/roles-add.component';
import {UserEditComponent} from './users-add/user-edit.component';
import {RolesEditComponent} from './roles-add/roles-edit.component';
import {SharedModule} from '../../shared.module';

@NgModule({
    declarations: [
        AdminComponent,
        UsersComponent,
        UsersAddComponent,
        UserEditComponent,
        RolesComponent,
        RolesAddComponent,
        RolesEditComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        AdminRoutingModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        AvatarModule,
        NgSelectModule,
        ToasterModule,
        SweetAlert2Module,
        MomentModule
    ]
})
export class AdminModule {
}
