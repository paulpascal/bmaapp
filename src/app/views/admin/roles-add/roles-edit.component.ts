import { Component, OnInit } from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-roles-edit',
    templateUrl: './roles-edit.component.html',
    styleUrls: ['./roles-edit.component.scss'],
    providers: [ToasterService]
})
export class RolesEditComponent implements OnInit {

    roleForm: FormGroup;
    role: any;
    error = '';
    submitted = false;
    loading = false;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private toasterService: ToasterService, private route: ActivatedRoute, private router: Router) {
    }

    get f() { return this.roleForm.controls; }

    ngOnInit() {
        this.getRole(this.route.snapshot.params['id']);
        this.roleForm = this.formBuilder.group({
            'label': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
            'description': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
        });
    }

    getRole(roleId) {
        this.apiService.getRole(roleId)
            .pipe(first())
            .subscribe((role: any[]) => {
                this.role = role;
                // Setting values
                this.roleForm.setValue({
                    'label': this.role.label,
                    'description': this.role.description
                });
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/admin/roles/list']);
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.roleForm.invalid) {
            return;
        }
        this.loading = true;
        const roleData = {
            label: this.f.label.value,
            description: this.f.description.value,
        };
        this.apiService.editRole(this.role.id, roleData)
            .pipe(first())
            .subscribe(role => {
                    console.log(role);
                    this.error = '';
                    this.loading = false;
                    this.submitted = false;
                    this.showSuccess();
                },
                error => {
                    console.log(error);
                    this.error = error;
                    this.loading = false;
                });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Rôle modifié avec succès!');
    }
}
