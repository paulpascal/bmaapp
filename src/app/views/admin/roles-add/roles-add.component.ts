import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
import {NgSelectConfig} from '@ng-select/ng-select';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {Router} from '@angular/router';

@Component({
    selector: 'app-roles-add',
    templateUrl: './roles-add.component.html',
    styleUrls: ['./roles-add.component.scss'],
    providers: [ToasterService]
})
export class RolesAddComponent implements OnInit {
    roleForm: FormGroup;
    error = '';
    submitted = false;
    loading = false;
    roles: any[];

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private toasterService: ToasterService) {
    }

    get f() { return this.roleForm.controls; }

    ngOnInit() {
        this.roleForm = this.formBuilder.group({
           'label': ['', Validators.compose([Validators.required, Validators.minLength(3)])],
           'description': ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        });
        this.apiService.getRoles()
            .pipe(first())
            .subscribe((roles: any[]) => {
              this.roles = roles;
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.roleForm.invalid) {
            return;
        }
        this.loading = true;
        const roleData = {
            label: this.f.label.value,
            description: this.f.description.value,
        };
        this.apiService.saveRole(roleData)
            .pipe(first())
            .subscribe(role => {
                console.log(role);
                this.error = null;
                this.loading = false;
                this.submitted = false;
                this.showSuccess();
                this.roleForm.reset();
            },
            error => {
                console.log(error);
                this.error = error;
                this.loading = false;
            });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Rôle ajouté avec succès!');
    }
}
