import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
import {NgSelectConfig} from '@ng-select/ng-select';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

@Component({
    selector: 'app-users-add',
    templateUrl: './users-add.component.html',
    styleUrls: ['./users-add.component.scss'],
    providers: [ToasterService]
})
export class UsersAddComponent implements OnInit {
    userForm: FormGroup;
    error = '';
    submitted = false;
    loading = false;
    roles: any[];

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private config: NgSelectConfig, private toasterService: ToasterService
    ) {
        this.config.notFoundText = 'Aucun role trouvé';
    }

    get f() { return this.userForm.controls; }

    static passwordMatchValidator(control: AbstractControl) {
        const password: string = control.get('password').value; // get password from our password form control
        const confirmPassword: string = control.get('confirmation').value; // get password from our confirmPassword form control
        // compare is the password math
        if (password !== confirmPassword) {
            // if they don't match, set an error in our confirmPassword form control
            control.get('confirmation').setErrors({ NoPassswordMatch: true });
        }
    }

    ngOnInit() {
        this.userForm = this.formBuilder.group({
           'firstName': ['', Validators.compose([Validators.required, Validators.minLength(3)])],
           'lastName': ['', Validators.compose([Validators.required, Validators.minLength(3)])],
           'username': ['', Validators.compose([Validators.required])],
           'password': ['', Validators.compose([Validators.required])],
           'confirmation': ['', Validators.compose([Validators.required])],
           'roles': [null, Validators.compose([Validators.required])],
           'is_active': [true, Validators.compose([Validators.required])],
           'sex': ['M', Validators.compose([Validators.required])],
        }, { validator: UsersAddComponent.passwordMatchValidator });
        this.apiService.getRoles()
            .pipe(first())
            .subscribe((roles: any[]) => {
              this.roles = roles;
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.userForm.invalid) {
            return;
        }
        this.loading = true;
        const userData = {
            username: this.f.username.value,
            email: this.f.username.value,
            firstName: this.f.firstName.value,
            lastName: this.f.lastName.value,
            password: this.f.password.value,
            roles: this.f.roles.value,
            is_active: this.f.is_active.value,
            sex: this.f.sex.value
        };
        console.log(userData);
        this.apiService.saveUser(userData)
            .pipe(first())
            .subscribe(user => {
                console.log(user);
                this.error = null;
                this.loading = false;
                this.submitted = false;
                this.showSuccess();
                this.userForm.reset();
            },
            error => {
                console.log(error);
                this.error = error;
                this.loading = false;
            });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Utilisateur ajouté avec succès!');
    }
}
