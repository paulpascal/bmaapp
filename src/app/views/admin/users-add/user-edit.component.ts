import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, DataStorageService} from '../../../_services';
import {NgSelectConfig} from '@ng-select/ng-select';
import {ActivatedRoute, Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {UsersAddComponent} from './users-add.component';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.scss'],
    providers: [ToasterService]
})
export class UserEditComponent implements OnInit {
    userForm: FormGroup;
    user: any;
    error = '';
    submitted = false;
    loading = false;
    roles: any[];

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private config: NgSelectConfig, private toasterService: ToasterService,
                private dataStorage: DataStorageService, private route: ActivatedRoute, private router: Router) {
        this.config.notFoundText = 'Aucun role trouvé';
    }

    get f() { return this.userForm.controls; }

    ngOnInit() {
        this.getUser();
        this.userForm = this.formBuilder.group({
            'firstName': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
            'lastName': [null, Validators.compose([Validators.required, Validators.minLength(3)])],
            'username': [null, Validators.compose([Validators.required])],
            'password': [null, Validators.compose([])],
            'confirmation': [null, Validators.compose([])],
            'roles': [null, Validators.compose([Validators.required])],
            'is_active': [null, Validators.compose([Validators.required])],
            'sex': [null, Validators.compose([Validators.required])],
        }, { validator: UsersAddComponent.passwordMatchValidator });
        this.apiService.getRoles()
            .pipe(first())
            .subscribe((roles: any[]) => {
                this.roles = roles;
            });
    }


    getUser() {
        this.apiService.getUser(this.route.snapshot.params['id'])
            .pipe(first())
            .subscribe((user: any) => {
                this.user = user;
                const roles = user['roles'].map(r => r.id);
                console.log(roles);
                // Setting data
                this.userForm.setValue({
                    'firstName': this.user.firstName,
                    'lastName': this.user.lastName,
                    'username': this.user.username,
                    'password': '',
                    'confirmation': '',
                    'roles': roles,
                    'is_active': this.user.is_active,
                    'sex': this.user.sex,
                });
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/admin/users/list']);
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.userForm.invalid) {
            return;
        }
        this.loading = true;

        const userData = {
            username: this.f.username.value,
            email: this.f.username.value,
            firstName: this.f.firstName.value,
            lastName: this.f.lastName.value,
            roles: this.f.roles.value,
            is_active: this.f.is_active.value,
            sex: this.f.sex.value
        };
        this.apiService.editUser(this.user.id, userData)
            .pipe(first())
            .subscribe(user => {
                    console.log(user);
                    this.error = '';
                    this.loading = false;
                    this.submitted = false;
                    this.showSuccess();
                },
                error => {
                    console.log(error);
                    this.error = error;
                    this.loading = false;
                });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Utilisateur modifié avec succès!');
    }

    goToUserList() {}
}
