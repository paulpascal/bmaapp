import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {UsersAddComponent} from './users-add/users-add.component';
import {RolesComponent} from './roles/roles.component';
import {RolesAddComponent} from './roles-add/roles-add.component';
import {UserEditComponent} from './users-add/user-edit.component';
import {RolesEditComponent} from './roles-add/roles-edit.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'users/list',
        pathMatch: 'full',
    },
    {
        path: '', children: [
            { path: 'users/list', component: UsersComponent },
            { path: 'users/new', component: UsersAddComponent },
            { path: 'users/edit/:id', component: UserEditComponent },
            { path: 'roles/list', component: RolesComponent },
            { path: 'roles/new', component: RolesAddComponent },
            { path: 'roles/edit/:id', component: RolesEditComponent },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
