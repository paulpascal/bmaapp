import {Component, OnInit, ViewChild} from '@angular/core';

import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

import { SwalComponent } from '@toverux/ngx-sweetalert2';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

import * as moment from 'moment';
moment.locale('fr');

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss'],
    providers: [ToasterService]
})
export class UsersComponent implements OnInit {

    error: any;
    public users: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fecth();
    }

    public toInt(num: string) {
        return +num;
    }

    public sortByWordLength(a: any) {
        return a.name.length;
    }

    public goToUserDetsil(user) {
        this.router.navigate([`/profil/utilisateur/${user.id}`]);
    }

    public goToAddUser() {
        this.router.navigate(['/admin/users/new']);
    }

    public editUser(user) {
        this.router.navigate([`/admin/users/edit/${user.id}`]);
    }

    public confirmDeleteUser(user) {
        this.deleteSwal.options = {
            title: 'Supprimer cet utilisateur?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteUser(user);
            }
        };
        this.deleteSwal.show();
    }

    deleteUser(user) {
        this.apiService.deleteUser(user.id)
            .subscribe(() => {
                const index = this.users.indexOf(user, 0);
                if (index > -1) {
                    this.users.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Utilisateur supprimé avec succès!');
            }, err => {
                console.log(err.error.message);
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getUsers()
            .pipe(first())
            .subscribe(
                (users: any[]) => {
                    // ..
                    this.users = users;
                    this.loading = false;
                }
            );
    }
}
