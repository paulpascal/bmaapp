import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import {EditComponent} from './edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {ModalsModule} from '../../modals';
import {FileUploadModule} from 'ng2-file-upload';

@NgModule({
    declarations: [
        EditComponent
    ],
    imports: [
        CommonModule,
        EditRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        NgSelectModule,
        BsDropdownModule,
        BsDatepickerModule.forRoot(),
        ModalsModule,
        FileUploadModule
    ],
    providers: [
        ToasterService,
    ]
})
export class EditModule { }
