import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import {MissionItem} from '../new/new.component';
import {ActivatedRoute, Router} from '@angular/router';
import {pipe} from 'rxjs';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';
import {GlobalProvider} from '../../../global';
import {DatePipe} from '@angular/common';

declare const $;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss', '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditComponent implements OnInit {

    form: FormGroup;

    error = false;
    loading = false;
    submitted = false;
    ships: any[];
    ship: any;
    folder: any;
    customer: any;
    date = new Date();
    curs = [{name: 'EUR'}, {name: 'USD'}, {name: 'F CFA'}];

    accounting = false;
    isFolderCloser = false;
    isFolderValidator = false;
    isInvoiceValidator = false;
    showCreateShipModal = false;
    showSelectShipModal = false;
    showCreateCustomerModal = false;
    showSelectCustomerModal = false;

    operation_patterns: any[];
    mission_types: any[];
    results: any;
    results2: any;
    lastkeydown2 = 0;
    isANotToShowFile = {};

    savedMissions: any[];

    uploaders = {};

    uploadUrl;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @ViewChild('shipChoiceModal') shipChoiceModal;

    constructor(private formBuilder: FormBuilder, private apiProvider: ApiService, private authService: AuthenticationService,
                toasterService: ToasterService, private route: ActivatedRoute, private router: Router, private global: GlobalProvider) {
        this.toasterService = toasterService;
        this.accounting = this.authService.hasRole('Accounting');
        this.isFolderCloser = this.authService.hasRole('FolderCloser');
        this.isFolderValidator = this.authService.hasRole('FolderValidator');
        this.isInvoiceValidator = this.authService.hasRole('InvoiceValidator');

    }

    ngOnInit() {
        this.initForm();
        this.fetchFolder(this.route.snapshot.params['id']);
        this.fetchSips();
        this.fetchMissionTypes();
        this.fecthOperationPatterns();
        //
    }

    fetchFolder(id) {
        this.apiProvider.getFolder(id)
            .pipe(first())
            .subscribe((folder: any[]) => {
                this.folder = folder[0];
                this.form.controls['ship'].setValue(this.folder.ship_id);
                this.form.controls['customer'].setValue(this.folder.customer_id);
                this.form.controls['currency'].setValue(this.folder.currency);
                this.form.controls['customerRef'].setValue(this.folder.customerRef);
                this.form.controls['shipArrivalDate'].setValue(this.folder.shipArrivalDate);
                this.form.controls['shipDepartureDate'].setValue(this.folder.shipDepartureDate);
                this.initComponent();
            }, err => {
                console.log(err);
                this.showSuccess('error', 'Notification', err.error.message);
                this.router.navigate(['/dossier/liste']);
            });
    }
    fetchSips() {
        this.apiProvider.getShips()
            .pipe(first())
            .subscribe((ships: any[]) => {
                this.ships = ships;
            });
    }
    fetchMissionTypes() {
        this.apiProvider.getMissionTypes()
            .pipe(first())
            .subscribe((data: any[]) => {
                this.mission_types = data;
                console.log(this.mission_types);
            });
    }
    fecthOperationPatterns() {
        this.apiProvider.getOperationPatternsNames()
            .pipe(first())
            .subscribe(
                (operation_patterns: any[]) => {
                    // ..
                    this.operation_patterns = operation_patterns;
                }
            );
    }

    initForm() {
        this.form = this.formBuilder.group({
            ship: [null, Validators.required],
            customer: [null, Validators.required],
            customerRef: [null, Validators.required],
            currency: [null, Validators.required],
            openDate: [null],
            closeDate: [null],
            shipArrivalDate: [null],
            shipDepartureDate: [null],
            missions: this.formBuilder.array([])
        });
    }
    initComponent() {
        this.savedMissions = this.folder.missions;
        for (let i = 0; i < this.savedMissions.length; ++i) {
            const mission = this.savedMissions[i];
            // ..
            this.missions.controls.push(this.formBuilder.group({
                name: mission.name,
                isArchived: mission.isArchived,
                id: mission.id,
                items: this.formBuilder.array([]),
                user: mission.user
            }));

            console.log(this.missions);

            for (let j = 0; j < mission.operations.length; ++j) {

                this.addUploader(j, i);
                // @ts-ignore
                this.missions.controls[i].controls.items.push(this.formBuilder.group(mission.operations[j]));
                // @ts-ignore
                this.missions.controls[i].controls.items.controls[j].controls.files.setValue([...mission.operations[j].files]);
                // tslint:disable-next-line:max-line-length
                console.log(mission.operations[j]);
                // @ts-ignore
                console.log(this.missions.controls[i].controls.items.controls[j]);
            }

            this.listener(i + 1);
        }
    }

    onSubmit() {
        console.log(this.f);
        this.submitted = true;

        if (this.form.invalid) {
            console.log(this.form);

            if (this.form.controls.customerRef.invalid) {
                return;
            }
            if (this.accounting && (this.form.controls.currency.invalid) ) {
                return ;
            }
        }

        if (this.accounting) {
            for (const mission of this.missions.controls) {
                // return ;
                // @ts-ignore
                for (const item of mission.controls.items.controls) {

                    if (item.controls.bas.errors) {
                        console.log(item);
                        return ;
                    }
                }
            }
        }

        this.toggleLoading();

        const items = this.missions.getRawValue();
        // items =  items.filter(element => !element.hasOwnProperty('id'));
        console.log(items);

        const length = items.length;

        for (let i = 0; i < length; ++i) {
            for (let j = 0; j < items[i]['items'].length; ++j) {
                console.log(items[i]['items'][j]['name'], document.getElementById(`dynamicServices${i}${j}`)['value']);
                items[i]['items'][j]['name'] = document.getElementById(`dynamicServices${i}${j}`)['value'];
            }
        }

        // tslint:disable-next-line:no-shadowed-variable
        const pipe = new DatePipe('en-US');

        const folderData = {
            ship_id: this.f.ship.value,
            currency: this.f.currency.value,
            customer_id: this.f.customer.value,
            customerRef: this.f.customerRef.value,
            shipArrivalDate: pipe.transform(new Date(this.f.shipArrivalDate.value), 'yyyy-MM-dd hh:mm:ss'),
            shipDepartureDate: pipe.transform(new Date(this.f.shipDepartureDate.value), 'yyyy-MM-dd hh:mm:ss'),
            missions: items,
            user_id: this.authService.currentUserObject['id'],
        };
        console.log(folderData);

        this.apiProvider.editFolder(this.folder.id, folderData)
            .pipe(first())
            .subscribe(folder => {
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('success', 'Notification', 'Dossier modifié avec succès!');
                this.form.reset();
                window.location.reload();
                // setTimeout(() => {
                //     this.ngOnInit();
                // }, 500);
            },
                    err => {
                console.log(err.error.message);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('error', 'Notification', err.error.message);
            });
    }

    get f() { return this.form.controls; }

    get missions(): FormArray {
        return this.form.get('missions') as FormArray;
    }
    getMissionItems(mission): FormArray {
        return mission.get('items') as FormArray;
    }

    // addMission() {
    //     this.missions.push(this.formBuilder.group({
    //         name: ['', Validators.required],
    //         items: this.formBuilder.array([])
    //     }));
    //     console.table(this.missions['controls']);
    //     this.listener(this.missions.length);
    // }

    removeMission(mission) {
        const i = this.missions.controls.indexOf(mission);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            // this.missions.controls.splice(i, 1);
            if (mission.controls.id) {
                this.apiProvider.deleteFolderMission(mission.controls.id.value)
                    .pipe(first())
                    .subscribe(resp => {
                        this.showSuccess('success', 'Notification', 'Mission supprimée avec succès.');
                        this.ngOnInit();
                    }, err => {
                        console.log(err);
                        this.showSuccess('error', 'Notification', 'Action impossible.');
                    });

            } else {
                this.missions.controls.splice(i, 1);
            }
        }
    }

    addMissionItem(mission, i) {
        // console.log(mission);
        // console.log(this.missions);
        const k = mission.controls.items.controls.length;
        this.addUploader(k, i);

        mission.controls.items.push(this.formBuilder.group(new MissionItem()));
        // console.table(mission['controls']);
    }
    removeMissionItem(mission, item) {
        const i = mission.controls.items.controls.indexOf(item);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            // mission.controls.items.controls.splice(i, 1);
            if (item.controls.id) {
                this.apiProvider.deleteFolderOperation(item.controls.id.value)
                    .pipe(first())
                    .subscribe(resp => {
                        this.showSuccess('success', 'Notification', 'Opération supprimée avec succès.');
                        this.ngOnInit();
                    }, err => {
                        console.log(err);
                        this.showSuccess('error', 'Notification', 'Action impossible.');
                    });

            } else {
                mission.controls.items.controls.splice(i, 1);
            }
        }
    }

    archiveMission(mitem) {
        // console.log(mitem.controls.id.value);
        this.apiProvider.archiveMission(mitem.controls.id.value)
            .pipe(first())
            .subscribe(folder => {
                    this.showSuccess('success', 'Notification', 'Mission archivée modifié avec succès!');
                    setTimeout(() => {
                        this.ngOnInit();
                    }, 500);
                },
                err => {
                    console.log(err.error.message);
                    this.toggleLoading();
                    this.showSuccess('error', 'Notification', err.error.message);
                });
    }

    setCustomer($event) {
        console.log('Customer got: ', $event);
        this.form.controls['customer'].setValue($event);
        this.apiProvider.getCustomer($event)
            .pipe(first())
            .subscribe(customer => {
                this.customer = customer;
            });
    }
    setShip($event) {
        console.log('Ship got: ', $event);
        this.form.controls['ship'].setValue($event);
        this.apiProvider.getShip($event)
            .pipe(first())
            .subscribe(ship => {
                this.ship = ship;
            });
    }

    toggleLoading() {
        this.loading = !this.loading;
    }
    toggleCreateShip = () => {
        if (this.showSelectShipModal) {
            this.toggleSelectShip();
        }
        this.showCreateShipModal = !this.showCreateShipModal;
    }
    toggleSelectShip = () => {
        this.showSelectShipModal = !this.showSelectShipModal;
    }

    toggleCreateCustomer = () => {
        if (this.showSelectCustomerModal) {
            this.toggleSelectCustomer();
        }
        this.showCreateCustomerModal = !this.showCreateCustomerModal;
    }
    toggleSelectCustomer = () => {
        this.showSelectCustomerModal = !this.showSelectCustomerModal;
    }

    listener(i) {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${i - 1}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 500);
    }

    getServices($event, i, j) {
        console.log(i);
        const name = (<HTMLInputElement>document.getElementById(`dynamicServices${i}${j}`)).value;
        this.results = [];

        if (name.length > 0) {
            if ($event.timeStamp - this.lastkeydown2 > 200) {
                this.results = this.searchFromArray(this.operation_patterns, name.toUpperCase());
                $(`#dynamicServices${i}${j}`).autocomplete({
                    source: this.results,
                    messages: {
                        noResults: '',
                        results: function () { }
                    }
                });
            }
        }
    }

    searchFromArray(arr, regex) {
        const matches = [];
        let i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].match(regex)) {
                matches.push(arr[i]);
            }
        }
        return matches;
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    fenceFolder() {
        this.apiProvider.fenceFolder(this.folder.id)
            .pipe((first()))
            .subscribe(folder => {
                this.router.navigate(['/dossier/liste']);
            });
    }
    reopenFolder() {
        this.apiProvider.reopenFolder(this.folder.id)
            .pipe((first()))
            .subscribe(folder => {
                this.ngOnInit();
            });
    }

    generateInvoice() {
        this.router.navigate(['/facture/ajouter/' + this.folder.id]);
    }

    hasError(i: number, j: number) {
        // @ts-ignore
        return this.submitted && this.f.missions.controls[i].controls.items.controls[j].controls.bas.errors;
    }

    getError(i: number, j: number) {
        // @ts-ignore
        return this.f.missions.controls[i].controls.items.controls[j].controls.bas.errors;
    }

    genTemplateFor(missionType) {
        const i = this.missions.length;

        this.missions.push(this.formBuilder.group({
            name: [missionType.name, Validators.required],
            mission_type_id: [missionType.id],
            items: this.formBuilder.array([])
        }));

        // creating ops array
        for (let k = 0; k < missionType.operation_patterns.length; ++k) {
            if (!k) {
                this.uploaders[i] = {};
            }
            // @ts-ignore
            const j = this.missions.controls[i].controls.items.length;
            // uploaders[mission_I][operation_J] = new FileUploader() --> chaque operation a son uploader
            // @ts-ignore
            this.uploaders[i][j] = new FileUploader({
                 url: this.global.uploadUrl(),
                method: 'POST',
                itemAlias: 'attached_files',
                isHTML5: true,
                authTokenHeader:  'authorization',
                authToken: `Bearer ${this.authService.currentUserObject.token}`,
                // tslint:disable-next-line:max-line-length
                headers: [
                    {name: 'Accept', value: 'application/json'},
                    { name: 'Access-Control-Allow-Origin', value: 'true' },

                ],
                autoUpload: true,
            });

            this.uploaders[i][j].onAfterAddingFile = (item => {
                item.withCredentials = false;
            });
            // @ts-ignore
            this.uploaders[i][j].onErrorItem
                = (item, response, status, headers) => this.onErrorItem(item, response, status, headers, i, j);
            // @ts-ignore
            this.uploaders[i][j].onSuccessItem
                = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers, i, j);

            // @ts-ignore
            this.missions.controls[i].controls
                .items.push(this.formBuilder.group(new MissionItem(missionType.operation_patterns[k].name)));

            console.log(k, this.uploaders);
        }
        // console.log(this.missions);

        this.listener(this.missions.length);
    }

    deleteDownloadedFile(missionIndex, operationId, file) {

        const fileId = file.id;
        // @ts-ignore
        let files = this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.value;

        console.log(files);
        console.log(fileId);

        this.apiProvider.deleteFile(fileId)
            .pipe(first())
            .subscribe((data: any[]) => {
                console.log(data);

                files = files.filter(function( obj ) {
                    return obj.id !== fileId;
                });
                // @ts-ignore
                this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.setValue([...files]);

                // finish
                // item.remove();

                // const index = files.indexOf(fileId, 0);
                // if (index > -1) {
                //     files.splice(index, 1);
                //     // @ts-ignore
                //     this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.setValue([...files]);
                // }
            });

        // @ts-ignore

    }

    deleteUploadedFile(missionIndex, operationId, item) {

        const fileId = item['uploadedId'];
        // @ts-ignore
        let files = this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.value;

        console.log(files);
        console.log(fileId);

        this.apiProvider.deleteFile(fileId)
            .pipe(first())
            .subscribe((data: any[]) => {
                console.log(data);

                files = files.filter(function( obj ) {
                    return obj.id !== fileId;
                });
                // @ts-ignore
                this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.setValue([...files]);

                // finish
                item.remove();

                // const index = files.indexOf(fileId, 0);
                // if (index > -1) {
                //     files.splice(index, 1);
                //     // @ts-ignore
                //     this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.setValue([...files]);
                // }
            });

        // @ts-ignore

    }

    onFileSelected(i, j) {
        this.uploaders[i][j].uploadAll();
    }

    onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders, i, j): any {
        const data = JSON.parse(response); // success server response
        console.log(data);

        // @ts-ignore
        if (this.missions.controls[i].controls.items.controls[j].controls.files.value) {
            // @ts-ignore
            // console.log('Here1', this.missions.controls[i].controls.items.controls[j].controls.files.value);
            // console.log('Here1', ...data.files);
            // @ts-ignore
            const old = this.missions.controls[i].controls.items.controls[j].controls.files.value;
            // @ts-ignore
            this.missions.controls[i].controls.items.controls[j].controls.files.setValue([...old, ...data.files]);
        } else {
            // @ts-ignore
            // console.log('Here2', ...data.files);
            // @ts-ignore
            this.missions.controls[i].controls.items.controls[j].controls.files.setValue([...data.files]);
        }
        // this.missions.controls[i].controls.items.controls[j].controls.files.value.push(...data.map(e => e.id));

        item['uploadedId'] = data.files[0].id;
        item['uploadedName'] = data.files[0].name;
        this.isANotToShowFile[data.files[0].id] = true;


        // @ts-ignore
        console.log(this.missions.controls[i].controls.items.controls[j]);
    }

    onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders, i, j): any {
        // const error = JSON.parse(response); // error server response
        console.log(response);
    }

    /**
     * @param k operation _index
     * @param k operation _index
     * @param i mission _index
     */
    addUploader(k, i) {
        console.log(k,  i);
        if (!k) {
            this.uploaders[i] = {};
        }
        // @ts-ignore
        const j = this.missions.controls[i].controls.items.length;
        // uploaders[mission_I][operation_J] = new FileUploader() --> chaque operation a son uploader
        // @ts-ignore
        this.uploaders[i][j] = new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            headers: [
                {name: 'Accept', value: 'application/json'},
                { name: 'Access-Control-Allow-Origin', value: 'true' },

            ],
            autoUpload: true,
        });

        this.uploaders[i][j].onAfterAddingFile = (item => {
            item.withCredentials = false;
        });
        // @ts-ignore
        this.uploaders[i][j].onErrorItem
            = (item, response, status, headers) => this.onErrorItem(item, response, status, headers, i, j);
        // @ts-ignore
        this.uploaders[i][j].onSuccessItem
            = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers, i, j);
    }

    genFileUrl(file) {
        return `${this.global.serverUrl}/uploads/${file.name}`;
    }
    genItemFileUrl(itemFile) {
        return `${this.global.serverUrl}/uploads/${itemFile['uploadedName']}`;
    }
}
