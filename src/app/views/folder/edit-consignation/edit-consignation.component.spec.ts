import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditConsignationComponent } from './edit-consignation.component';

describe('EditConsignationComponent', () => {
  let component: EditConsignationComponent;
  let fixture: ComponentFixture<EditConsignationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditConsignationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditConsignationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
