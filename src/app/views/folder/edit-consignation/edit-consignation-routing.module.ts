import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EditConsignationComponent} from './edit-consignation.component';

const routes: Routes = [
    {
        path: '',
        component: EditConsignationComponent,
        data: {
            title: 'Editer une consignation'
        }
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditConsignationRoutingModule { }
