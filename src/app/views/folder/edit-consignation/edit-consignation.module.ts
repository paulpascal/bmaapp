import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditConsignationRoutingModule } from './edit-consignation-routing.module';
import {EditConsignationComponent} from './edit-consignation.component';
import {SharedModule} from '../../../shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {LaddaModule} from 'angular2-ladda';
import {ModalsModule} from '../../modals';
import {AutoCompleteModule} from 'ng5-auto-complete';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {FileUploadModule} from 'ng2-file-upload';
import {OperationListModule} from '../../../components/operation-list/operation-list.module';
import {ConsignationEditModule} from '../../../components/consignation-edit/consignation-edit.module';

@NgModule({
    declarations: [
        EditConsignationComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        NgSelectModule,
        LaddaModule,
        EditConsignationRoutingModule,
        ModalsModule,
        AutoCompleteModule,
        BsDropdownModule,
        BsDatepickerModule.forRoot(),
        FileUploadModule,
        ConsignationEditModule,
        OperationListModule,
    ],
    providers: [
        ToasterService
    ]
})
export class EditConsignationModule { }
