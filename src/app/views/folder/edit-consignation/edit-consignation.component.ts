import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';

declare var $: any;

@Component({
    selector: 'app-edit-folder-consignation',
    templateUrl: './edit-consignation.component.html',
    styleUrls: [
        './edit-consignation.component.scss',
        '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss',
    ],
    encapsulation: ViewEncapsulation.None
})
export class EditConsignationComponent implements OnInit {

    folder: any;

    @ViewChild('shipChoiceModal') shipChoiceModal;

    constructor(private router: Router) {
    }

    ngOnInit() {
    }

    setFolder($event: any) {
        console.log('Folder got: ', $event);
        this.folder = $event;
        this.router.navigate([`/dossier/editer/${this.folder.id}`]);
    }

}
