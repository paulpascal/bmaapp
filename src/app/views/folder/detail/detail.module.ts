import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailRoutingModule } from './detail-routing.module';
import {DetailComponent} from './detail.component';
import {DeliveryOperationDetailModule} from '../../../components/delivery-operation-detail/delivery-operation-detail.module';
import {MedicalAttendanceOperationDetailModule} from '../../../components/medical-attendance-operation-detail/medical-attendance-operation-detail.module';
import {CrewChangeOperationDetailModule} from '../../../components/crew-change-operation-detail/crew-change-operation-detail.module';
import {GarbageRemovalOperationDetailComponent} from '../../../components/garbage-removal-operation-detail/garbage-removal-operation-detail.component';
import {MedicalAttendanceOperationDetailComponent} from '../../../components/medical-attendance-operation-detail/medical-attendance-operation-detail.component';
import {CashToMasterOperationDetailComponent} from '../../../components/cash-to-master-operation-detail/cash-to-master-operation-detail.component';
import {CrewChangeOperationDetailComponent} from '../../../components/crew-change-operation-detail/crew-change-operation-detail.component';
// tslint:disable-next-line:max-line-length
import {CashToMasterOperationDetailModule} from '../../../components/cash-to-master-operation-detail/cash-to-master-operation-detail.module';
import {CertificateRenewalOperationDetailComponent} from '../../../components/certificate-renewal-operation-detail/certificate-renewal-operation-detail.component';
import {GarbageRemovalOperationDetailModule} from '../../../components/garbage-removal-operation-detail/garbage-removal-operation-detail.module';
import {CertificateRenewalOperationDetailModule} from '../../../components/certificate-renewal-operation-detail/certificate-renewal-operation-detail.module';
import {DeliveryOperationDetailComponent} from '../../../components/delivery-operation-detail/delivery-operation-detail.component';

@NgModule({
  declarations: [
      DetailComponent
  ],
  imports: [
    CommonModule,
    DetailRoutingModule,
      CrewChangeOperationDetailModule,
      CashToMasterOperationDetailModule,
      GarbageRemovalOperationDetailModule,
      CertificateRenewalOperationDetailModule,
      MedicalAttendanceOperationDetailModule,
      DeliveryOperationDetailModule,
  ],
    entryComponents: [
        CrewChangeOperationDetailComponent,
        DeliveryOperationDetailComponent,
        GarbageRemovalOperationDetailComponent,
        MedicalAttendanceOperationDetailComponent,
        CertificateRenewalOperationDetailComponent,
        CashToMasterOperationDetailComponent,
    ]
})
export class DetailModule { }
