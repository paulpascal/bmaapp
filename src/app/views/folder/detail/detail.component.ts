import {Component, ComponentFactoryResolver, ComponentRef, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ApiService} from '../../../_services';
import {first} from 'rxjs/operators';
// tslint:disable-next-line:max-line-length
import {CrewChangeOperationDetailComponent} from './../../../components/crew-change-operation-detail/crew-change-operation-detail.component';
import {DeliveryOperationDetailComponent} from './../../../components/delivery-operation-detail/delivery-operation-detail.component';
import {GarbageRemovalOperationDetailComponent} from './../../../components/garbage-removal-operation-detail/garbage-removal-operation-detail.component';
import {CashToMasterOperationDetailComponent} from './../../../components/cash-to-master-operation-detail/cash-to-master-operation-detail.component';
import {MedicalAttendanceOperationDetailComponent} from './../../../components/medical-attendance-operation-detail/medical-attendance-operation-detail.component';
import {CertificateRenewalOperationDetailComponent} from './../../../components/certificate-renewal-operation-detail/certificate-renewal-operation-detail.component';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

    mission_types = [
        { 'title': CrewChangeOperationDetailComponent.title, 'component': CrewChangeOperationDetailComponent, },
        { 'title': DeliveryOperationDetailComponent.title, 'component': DeliveryOperationDetailComponent, },
        { 'title': GarbageRemovalOperationDetailComponent.title, 'component': GarbageRemovalOperationDetailComponent, },
        { 'title': CashToMasterOperationDetailComponent.title, 'component': CashToMasterOperationDetailComponent, },
        { 'title': MedicalAttendanceOperationDetailComponent.title, 'component': MedicalAttendanceOperationDetailComponent, },
        { 'title': CertificateRenewalOperationDetailComponent.title, 'component': CertificateRenewalOperationDetailComponent, },
    ];
    missions: any[];
    folder: any;

    folderId: any;

    @ViewChild('operationsZone', {read : ViewContainerRef}) target: ViewContainerRef;
    private componentRef: ComponentRef<any>;


    constructor( private apiProvider: ApiService, private resolver: ComponentFactoryResolver, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        console.log('HERRE');

        if (this.route.snapshot.params['idCrewChange']) {
            this.addCrewChange(this.route.snapshot.params['idCrewChange']);
        } else if (this.route.snapshot.params['idGarbage']) {
            this.addGarbageRemoval(this.route.snapshot.params['idGarbage']);
        } else if (this.route.snapshot.params['idCtm']) {
            this.addCashToMaster(this.route.snapshot.params['idCtm']);
        } else if (this.route.snapshot.params['idCertificate']) {
            this.addCertificateRenewal(this.route.snapshot.params['idCertificate']);
        } else if (this.route.snapshot.params['idDelivery']) {
            this.addDelivery(this.route.snapshot.params['idDelivery']);
        }
    }

    addCrewChange(opId = null) {
        if (opId) {
            const childComponent = this.resolver.resolveComponentFactory(CrewChangeOperationDetailComponent);
            this.componentRef = this.target.createComponent(childComponent);
            this.componentRef.instance.folderId = this.route.snapshot.params['id'];
            this.componentRef.instance.crewChangeId = opId;
        }
    }

    addDelivery(opId = null) {
        const childComponent = this.resolver.resolveComponentFactory(DeliveryOperationDetailComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.deliveryId = opId;
    }

    addCashToMaster(opId = null) {
        const childComponent = this.resolver.resolveComponentFactory(CashToMasterOperationDetailComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.ctmId = opId;
    }

    addGarbageRemoval(opId = null) {
        const childComponent = this.resolver.resolveComponentFactory(GarbageRemovalOperationDetailComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.garbageId = opId;
    }

    addMedicalAttendance(opId = null) {
        const childComponent = this.resolver.resolveComponentFactory(MedicalAttendanceOperationDetailComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.medicalAttendanceId = opId;
    }

    addCertificateRenewal(opId = null) {
        const childComponent = this.resolver.resolveComponentFactory(CertificateRenewalOperationDetailComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.certificateRenewalId = opId;
    }

    backToList() {
        this.router.navigate(['/dossier/liste']);
    }
}
