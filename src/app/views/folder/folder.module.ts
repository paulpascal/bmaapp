import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FolderRoutingModule } from './folder-routing.module';
import {SharedModule} from '../../shared.module';
import {DataTableModule} from 'angular2-datatable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AvatarModule} from 'ngx-avatar';
import {NgSelectModule} from '@ng-select/ng-select';
import {ToasterModule} from 'angular2-toaster';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {MomentModule} from 'ngx-moment';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        FolderRoutingModule,
        SharedModule,
        DataTableModule,
        FormsModule,
        ReactiveFormsModule,
        AvatarModule,
        NgSelectModule,
        ToasterModule,
        SweetAlert2Module,
        MomentModule,
    ]
})
export class FolderModule { }
