import {Component, OnInit, ViewChild} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {ApiService, AuthenticationService} from '../../../_services';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
import {Role} from '../../../_models';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

    error: any;
    public folders: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    isFolderDeleter = false;
    isAccounter = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService,
                private authService: AuthenticationService) {
        this.isFolderDeleter = this.authService.hasRole('FolderDeleter');
        this.isAccounter = this.authService.hasRole(Role.Accounting);
    }

    ngOnInit() {
        this.fecth();
    }

    public goToAddFolder() {
        this.router.navigate(['/dossier/creer']);
    }

    public editFolder(folder) {
        this.router.navigate([`/dossier/editer/${folder.id}`]);
    }

    public showFolder(folder) {
        this.router.navigate([`/dossier/detail/${folder.id}`]);
    }

    public confirmDeleteFolder(folder) {
        this.deleteSwal.options = {
            title: 'Supprimer cet dossier?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteFolder(folder);
            }
        };
        this.deleteSwal.show();
    }

    deleteFolder(folder) {
        this.apiService.deleteFolder(folder.id)
            .subscribe(() => {
                const index = this.folders.indexOf(folder, 0);
                if (index > -1) {
                    this.folders.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Dossier supprimé avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getFolders()
            .pipe(first())
            .subscribe(
                (folders: any[]) => {
                    // ..
                    this.folders = folders;
                    this.loading = false;
                }
            );
    }

    addPDA(folder: any) {
        this.router.navigate([`/pda/creer/${folder.id}`]);
    }
}
