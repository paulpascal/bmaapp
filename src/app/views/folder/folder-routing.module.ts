import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'liste',
        pathMatch: 'full',
    },
    {
        path: '',
        data: {
            title: 'Dossiers'
        },
        children: [
            {
                path: 'creer',
                loadChildren: './new/new.module#NewModule'
            },
            {
                path: 'editer/:id',
                loadChildren: './edit/edit.module#EditModule'
            },
            {
                path: 'editer/:id/consignation',
                loadChildren: './edit-consignation/edit-consignation.module#EditConsignationModule'
            },
            {
                path: 'detail/:id/delivery/:idDelivery',
                loadChildren: './detail/detail.module#DetailModule'
            },
            {
                path: 'detail/:id/ctm/:idCtm',
                loadChildren: './detail/detail.module#DetailModule'
            },
            {
                path: 'detail/:id/garbage/:idGarbage',
                loadChildren: './detail/detail.module#DetailModule'
            },
            {
                path: 'detail/:id/crew-change/:idCrewChange',
                loadChildren: './detail/detail.module#DetailModule'
            },
            {
                path: 'detail/:id/certificate-renewal/:idCertificate',
                loadChildren: './detail/detail.module#DetailModule'
            },
            {
                path: 'liste',
                loadChildren: './list/list.module#ListModule'
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FolderRoutingModule { }
