import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewRoutingModule } from './new-routing.module';
import {NewComponent} from './new.component';
import {SharedModule} from '../../../shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {LaddaModule} from 'angular2-ladda';
import {ModalsModule} from '../../modals';
import {AutoCompleteModule} from 'ng5-auto-complete';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
// import { AngularFileUploaderModule } from "angular-file-uploader";
import {FileUploadModule} from 'ng2-file-upload';

@NgModule({
    declarations: [
        NewComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        NgSelectModule,
        LaddaModule,
        NewRoutingModule,
        ModalsModule,
        AutoCompleteModule,
        BsDropdownModule,
        BsDatepickerModule.forRoot(),
        // AngularFileUploaderModule
        FileUploadModule
],
    providers: [
        ToasterService
    ]
})
export class NewModule { }
