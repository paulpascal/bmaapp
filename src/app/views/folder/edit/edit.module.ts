import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import {EditComponent} from './edit.component';
import {SharedModule} from '../../../shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule, ToasterService} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {LaddaModule} from 'angular2-ladda';
import {ModalsModule} from '../../modals';
import {AutoCompleteModule} from 'ng5-auto-complete';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
// import { AngularFileUploaderModule } from "angular-file-uploader";
import {FileUploadModule} from 'ng2-file-upload';
import {OperationListModule} from '../../../components/operation-list/operation-list.module';
import {ConsignationDetailModule} from '../../../components/consignation-detail/consignation-detail.module';

@NgModule({
    declarations: [
        EditComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        NgSelectModule,
        LaddaModule,
        EditRoutingModule,
        ModalsModule,
        AutoCompleteModule,
        BsDropdownModule,
        BsDatepickerModule.forRoot(),
        // AngularFileUploaderModule
        FileUploadModule,
        ConsignationDetailModule,
        OperationListModule,
    ],
    providers: [
        ToasterService
    ]
})
export class EditModule { }
