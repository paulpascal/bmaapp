import {AfterViewInit, Component, Directive, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService, AuthenticationService} from '../../../_services';
import {first} from 'rxjs/operators';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {Router} from '@angular/router';
import {FileItem, FileUploader, ParsedResponseHeaders} from 'ng2-file-upload';

import {GlobalProvider} from '../../../global';
import {DatePipe} from '@angular/common';

declare var $: any;

export class MissionItem {
    name = '';
    qty = null;
    bas = null;
    description = '';
    files = [];

    constructor(name: string = '') {
        this.name = name;
        this.files = [];
    }
}

@Component({
    selector: 'app-new-folder',
    templateUrl: './new.component.html',
    styleUrls: [
        './new.component.scss',
        '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss',
    ],
    encapsulation: ViewEncapsulation.None
})
export class NewComponent implements OnInit {

    form: FormGroup;

    folder: any;
    error = false;
    loading = false;
    submitted = false;
    ships: any[];
    ship: any;
    customer: any;
    date = new Date();

    showCreateShipModal = false;
    showSelectShipModal = false;
    showCreateCustomerModal = false;
    showSelectCustomerModal = false;

    operation_patterns: any[];
    missionsR: any[];
    results: any;
    results2: any;
    lastkeydown2 = 0;

    mission_types: any[];
    uploaders = {};

    uploadUrl;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @ViewChild('shipChoiceModal') shipChoiceModal;

    constructor(private formBuilder: FormBuilder, private apiProvider: ApiService, private authService: AuthenticationService,
                toasterService: ToasterService, private router: Router,  private global: GlobalProvider) {
        this.toasterService = toasterService;


    }

    ngOnInit() {
        this.initForm();
        this.fetchSips();
        this.fecthOperationPatterns();
        this.fetchMissionTypes();
        //
    }

    fetchSips() {
        this.apiProvider.getShips()
            .pipe(first())
            .subscribe((ships: any[]) => {
                this.ships = ships;
            });
    }
    fetchMissionTypes() {
        this.apiProvider.getMissionTypes()
            .pipe(first())
            .subscribe((data: any[]) => {
                this.mission_types = data;
                console.log(this.mission_types);
            });
    }
    fecthOperationPatterns() {
        this.apiProvider.getOperationPatternsNames()
            .pipe(first())
            .subscribe(
                (operation_patterns: any[]) => {
                    // ..
                    this.operation_patterns = operation_patterns;
                }
            );
    }

    initForm() {
        this.form = this.formBuilder.group({
            ship: [null, Validators.required],
            customer: [null, Validators.required],
            customerRef: [null, Validators.required],
            openDate: [null],
            closeDate: [null],
            shipArrivalDate: [null,  Validators.required],
            shipDepartureDate: [null],
            missions: this.formBuilder.array([])
        });
    }

    onSubmit() {
        this.submitted = true;
        this.toggleLoading();

        if (this.form.invalid) {
            // console.log(this.form);
            this.showSuccess('error', 'Erreur', 'Verifier que vous aviez renseigné tous les champs requis.');
            this.toggleLoading();
            return ;
        }

        const missions = this.missions.getRawValue();
        console.log(missions);
        const length = missions.length;

        for (let i = 0; i < length; ++i) {
            // missions[i]['name'] = document.getElementById(`dynamicMissions${i}`)['value'];
            for (let j = 0; j < missions[i]['items'].length; ++j) {
                console.log(missions[i]['items'][j]['name'], document.getElementById(`dynamicServices${i}${j}`)['value']);
                missions[i]['items'][j]['name'] = document.getElementById(`dynamicServices${i}${j}`)['value'];
            }
        }

        const pipe = new DatePipe('en-US');

        const folderData = {
            ship_id: this.f.ship.value,
            customer_id: this.f.customer.value,
            customerRef: this.f.customerRef.value,
            shipArrivalDate: pipe.transform(new Date(this.f.shipArrivalDate.value), 'yyyy-MM-dd hh:mm:ss'),
            shipDepartureDate: pipe.transform(new Date(this.f.shipDepartureDate.value), 'yyyy-MM-dd hh:mm:ss'),
            missions: missions,
            user_id: this.authService.currentUserObject['id'],
        };
        console.log(folderData);

        this.apiProvider.saveFolder(folderData)
            .pipe(first())
            .subscribe(folder => {
                console.log(folder);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('success', 'Notification', 'Dossier enregistré avec succès!');
                this.router.navigate([`/dossier/editer/${folder[0].id}`]);
            }, err => {
                let message = 'Une erreur s\'est produite, réessayez!';
                if (err.error && err.error.message) {
                    message = err.error.message;
                }
                console.log(message);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('error', 'Notification', message);
            });
    }

    get f() { return this.form.controls; }

    get missions(): FormArray {
        return this.form.get('missions') as FormArray;
    }
    getMissionItems(mission): FormArray {
        return mission.get('items') as FormArray;
    }

    // addMission() {
    //     this.missions.push(this.formBuilder.group({
    //         name: ['', Validators.required],
    //         mission_type_id: [null, Validators.required],
    //         items: this.formBuilder.array([])
    //     }));
    //     console.table(this.missions['controls']);
    //     this.listener(this.missions.length);
    // }

    removeMission(mission) {
        const i = this.missions.controls.indexOf(mission);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            this.missions.controls.splice(i, 1);
            const items = this.form.get('missions') as FormArray;
            const data = {missions: items};
            // this.updateForm(data);
        }
    }

    addMissionItem(missionId, mission) {
        // console.log(mission);
        // console.log(this.missions);
        const k = mission.controls.items.controls.length;
        this.addUploader(k, missionId);

        mission.controls.items.push(this.formBuilder.group(new MissionItem()));

        console.table(mission['controls']);
    }
    removeMissionItem(mission, item) {
        console.log(mission);
        const i = mission.controls.items.controls.indexOf(item);

        // tslint:disable-next-line:triple-equals
        if (i != -1) {
            mission.controls.items.controls.splice(i, 1);
        }
    }


    setFolder($event: any) {
        console.log('Folder got: ', $event);
        this.folder = $event;
        this.router.navigate([`/dossier/editer/${this.folder[0].id}`]);
    }
    setCustomer($event) {
        console.log('Customer got: ', $event);
        this.form.controls['customer'].setValue($event);
        this.apiProvider.getCustomer($event)
            .pipe(first())
            .subscribe(customer => {
                this.customer = customer;
            });
    }
    setShip($event) {
        console.log('Ship got: ', $event);
        this.form.controls['ship'].setValue($event);
        this.apiProvider.getShip($event)
            .pipe(first())
            .subscribe(ship => {
                this.ship = ship;
            });
    }

    toggleLoading() {
        this.loading = !this.loading;
    }
    toggleCreateShip = () => {
        if (this.showSelectShipModal) {
            this.toggleSelectShip();
        }
        this.showCreateShipModal = !this.showCreateShipModal;
    }
    toggleSelectShip = () => {
        this.showSelectShipModal = !this.showSelectShipModal;
    }

    toggleCreateCustomer = () => {
        if (this.showSelectCustomerModal) {
            this.toggleSelectCustomer();
        }
        this.showCreateCustomerModal = !this.showCreateCustomerModal;
    }
    toggleSelectCustomer = () => {
        this.showSelectCustomerModal = !this.showSelectCustomerModal;
    }

    listener(i) {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${i - 1}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 500);
    }

    getServices($event, i, j) {
        console.log(i);
        const name = (<HTMLInputElement>document.getElementById(`dynamicServices${i}${j}`)).value;
        this.results = [];

        if (name.length > 0) {
            if ($event.timeStamp - this.lastkeydown2 > 200) {
                this.results = this.searchFromArray(this.operation_patterns, name.toUpperCase());
                $(`#dynamicServices${i}${j}`).autocomplete({
                    source: this.results,
                    messages: {
                        noResults: '',
                        results: function () { }
                    }
                });
            }
        }
    }
    getMissions($event, i) {
        console.log(i);
        const name = (<HTMLInputElement>document.getElementById(`dynamicMissions${i}`)).value;
        this.results2 = [];

        if (name.length > 0) {
            if ($event.timeStamp - this.lastkeydown2 > 200) {
                this.results2 = this.searchFromArray(this.missionsR, name.toUpperCase());
                $(`#dynamicMissions${i}`).autocomplete({
                    source: this.results2,
                    messages: {
                        noResults: '',
                        results: function () { }
                    }
                });
            }
        }
    }

    searchFromArray(arr, regex) {
        const matches = [];
        let i;
        for (i = 0; i < arr.length; i++) {
            if (arr[i].match(regex)) {
                matches.push(arr[i]);
            }
        }
        return matches;
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    genTemplateFor(missionType) {
        const i = this.missions.length;

        this.missions.push(this.formBuilder.group({
            name: [missionType.name, Validators.required],
            mission_type_id: [missionType.id],
            items: this.formBuilder.array([])
        }));

        // creating ops array
        for (let k = 0; k < missionType.operation_patterns.length; ++k) {

            if (!k) {
                this.uploaders[i] = {};
            }
            // @ts-ignore
            const j = this.missions.controls[i].controls.items.length;
            // uploaders[mission_I][operation_J] = new FileUploader() --> chaque operation a son uploader
            // @ts-ignore
            this.uploaders[i][j] = new FileUploader({
                     url: this.global.uploadUrl(),
                    method: 'POST',
                    itemAlias: 'attached_files',
                    isHTML5: true,
                    authTokenHeader:  'authorization',
                    authToken: `Bearer ${this.authService.currentUserObject.token}`,
                    // tslint:disable-next-line:max-line-length
                    headers: [
                        {name: 'Accept', value: 'application/json'},
                        { name: 'Access-Control-Allow-Origin', value: 'true' },

                    ],
                    autoUpload: true,
                });

            this.uploaders[i][j].onAfterAddingFile = (item => {
                item.withCredentials = false;
            });
            // @ts-ignore
            this.uploaders[i][j].onErrorItem
                = (item, response, status, headers) => this.onErrorItem(item, response, status, headers, i, j);
            // @ts-ignore
            this.uploaders[i][j].onSuccessItem
                = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers, i, j);

            // @ts-ignore
            this.missions.controls[i].controls
                .items.push(this.formBuilder.group(new MissionItem(missionType.operation_patterns[k].name)));

            console.log(k, this.uploaders);
        }
        // console.log(this.missions);

        this.listener(this.missions.length);
    }

    deleteFile(missionIndex, operationId, item) {

        const fileId = item['uploadedId'];
        // @ts-ignore
        let files = this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.value;

        console.log(files);
        console.log(fileId);

        this.apiProvider.deleteFile(fileId)
            .pipe(first())
            .subscribe((data: any[]) => {
                console.log(data);

                files = files.filter(function( obj ) {
                    return obj.id !== fileId;
                });
                // @ts-ignore
                this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.setValue([...files]);

                // finish
                item.remove();

                // const index = files.indexOf(fileId, 0);
                // if (index > -1) {
                //     files.splice(index, 1);
                //     // @ts-ignore
                //     this.missions.controls[missionIndex].controls.items.controls[operationId].controls.files.setValue([...files]);
                // }
            });

        // @ts-ignore

    }

    handleFile(event, i , j) {
        // tslint:disable-next-line:no-unused-expression
        console.log(event);

        const reader = new FileReader();
        // tslint:disable-next-line:no-shadowed-variable
        reader.onload = (event: any) => {
            const image = event.target.result;
            if (!this.uploaders[i]) {
                this.uploaders[i] = {};
            }
            // uploaders[mission_I][operation_J] = new FileUploader() --> chaque operation a son uploader

        };
        this.uploaders[i][j] = event.target.files;
        // tslint:disable-next-line:no-shadowed-variable
        for (let i = 0; i < event.target.files; ++i) {
            reader.readAsDataURL(event.target.file[i]);
        }
        console.log(this.uploaders);
    }

    onFileSelected(i, j) {
        this.uploaders[i][j].uploadAll();
    }

    onSuccessItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders, i, j): any {
        const data = JSON.parse(response); // success server response
        console.log(data);

        // @ts-ignore
        if (this.missions.controls[i].controls.items.controls[j].controls.files.value) {
            // @ts-ignore
            // console.log('Here1', this.missions.controls[i].controls.items.controls[j].controls.files.value);
            // console.log('Here1', ...data.files);
            // @ts-ignore
            const old = this.missions.controls[i].controls.items.controls[j].controls.files.value;
            // @ts-ignore
            this.missions.controls[i].controls.items.controls[j].controls.files.setValue([...old, ...data.files]);
        } else {
            // @ts-ignore
            // console.log('Here2', ...data.files);
            // @ts-ignore
            this.missions.controls[i].controls.items.controls[j].controls.files.setValue([...data.files]);
        }
        // this.missions.controls[i].controls.items.controls[j].controls.files.value.push(...data.map(e => e.id));

        item['uploadedId'] = data.files[0].id;
        item['uploadedName'] = data.files[0].name;

        // @ts-ignore
        console.log(this.missions.controls[i].controls.items.controls[j]);
    }

    onErrorItem(item: FileItem, response: string, status: number, headers: ParsedResponseHeaders, i, j): any {
        // const error = JSON.parse(response); // error server response
        console.log(response);
    }

    genFileUrl(file) {
        return `${this.global.serverUrl}/uploads/${file.name}`;
    }
    genItemFileUrl(itemFile) {
        return `${this.global.serverUrl}/uploads/${itemFile['uploadedName']}`;
    }

    /**
     * @param k operation _index
     * @param k operation _index
     * @param i mission _index
     */
    addUploader(k, i) {
        console.log(k,  i);
        if (!k) {
            this.uploaders[i] = {};
        }
        // @ts-ignore
        const j = this.missions.controls[i].controls.items.length;
        // uploaders[mission_I][operation_J] = new FileUploader() --> chaque operation a son uploader
        // @ts-ignore
        this.uploaders[i][j] = new FileUploader({
             url: this.global.uploadUrl(),
            method: 'POST',
            itemAlias: 'attached_files',
            isHTML5: true,
            authTokenHeader:  'authorization',
            authToken: `Bearer ${this.authService.currentUserObject.token}`,
            // tslint:disable-next-line:max-line-length
            headers: [
                {name: 'Accept', value: 'application/json'},
                { name: 'Access-Control-Allow-Origin', value: 'true' },

            ],
            autoUpload: true,
        });

        this.uploaders[i][j].onAfterAddingFile = (item => {
            item.withCredentials = false;
        });
        // @ts-ignore
        this.uploaders[i][j].onErrorItem
            = (item, response, status, headers) => this.onErrorItem(item, response, status, headers, i, j);
        // @ts-ignore
        this.uploaders[i][j].onSuccessItem
            = (item, response, status, headers) => this.onSuccessItem(item, response, status, headers, i, j);
    }

}
