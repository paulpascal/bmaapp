import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit-customer',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    providers: [ToasterService]
})
export class EditCustomerComponent implements OnInit {

    customerForm: FormGroup;
    customer: any;
    error = '';
    submitted = false;
    loading = false;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private toasterService: ToasterService, private route: ActivatedRoute, private router: Router) {
    }

    get f() { return this.customerForm.controls; }

    ngOnInit() {
        this.getCustomer(this.route.snapshot.params['id']);
        this.customerForm = this.formBuilder.group({
            name: ['', Validators.required],
            tel: [''],
            dueDateDelay: ['2', Validators.required],
            address: [''],
            description: ['']
        });
    }

    getCustomer(customerId) {
        this.apiService.getCustomer(customerId)
            .pipe(first())
            .subscribe((customer: any[]) => {
                this.customer = customer;
                // Setting values
                this.customerForm.setValue({
                    name: this.customer.name,
                    tel: this.customer.tel,
                    dueDateDelay: this.customer.dueDateDelay,
                    address: this.customer.address,
                    description: this.customer.description
                });
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/customers/list']);
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.customerForm.invalid) {
            return;
        }
        this.loading = true;
        const customerData = {
            name: this.f.name.value,
            tel: this.f.tel.value,
            dueDateDelay: this.f.dueDateDelay.value,
            address: this.f.address.value,
            description: this.f.description.value,
        };
        this.apiService.editCustomer(this.customer.id, customerData)
            .pipe(first())
            .subscribe(customer => {
                    console.log(customer);
                    this.error = '';
                    this.loading = false;
                    this.submitted = false;
                    this.showSuccess();
                },
                error => {
                    console.log(error);
                    this.error = error;
                    this.loading = false;
                });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Client modifié avec succès!');
    }
}
