import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import {CustomerComponent} from './customer.component';
import {EditCustomerComponent} from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule} from 'angular2-toaster';
import {DataTableModule} from 'angular2-datatable';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {SharedModule} from '../../shared.module';
import {ModalsModule} from '../modals';

@NgModule({
    declarations: [
        CustomerComponent,
        EditCustomerComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        CustomerRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        DataTableModule,
        SweetAlert2Module,
        ModalsModule
    ]
})
export class CustomerModule { }
