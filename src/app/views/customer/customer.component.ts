import {Component, OnInit, ViewChild} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {first} from 'rxjs/operators';
import {ApiService} from '../../_services';
import {Router} from '@angular/router';

@Component({
    selector: 'app-customer',
    templateUrl: './customer.component.html',
    styleUrls: ['./customer.component.scss'],
    providers: [ToasterService]
})
export class CustomerComponent implements OnInit {

    error: any;
    public customers: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    showCreateCustomerModal = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fecth();
    }

    public editCustomer(customer) {
        this.router.navigate([`/customers/edit/${customer.id}`]);
    }

    public confirmDeleteCustomer(customer) {
        this.deleteSwal.options = {
            title: 'Supprimer cet client?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteCustomer(customer);
            }
        };
        this.deleteSwal.show();
    }

    deleteCustomer(customer) {
        this.apiService.deleteCustomer(customer.id)
            .subscribe(() => {
                const index = this.customers.indexOf(customer, 0);
                if (index > -1) {
                    this.customers.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Client supprimé avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    toggleCreateCustomer = () => {
        this.showCreateCustomerModal = !this.showCreateCustomerModal;
    }
    setCustomer($event) {
        console.log('Customer got: ', $event);
        this.fecth();
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getCustomers()
            .pipe(first())
            .subscribe(
                (customers: any[]) => {
                    // ..
                    this.customers = customers;
                    this.loading = false;
                }
            );
    }
}
