import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HotelRoutingModule } from './hotel-routing.module';
import {HotelComponent} from './hotel.component';
import {EditHotelComponent} from './edit/edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule} from 'angular2-toaster';
import {DataTableModule} from 'angular2-datatable';
import {SweetAlert2Module} from '@toverux/ngx-sweetalert2';
import {SharedModule} from '../../shared.module';
import {ModalsModule} from '../modals';
import {AddHotelModule} from '../modals/add-hotel/add-hotel.module';

@NgModule({
    declarations: [
        HotelComponent,
        EditHotelComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        HotelRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        ToasterModule,
        DataTableModule,
        SweetAlert2Module,
        ModalsModule,
        AddHotelModule,
    ]
})
export class HotelModule { }
