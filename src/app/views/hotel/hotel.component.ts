import {Component, OnInit, ViewChild} from '@angular/core';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {SwalComponent} from '@toverux/ngx-sweetalert2';
import {first} from 'rxjs/operators';
import {ApiService} from '../../_services';
import {Router} from '@angular/router';

@Component({
    selector: 'app-hotel',
    templateUrl: './hotel.component.html',
    styleUrls: ['./hotel.component.scss'],
    providers: [ToasterService]
})
export class HotelComponent implements OnInit {

    error: any;
    public hotels: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    showCreateHotelModal = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fecth();
    }

    public editHotel(hotel) {
        this.router.navigate([`/hotels/edit/${hotel.id}`]);
    }

    public confirmDeleteHotel(hotel) {
        this.deleteSwal.options = {
            title: 'Supprimer cet hotel?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteHotel(hotel);
            }
        };
        this.deleteSwal.show();
    }

    deleteHotel(hotel) {
        this.apiService.deleteHotel(hotel.id)
            .subscribe(() => {
                const index = this.hotels.indexOf(hotel, 0);
                if (index > -1) {
                    this.hotels.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Hotel supprimé avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    toggleCreateHotel = () => {
        this.showCreateHotelModal = !this.showCreateHotelModal;
    }
    setHotel($event) {
        console.log('Hotel got: ', $event);
        this.fecth();
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getHotels()
            .pipe(first())
            .subscribe(
                (hotels: any[]) => {
                    // ..
                    this.hotels = hotels;
                    this.loading = false;
                }
            );
    }
}
