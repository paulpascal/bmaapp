import { Component, OnInit } from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-edit-hotel',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss'],
    providers: [ToasterService]
})
export class EditHotelComponent implements OnInit {

    hotelForm: FormGroup;
    hotel: any;
    error = '';
    submitted = false;
    loading = false;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private formBuilder: FormBuilder, private apiService: ApiService,
                private toasterService: ToasterService, private route: ActivatedRoute, private router: Router) {
    }

    get f() { return this.hotelForm.controls; }

    ngOnInit() {
        this.getHotel(this.route.snapshot.params['id']);
        this.hotelForm = this.formBuilder.group({
            name: ['', Validators.required],
            tel: [''],
            dueDateDelay: ['2', Validators.required],
            address: [''],
            description: ['']
        });
    }

    getHotel(hotelId) {
        this.apiService.getHotel(hotelId)
            .pipe(first())
            .subscribe((hotel: any[]) => {
                this.hotel = hotel;
                // Setting values
                this.hotelForm.setValue({
                    name: this.hotel.name,
                    tel: this.hotel.tel,
                    dueDateDelay: this.hotel.dueDateDelay,
                    address: this.hotel.address,
                    description: this.hotel.description
                });
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/hotels/list']);
                }
            });
    }

    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.hotelForm.invalid) {
            return;
        }
        this.loading = true;
        const hotelData = {
            name: this.f.name.value,
            tel: this.f.tel.value,
            dueDateDelay: this.f.dueDateDelay.value,
            address: this.f.address.value,
            description: this.f.description.value,
        };
        this.apiService.editHotel(this.hotel.id, hotelData)
            .pipe(first())
            .subscribe(hotel => {
                    console.log(hotel);
                    this.error = '';
                    this.loading = false;
                    this.submitted = false;
                    this.showSuccess();
                },
                error => {
                    console.log(error);
                    this.error = error;
                    this.loading = false;
                });
    }

    showSuccess() {
        this.toasterService.pop('success', 'Opération réussie!', 'Client modifié avec succès!');
    }
}
