import { Component, OnInit } from '@angular/core';
import * as JSPDF from 'jspdf';
import * as writtenForm from 'written-number';
import html2canvas from 'html2canvas';
import {first} from 'rxjs/operators';
import {ApiService} from '../../../_services';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
    selector: 'app-show',
    templateUrl: './show.component.html',
    styleUrls: ['./show.component.scss']
})
export class ShowComponent implements OnInit {

    sheet: any;

    constructor(private apiService: ApiService, private route: ActivatedRoute, private router: Router) { }

    ngOnInit() {
        this.getSheet(this.route.snapshot.params['id']);
    }

    public print() {
        const data = document.getElementById('printable');
        html2canvas(data).then(canvas => {
            // Few necessary setting options
            const imgWidth = 210;
            const pageHeight = 295;
            const imgHeight = canvas.height * imgWidth / canvas.width;
            const heightLeft = imgHeight;

            const contentDataURL = canvas.toDataURL('image/png');
            const pdf = new JSPDF('p', 'mm', 'a4'); // A4 size page of PDF
            const position = 5;
            pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
            pdf.save('fiche_caisse' + this.getMoment() + '.pdf'); // Generated PDF
        });
    }

    getSheet(sheetId) {
        this.apiService.getCheckoutSheet(sheetId)
            .pipe(first())
            .subscribe((sheet: any) => {
                this.sheet = sheet;
            }, error => {
                if (error.status === 404) {
                    this.router.navigate(['/fiche-caisse/liste']);
                }
            });
    }

    getMoment() {
        return Date.now();
    }

    toLetter(digit) {
        return writtenForm(digit, { lang: 'fr', noAnd: true });
    }
}
