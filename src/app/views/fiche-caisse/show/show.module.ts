import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShowRoutingModule } from './show-routing.module';
import {ShowComponent} from './show.component';
import {PDFExportModule} from '@progress/kendo-angular-pdf-export';
import {NgxPrintModule} from 'ngx-print';

@NgModule({
    declarations: [ShowComponent],
    imports: [
        CommonModule,
        ShowRoutingModule,
        PDFExportModule,
        NgxPrintModule
    ]
})
export class ShowModule { }
