import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'liste',
        pathMatch: 'full',
    },
    {
        path: '',
        data: {
            title: 'Fiches de caisse'
        },
        children: [
            {
                path: 'ajouter',
                loadChildren: './new/fiche-caisse-new.module#FicheCaisseNewModule'
            },
            {
                path: 'afficher/:id',
                loadChildren: './show/show.module#ShowModule'
            },
            {
                path: 'liste',
                loadChildren: './list/fiche-caisse-list.module#FicheCaisseListModule'
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FicheCaisseRoutingModule { }
