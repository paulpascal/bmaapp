import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FicheCaisseListComponent } from './fiche-caisse-list.component';

const routes: Routes = [
  {
    path: '',
    component: FicheCaisseListComponent,
    data: {
      title: 'Liste des fiches de caisses'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FicheCaisseListRoutingModule {}
