import {Component, OnInit, ViewChild} from '@angular/core';

import { SwalComponent } from '@toverux/ngx-sweetalert2';
import {ToasterConfig, ToasterService} from 'angular2-toaster';

import * as moment from 'moment';
import {ApiService} from '../../../_services';
import {Router} from '@angular/router';
import {first} from 'rxjs/operators';
moment.locale('fr');

@Component({
    templateUrl: 'fiche-caisse-list.component.html'
})
export class FicheCaisseListComponent implements OnInit {

    error: any;
    public sheets: any[];
    public filterQuery = '';
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 3000
    });
    loading = false;

    @ViewChild('deleteSwal') private deleteSwal: SwalComponent;

    constructor(private apiService: ApiService, private router: Router, private toasterService: ToasterService) {}

    ngOnInit() {
        this.fecth();
    }

    public goToAddSheet() {
        this.router.navigate(['/fiche-caisse/ajouter']);
    }

    showSheet(sheet: any) {
        this.router.navigate([`/fiche-caisse/afficher/${sheet.id}`]);
    }

    public editSheets(sheet) {
        // this.router.navigate([`/admin/sheets/edit/${sheet.id}`]);
    }

    public confirmDeleteSheet(sheet) {
        this.deleteSwal.options = {
            title: 'Supprimer cette fiche?',
            text: 'Cette action ne peux pas être annulé!',
            type: 'question',
            showCancelButton: true,
            focusCancel: true,
            preConfirm: () => {
                this.deleteSheets(sheet);
            }
        };
        this.deleteSwal.show();
    }

    deleteSheets(sheet) {
        this.apiService.deleteCheckoutSheet(sheet.id)
            .subscribe(() => {
                const index = this.sheets.indexOf(sheet, 0);
                if (index > -1) {
                    this.sheets.splice(index, 1);
                    this.fecth();
                }
                this.showAlert('success', 'Opération réussie!', 'Fiche supprimée avec succès!');
            }, err => {
                this.showAlert('error', 'Opération échouée!', err.error.message);
            });
    }

    showAlert(type, title, message) {
        this.toasterService.pop(type, title, message);
    }

    fecth() {
        this.loading = true;
        this.apiService.getCheckoutSheets()
            .pipe(first())
            .subscribe(
                (sheets: any[]) => {
                    // ..
                    this.sheets = sheets;
                    this.loading = false;
                }
            );
    }
}
