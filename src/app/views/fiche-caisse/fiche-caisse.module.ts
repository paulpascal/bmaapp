import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FicheCaisseRoutingModule } from './fiche-caisse-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    FicheCaisseRoutingModule
  ]
})
export class FicheCaisseModule { }
