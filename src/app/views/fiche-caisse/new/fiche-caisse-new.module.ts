import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

// Angular 2 Input Mask
import { TextMaskModule } from 'angular2-text-mask';

// Timepicker
import {BsDropdownModule, CollapseModule, ModalModule, TimepickerModule} from 'ngx-bootstrap';

// Datepicker
import { BsDatepickerModule } from 'ngx-bootstrap';

// Routing
import { FicheCaisseNewRoutingModule } from './fiche-caisse-new-routing.module';
import { FicheCaisseNewComponent } from './fiche-caisse-new.component';
import {LaddaModule} from 'angular2-ladda';
import {ToasterModule} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {SharedModule} from '../../../shared.module';
import {ModalsModule} from '../../modals';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        LaddaModule,
        ToasterModule,
        FicheCaisseNewRoutingModule,
        TextMaskModule,
        TimepickerModule.forRoot(),
        BsDatepickerModule.forRoot(),
        NgSelectModule,
        BsDropdownModule.forRoot(),
        CollapseModule.forRoot(),
        ModalsModule
    ],
    declarations: [
        FicheCaisseNewComponent,
    ]
})
export class FicheCaisseNewModule { }
