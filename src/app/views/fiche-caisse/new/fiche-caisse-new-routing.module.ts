import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FicheCaisseNewComponent } from './fiche-caisse-new.component';

const routes: Routes = [
  {
    path: '',
    component: FicheCaisseNewComponent,
    data: {
      title: 'Ajouter une nouvelle fiche de caisse'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FicheCaisseNewRoutingModule {}
