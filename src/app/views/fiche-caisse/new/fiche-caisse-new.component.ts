import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../../_services';
import {DatePipe} from '@angular/common';

@Component({
    templateUrl: 'fiche-caisse-new.component.html',
    styleUrls: [
        '../../../../scss/vendors/bs-datepicker/bs-datepicker.scss',
        '../../../../scss/vendors/ng-select/ng-select.scss',
        '../../../../../node_modules/ladda/dist/ladda-themeless.min.css',
        '../../../../scss/vendors/toastr/toastr.scss'
    ],
    providers: [ToasterService],
    encapsulation: ViewEncapsulation.None
})
export class FicheCaisseNewComponent implements OnInit {
    // ..
    ships: any[];
    folders: any[];
    investmentTypes: any[];

    // Forms
    sheetForm: FormGroup;
    bsValue: Date = new Date();

    // ..
    loading = false;
    submitted = false;
    showCreateShipModal = false;
    error = '';

    private toasterService: ToasterService;

    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(
        private formBuilder: FormBuilder, toasterService: ToasterService,
        private apiService: ApiService, private authService: AuthenticationService
    ) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.fecthShip();
        this.fetchInvestmentType();
        // ..
        this.sheetForm = this.formBuilder.group({
            date: [this.bsValue, Validators.required],
            amount: [null, Validators.required],
            purpose: [null, Validators.required],
            type: [null, Validators.required],
            ship: [null],
            folderId: [null],
            pieceNumber: [null],
        });
    }

    toggleLoading() {
        this.loading = !this.loading;
    }
    toggleCreateShip = () => {
        this.showCreateShipModal = !this.showCreateShipModal;
    }

    get g() { return this.sheetForm.controls; }

    setShip($event) {
        console.log('Ship got: ', $event);
        this.fecthShip();
        this.sheetForm.controls['ship'].setValue($event);
    }

    onFormSubmit() {
        this.submitted = true;
        if (this.sheetForm.invalid) {
            return ;
        }
        this.toggleLoading();

        const pipe = new DatePipe('en-US');
        const formattedDate = pipe.transform(new Date(this.g.date.value), 'yyyy-MM-dd hh:mm:ss');

        // ..
        const sheetData = {
            date: formattedDate,
            amount: this.g.amount.value,
            purpose: this.g.purpose.value,
            folder_id: this.g.folderId.value,
            pieceNumber: this.g.pieceNumber.value,
            ship_id: this.g.ship.value,
            investment_type_id: this.g.type.value,
            user_id: this.authService.currentUserObject.id,
        };

        this.apiService.saveCheckoutSheet(sheetData)
            .pipe(first())
            .subscribe(sheet => {
                console.log(sheet);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('success', 'Notification', 'Fiche de caisse enregistrée avec succès!');
                this.ngOnInit();
            }, err => {
                console.log(err.error.message);
                this.toggleLoading();
                this.submitted = false;
                this.showSuccess('error', 'Notification', err.error.message);
            });
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    fecthShip() {
        this.apiService.getShips()
            .pipe(first())
            .subscribe(
                (ships: any[]) => {
                    // ..
                    this.ships = ships;
                }
            );
    }

    fecthFoldersForShip(shipId) {
        this.apiService.getFoldersForShip(shipId)
            .pipe(first())
            .subscribe(
                (folders: any[]) => {
                    // ..
                    this.folders = folders;
                }
            );
    }

    fetchInvestmentType() {
        this.apiService.getInvestementTypes()
            .pipe(first())
            .subscribe(
                (types: any[]) => {
                    // ..
                    this.investmentTypes = types;
                }
            );
    }

    shipSelected(ev) {
        console.log(ev);
        this.fecthFoldersForShip(ev.id);
    }
}
