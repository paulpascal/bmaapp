import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import {AuthGuard} from './_guards';
import {Role} from './_models';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'login/:not',
        component: LoginComponent
    },
    {
        path: 'register',
        component: RegisterComponent,
        data: { title: 'Register Page' }
    },
    {
        path: '',
        component: DefaultLayoutComponent,
        canActivate: [AuthGuard],
        data: {
            title: 'Accueil'
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: './views/dashboard/dashboard.module#DashboardModule',
                canActivate: [AuthGuard],
                data: { title: 'Dashboard' }
            },
            {
                path: 'fiche-bons',
                loadChildren: './views/fiche-bons/fiche-bons.module#FicheBonsModule',
                canActivate: [AuthGuard],
                data: { roles: [Role.Admin, Role.Accounting], title: 'Fiches de bons' }
            },
            {
                path: 'fiche-caisse',
                loadChildren: './views/fiche-caisse/fiche-caisse.module#FicheCaisseModule',
                canActivate: [AuthGuard],
                data: { roles: [Role.Admin, Role.Accounting], title: 'Fiches de caisse' }
            },
            {
                path: 'dossier',
                loadChildren: './views/folder/folder.module#FolderModule',
                canActivate: [AuthGuard],
                data: { title: 'Dossiers' }
            },
            {
                path: 'pda',
                loadChildren: './views/pda/pda.module#PdaModule',
                canActivate: [AuthGuard],
                data: { title: 'Pdas' }
            },
            {
                path: 'facture',
                loadChildren: './views/facture/facture.module#FactureModule',
                canActivate: [AuthGuard],
                data: { title: 'Facture' }
            },
            {
                path: 'parametres',
                loadChildren: './views/setting/setting.module#SettingModule',
                canActivate: [AuthGuard],
                data: { title: 'Paramètres' }
            },
            {
                path: 'admin',
                loadChildren: './views/admin/admin.module#AdminModule',
                canActivate: [AuthGuard],
                data: { roles: [Role.Admin], title: 'Administration' }
            },
            {
                path: 'ships',
                loadChildren: './views/navire/navire.module#NavireModule',
                canActivate: [AuthGuard],
                data: { roles: [Role.Admin, Role.Operation], title: 'Navire' }
            },
            {
                path: 'customers',
                loadChildren: './views/customer/customer.module#CustomerModule',
                canActivate: [AuthGuard],
                data: { roles: [Role.Admin, Role.Operation], title: 'Clients' }
            },
            {
                path: 'hotels',
                loadChildren: './views/hotel/hotel.module#HotelModule',
                canActivate: [AuthGuard],
                data: { roles: [Role.Admin, Role.Operation], title: 'Hotels' }
            },
            {
                path: 'clinics',
                loadChildren: './views/clinic/clinic.module#ClinicModule',
                canActivate: [AuthGuard],
                data: { roles: [Role.Admin, Role.Operation], title: 'Cliniques' }
            },
            {
                path: 'calendrier',
                loadChildren: './views/calendar/calendar.module#CalendarTaskModule',
                canActivate: [AuthGuard],
                data: { title: 'Calendrier' }
            },
            {
                path: 'profil',
                loadChildren: './views/profile/profile.module#ProfileModule',
                canActivate: [AuthGuard],
                data: { title: 'Profil' }
            },
            {
                path: 'profil/utilisateur/:id',
                loadChildren: './views/profile/profile.module#ProfileModule',
                canActivate: [AuthGuard],
                data: { title: 'Profil' }
            },
        ]
    },
    // otherwise redirect to home
    // { path: '**', redirectTo: '' }
];

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule.forRoot(routes)
    ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {}
