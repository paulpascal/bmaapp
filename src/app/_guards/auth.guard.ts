import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import {AuthenticationService, NgTUtilService} from '../_services';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authService: AuthenticationService,
        private ngUtils: NgTUtilService
    ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        const currentUser = this.authService.currentUserObject;

        if (! this.ngUtils.getState()) {
            this.ngUtils.doTheJob();
            return false;
        }

        if (currentUser) {
            // check if the user has appropriate to parse this route
            // it means if the user has one the roles required in his roles package
            if (next.data.roles && !currentUser.roles.some((ur) => next.data.roles.indexOf(ur.label) !== -1)) {
                // role not authorize => to home page
                this.router.navigate(['/']);
                return false;
            }

            // authorized
            return true;
        }

        // not logged so redirect to login page
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }, skipLocationChange: true });
        return false;
    }
}
