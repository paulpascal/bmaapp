export enum Role {
    Accounting = 'Accounting',
    Admin = 'Admin',
    Direction = 'Direction',
    It = 'It',
    Operation = 'Operation',
    Secretariat = 'Secretariat',
    FolderCloser = 'FolderCloser',
    FolderValidator = 'FolderValidator',
    InvoiceValidator = 'InvoiceValidator',
    ConsignationUpdate = 'ConsignationUpdate',
}
