export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    roles: [{
        id: number;
        label: string;
        description: string;
    }];
    token?: string;
}
