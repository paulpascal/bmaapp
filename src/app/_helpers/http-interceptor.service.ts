import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {AuthenticationService} from '../_services';
import {ErrorDialogService} from './error-dialog.service';

@Injectable()
export class HttpInterceptorService {

    constructor(private authService: AuthenticationService, private errorDialogService: ErrorDialogService) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUser = this.authService.currentUserObject;
        if (currentUser && currentUser.token) {
            request = request.clone({ setHeaders: { Authorization: `Bearer ${currentUser.token}` } });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        }

        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    console.log('event--->>>', event);
                }
                return event;
            }),
            catchError((error: HttpErrorResponse) => {
                // const data = {};
                // console.log(error);
                // if (error['validation']) {
                //     const validations = data['validation'];
                //     if (Array.isArray(validations)) {
                //         validations.forEach(err => {
                //             data['message'] = err.message;
                //         });
                //     }
                // }
                // // data['message'] = error.message;
                if (error.status === 401) { this.errorDialogService.openDialog(error.error.message); }
                return throwError(error);
            })
        );
    }
}
