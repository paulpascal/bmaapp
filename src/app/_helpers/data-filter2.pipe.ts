import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'dataFilter2'
})
export class DataFilter2Pipe implements PipeTransform {

    transform(array: any[], query: string): any {
        if (query) {
            return _.filter(array, row => {
                console.log(row);
                return row.customer && (
                    row.customer.name.toUpperCase().indexOf(query.toUpperCase()) > -1 ||
                    row.number.toUpperCase().indexOf(query.toUpperCase()) > -1 ||
                    row.ship.name.toUpperCase().indexOf(query.toUpperCase()) > -1 ||
                    row.date && row.date.toUpperCase().indexOf(query.toUpperCase()) > -1
                );
            });
        }
        return array;
    }

}
