const baseItems = [
    {
        name: 'Dashboard',
        url: '/dashboard',
        icon: 'icon-speedometer'
    }
];

const accountingNav = [
    ...baseItems,
    {
        title: true,
        name: 'Comptabilité'
    },
    {
        name: 'Fiches de bons',
        url: '/fiche-bons',
        icon: 'fa fa-clipboard',
        children: [
            {
                name: 'Nouvelle fiche',
                url: '/fiche-bons/ajouter',
                icon: 'fa fa-plus'
            },
            {
                name: 'Mes fiches de bons',
                url: '/fiche-bons/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Fiches de caisse',
        url: '/forms',
        icon: 'fa fa-briefcase',
        children: [
            {
                name: 'Nouvelle fiche',
                url: '/fiche-caisse/ajouter',
                icon: 'fa fa-plus'
            },
            {
                name: 'Mes fiches de caisse',
                url: '/fiche-caisse/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Pdas',
        url: '/pda/liste',
        icon: 'fa fa-file-powerpoint-o',
        children: [
            // {
            //     name: 'Ouvrir un nouveau',
            //     url: '/pda/creer',
            //     icon: 'fa fa-plus'
            // },
            {
                name: 'Consulter les PDAs',
                url: '/pda/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Dossiers',
        url: '/dossier/liste',
        icon: 'fa fa-folder',
        children: [
            {
                name: 'Consulter les dossiers',
                url: '/dossier/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        title: true,
        name: 'Paramètres',
    },
    {
        name: 'Paramétrage',
        url: '/parametres',
        icon: 'fa fa-cogs',
    },
];

const operationNav = [
    ...baseItems,
    {
        title: true,
        name: 'Opérations'
    },
    {
        name: 'Clients',
        url: '/customers',
        icon: 'fa fa-user-alt',
        children: [
            {
                name: 'Liste des clients',
                url: '/customers/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Navires',
        url: '/ships',
        icon: 'fa fa-ship',
        children: [
            {
                name: 'Liste des navires',
                url: '/ships/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Dossiers',
        url: '/dossier/liste',
        icon: 'fa fa-folder',
        children: [
            {
                name: 'Ouvrir un nouveau',
                url: '/dossier/creer',
                icon: 'fa fa-plus'
            },
            {
                name: 'Consulter les dossier',
                url: '/dossier/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Hotels',
        url: '/hotels',
        icon: 'fa fa-hotel',
        children: [
            {
                name: 'Liste des hotels',
                url: '/hotels/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Cliniques',
        url: '/clinics',
        icon: 'fa fa-hospital',
        children: [
            {
                name: 'Liste des clinique',
                url: '/clinics/list',
                icon: 'fa fa-list'
            }
        ]
    },
];

const ops_accNav = [
    ...baseItems,
    {
        title: true,
        name: 'Opérations'
    },
    {
        name: 'Navires',
        url: '/ships',
        icon: 'fa fa-ship',
        children: [
            {
                name: 'Liste des navires',
                url: '/ships/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Clients',
        url: '/customers',
        icon: 'fa fa-user-alt',
        children: [
            {
                name: 'Liste des clients',
                url: '/customers/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Pdas',
        url: '/pda/liste',
        icon: 'fa fa-file-powerpoint-o',
        children: [
            // {
            //     name: 'Ouvrir un nouveau',
            //     url: '/pda/creer',
            //     icon: 'fa fa-plus'
            // },
            {
                name: 'Consulter les PDAs',
                url: '/pda/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Dossiers',
        url: '/dossier/liste',
        icon: 'fa fa-folder',
        children: [
            {
                name: 'Ouvrir un nouveau',
                url: '/dossier/creer',
                icon: 'fa fa-plus'
            },
            {
                name: 'Consulter les dossier',
                url: '/dossier/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Fiches de bons',
        url: '/fiche-bons',
        icon: 'fa fa-clipboard',
        children: [
            {
                name: 'Nouvelle fiche',
                url: '/fiche-bons/ajouter',
                icon: 'fa fa-plus'
            },
            {
                name: 'Mes fiches de bons',
                url: '/fiche-bons/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Fiches de caisse',
        url: '/forms',
        icon: 'fa fa-briefcase',
        children: [
            {
                name: 'Nouvelle fiche',
                url: '/fiche-caisse/ajouter',
                icon: 'fa fa-plus'
            },
            {
                name: 'Mes fiches de caisse',
                url: '/fiche-caisse/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Hotels',
        url: '/hotels',
        icon: 'fa fa-hotel',
        children: [
            {
                name: 'Liste des hotels',
                url: '/hotels/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Cliniques',
        url: '/clinics',
        icon: 'fa fa-hospital',
        children: [
            {
                name: 'Liste des cliniques',
                url: '/clinics/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Calendrier',
        url: '/calendrier',
        icon: 'fa fa-calendar',
    },
    {
        title: true,
        name: 'Paramètres',
    },
    {
        name: 'Paramétrage',
        url: '/parametres',
        icon: 'fa fa-cogs',
    },
];

const adminNav = [
    ...baseItems,
    {
        title: true,
        name: 'Administrateurs'
    },
    {
        name: 'Utilisateurs',
        url: '/admin/users',
        icon: 'fa fa-group',
        children: [
            {
                name: 'Nouveau utilisateur',
                url: '/admin/users/new',
                icon: 'fa fa-plus'
            },
            {
                name: 'Liste des utilisateurs',
                url: '/admin/users/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Rôles',
        url: '/admin/roles',
        icon: 'icon-note',
        children: [
            {
                name: 'Nouveau rôle',
                url: '/admin/roles/new',
                icon: 'fa fa-plus'
            },
            {
                name: 'Liste des rôles',
                url: '/admin/roles/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        title: true,
        name: 'Comptabilité'
    },
    {
        name: 'Fiches de bons',
        url: '/fiche-bons',
        icon: 'fa fa-clipboard',
        children: [
            {
                name: 'Nouvelle fiche',
                url: '/fiche-bons/ajouter',
                icon: 'fa fa-plus'
            },
            {
                name: 'Mes fiches de bons',
                url: '/fiche-bons/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Fiches de caisse',
        url: '/forms',
        icon: 'fa fa-briefcase',
        children: [
            {
                name: 'Nouvelle fiche',
                url: '/fiche-caisse/ajouter',
                icon: 'fa fa-plus'
            },
            {
                name: 'Mes fiches de caisse',
                url: '/fiche-caisse/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Pdas',
        url: '/pda/liste',
        icon: 'fa fa-file-powerpoint-o',
        children: [
            // {
            //     name: 'Ouvrir un nouveau',
            //     url: '/pda/creer',
            //     icon: 'fa fa-plus'
            // },
            {
                name: 'Consulter les PDAs',
                url: '/pda/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Dossiers',
        url: '/dossier/liste',
        icon: 'fa fa-folder',
        children: [
            {
                name: 'Ouvrir un nouveau',
                url: '/dossier/creer',
                icon: 'fa fa-plus'
            },
            {
                name: 'Consulter les dossier',
                url: '/dossier/liste',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        title: true,
        name: 'Opérations'
    },
    {
        name: 'Navires',
        url: '/ships',
        icon: 'fa fa-ship',
        children: [
            {
                name: 'Liste des navires',
                url: '/ships/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Clients',
        url: '/customers',
        icon: 'fa fa-user-alt',
        children: [
            {
                name: 'Liste des clients',
                url: '/customers/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Hotels',
        url: '/hotels',
        icon: 'fa fa-hotel',
        children: [
            {
                name: 'Liste des hotels',
                url: '/hotels/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Cliniques',
        url: '/clinics',
        icon: 'fa fa-hospital',
        children: [
            {
                name: 'Liste des cliniques',
                url: '/clinics/list',
                icon: 'fa fa-list'
            }
        ]
    },
    {
        name: 'Calendrier',
        url: '/calendrier',
        icon: 'fa fa-calendar',
    },
    {
        title: true,
        name: 'Paramètres',
    },
    {
        name: 'Paramétrage',
        url: '/parametres',
        icon: 'fa fa-cogs',
    },
];

const directionNav = [
    ...baseItems,
    {
        title: true,
        name: 'Direction'
    },
];

const itNav = [
    ...baseItems,
    {
        title: true,
        name: 'IT'
    },
    {
        name: 'Fiches de bons',
        url: '/editors',
        icon: 'fa fa-code',
        children: [
            {
                name: 'Ajouter une fiche de bons',
                url: '/editors/text-editors',
                icon: 'icon-note',
                badge: {
                    variant: 'danger',
                    text: 'PRO'
                }
            },
            {
                name: 'Banques de fiches',
                url: '/editors/code-editors',
                icon: 'fa fa-code',
                badge: {
                    variant: 'danger',
                    text: 'PRO'
                }
            }
        ]
    },
];

const secretariatNav = [
    ...baseItems
];

export const NAV = {
    accounting: accountingNav,
    admin: adminNav,
    direction: directionNav,
    it: itNav,
    operation: operationNav,
    secretariat: secretariatNav,
    ops_acc: ops_accNav
};
