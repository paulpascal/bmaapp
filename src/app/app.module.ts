import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';
import { NotifierModule } from 'angular-notifier';

import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    suppressScrollX: true
};

import {AppComponent} from './app.component';

// Import containers
import {DefaultLayoutComponent} from './containers';

import {P404Component} from './views/error/404.component';
import {P500Component} from './views/error/500.component';
import {LoginComponent} from './views/login/login.component';
import {RegisterComponent} from './views/register/register.component';

const APP_CONTAINERS = [
    DefaultLayoutComponent
];

import {
    AppAsideModule,
    AppBreadcrumbModule,
    AppHeaderModule,
    AppFooterModule,
    AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import {AppRoutingModule} from './app.routing';

// Import 3rd party components
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {ChartsModule} from 'ng2-charts/ng2-charts';
import { AvatarModule } from 'ngx-avatar';

import {HttpInterceptorService, ErrorDialogService} from './_helpers';
import { GlobalProvider } from './global';
import {ModalModule} from 'ngx-bootstrap';
import {ErrorDialogComponent} from './views/error-dialog/error-dialog.component';
import {MatDialogModule} from '@angular/material';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import {MomentModule} from 'ngx-moment';
import {SharedModule} from './shared.module';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {CountryPickerModule} from 'ngx-country-picker';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        AppAsideModule,
        AppBreadcrumbModule.forRoot(),
        AppFooterModule,
        AppHeaderModule,
        AppSidebarModule,
        PerfectScrollbarModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        ModalModule.forRoot(),
        ChartsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        AvatarModule.forRoot(),
        BrowserAnimationsModule,
        MatDialogModule,
        SweetAlert2Module.forRoot(),
        MomentModule.forRoot(),
        SharedModule,
        NotifierModule.withConfig({
            position: {
                horizontal: { position: 'right' },
                vertical: { position: 'top', distance: 50 }
            },
            behaviour: {
                autoHide: false,
            }
        }),
        MDBBootstrapModule.forRoot(),

    ],
    exports: [
        SharedModule
    ],
    declarations: [
        AppComponent,
        ...APP_CONTAINERS,
        P404Component,
        P500Component,
        LoginComponent,
        RegisterComponent,
        ErrorDialogComponent,
    ],
    entryComponents: [
      ErrorDialogComponent,
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        // ..
        { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
        // provider used to create fake backend => only for test purpose
        //
        GlobalProvider,
        ErrorDialogService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
