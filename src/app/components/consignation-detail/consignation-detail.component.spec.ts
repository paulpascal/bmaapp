import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignationDetailComponent } from './consignation-detail.component';

describe('ConsignationDetailComponent', () => {
  let component: ConsignationDetailComponent;
  let fixture: ComponentFixture<ConsignationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

