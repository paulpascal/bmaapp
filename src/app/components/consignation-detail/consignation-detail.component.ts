import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Role} from '../../_models';
import {AuthenticationService} from '../../_services';

@Component({
  selector: 'app-consignation-detail',
  templateUrl: './consignation-detail.component.html',
  styleUrls: ['./consignation-detail.component.scss']
})
export class ConsignationDetailComponent implements OnInit {

    accounting: false;
    isFolderCloser: false;
    isFolderValidator: false;
    canUpdateConsignation: false;

    @Input() folder = null;

    constructor(private router: Router, private authService: AuthenticationService) {

        this.accounting = this.authService.hasRole(Role.Accounting);
        this.isFolderCloser = this.authService.hasRole(Role.FolderCloser);
        this.isFolderValidator = this.authService.hasRole(Role.FolderValidator);
        this.canUpdateConsignation = this.authService.hasRole(Role.ConsignationUpdate);
    }

  ngOnInit() {
  }

    public editFolder() {
        this.router.navigate([`/dossier/editer/${this.folder.id}/consignation`]);
    }

}
