import {Component, ComponentFactoryResolver, ComponentRef, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ApiService} from '../../_services';
import {first} from 'rxjs/operators';
import {CrewChangeOperationComponent} from '../crew-change-operation/crew-change-operation.component';
import {DeliveryOperationComponent} from '../delivery-operation/delivery-operation.component';
import {LandingOperationComponent} from '../landing-operation/landing-operation.component';
import {GarbageRemovalOperationComponent} from '../garbage-removal-operation/garbage-removal-operation.component';
import {CashToMasterOperationComponent} from '../cash-to-master-operation/cash-to-master-operation.component';
import {MedicalAttendanceOperationComponent} from '../medical-attendance-operation/medical-attendance-operation.component';
import {CertificateRenewalOperationComponent} from '../certificate-renewal-operation/certificate-renewal-operation.component';
import {ActivatedRoute} from '@angular/router';


@Component({
    selector: 'app-operation-list',
    templateUrl: './operation-list.component.html',
    styleUrls: ['./operation-list.component.scss']
})
export class OperationListComponent implements OnInit {

    mission_types = [
        { 'title': CrewChangeOperationComponent.title, 'component': CrewChangeOperationComponent, },
        { 'title': DeliveryOperationComponent.title, 'component': DeliveryOperationComponent, },
        { 'title': LandingOperationComponent.title, 'component': LandingOperationComponent, },
        { 'title': GarbageRemovalOperationComponent.title, 'component': GarbageRemovalOperationComponent, },
        { 'title': CashToMasterOperationComponent.title, 'component': CashToMasterOperationComponent, },
        { 'title': MedicalAttendanceOperationComponent.title, 'component': MedicalAttendanceOperationComponent, },
        { 'title': CertificateRenewalOperationComponent.title, 'component': CertificateRenewalOperationComponent, },
    ];
    missions: any[];
    folder: any;

    showAddGarbageModal = false;
    showAddCtm = false;
    showAddCertificateRenewal = false;
    showAddMedicalAttendance = false;
    showAddDelivery = false;
    showAddLanding = false;

    @Input() folderId = null;

    @ViewChild('operationsZone', {read : ViewContainerRef}) target: ViewContainerRef;
    private componentRef: ComponentRef<any>;


    constructor( private apiProvider: ApiService, private resolver: ComponentFactoryResolver, private route: ActivatedRoute) { }

    ngOnInit() {
        console.log('FOLDER_FROM OP L: ', this.folderId);
        //
        this.apiProvider.getFolder(this.folderId).subscribe((folder) => {
            console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', folder['operations']);
        });
        this.fetchFolder(this.route.snapshot.params['id']);
    }

    fetchFolder(id) {
        this.apiProvider.getFolder(id)
            .pipe(first())
            .subscribe((folder: any[]) => {
                this.folder = folder[0];

                for (let i = 0; i < this.folder['operations'].length; ++i) {
                    if (this.folder['operations'][i]['crewChange']) {
                        this.addCrewChange(this.folder['operations'][i]['crewChange']['id']);
                    } else if (this.folder['operations'][i]['garbageRemoval']) {
                        this.addGarbageRemoval(this.folder['operations'][i]['garbageRemoval']['id']);
                    } else if (this.folder['operations'][i]['ctm']) {
                        this.addCashToMaster(this.folder['operations'][i]['ctm']['id']);
                    } else if (this.folder['operations'][i]['certificateRenewal']) {
                        this.addCertificateRenewal(this.folder['operations'][i]['certificateRenewal']['id']);
                    } else if (this.folder['operations'][i]['delivery']) {
                        this.addDelivery(this.folder['operations'][i]['delivery']['id']);
                    } else if (this.folder['operations'][i]['landing']) {
                        this.addDelivery(this.folder['operations'][i]['landing']['id']);
                    }
                }
            }, err => {
                console.log(err);
                // this.showSuccess('error', 'Notification', err.error.message);
                // this.router.navigate(['/dossier/liste']);
            });
    }

    addNewComponent(missionType) {
        switch (missionType['title']) {
            case CrewChangeOperationComponent.title:
                this.addCrewChange();
                break;
            case DeliveryOperationComponent.title:
                this.toggleAddDelivery();
                break;
            case LandingOperationComponent.title:
                this.toggleAddLanding();
                break;
            case CashToMasterOperationComponent.title:
                this.toggleAddCtm();
                break;
            case MedicalAttendanceOperationComponent.title:
                this.toggleAddMedicalAttendance();
                break;
            case  CertificateRenewalOperationComponent.title:
                this.toggleAddCertificateRenewal();
                break;
            case GarbageRemovalOperationComponent.title:
                this.toggleAddGarbage();
                break;
        }
    }

    toggleAddGarbage = () => {
        this.showAddGarbageModal = !this.showAddGarbageModal;
    }
    toggleAddCtm = () => {
        this.showAddCtm = !this.showAddCtm;
    }
    toggleAddCertificateRenewal = () => {
        this.showAddCertificateRenewal = !this.showAddCertificateRenewal;
    }
    toggleAddMedicalAttendance = () => {
        this.showAddMedicalAttendance = !this.showAddMedicalAttendance;
    }
    toggleAddDelivery = () => {
        this.showAddDelivery = !this.showAddDelivery;
    }
    toggleAddLanding = () => {
        this.showAddLanding = !this.showAddLanding;
    }


    addCrewChange(opId = null) {
        if (opId) {
            const childComponent = this.resolver.resolveComponentFactory(CrewChangeOperationComponent);
            this.componentRef = this.target.createComponent(childComponent);
            this.componentRef.instance.folderId = this.route.snapshot.params['id'];
            this.componentRef.instance.crewChangeId = opId;
        } else {
            this.apiProvider.saveCrewChange({
                folder_id: this.folderId
            }).subscribe((operation) => {
                const childComponent = this.resolver.resolveComponentFactory(CrewChangeOperationComponent);
                this.componentRef = this.target.createComponent(childComponent);
                this.componentRef.instance.folderId = this.route.snapshot.params['id'];
                this.componentRef.instance.crewChangeId = operation['id'];
            });
        }
    }

    addDelivery(opId = null) {
        const childComponent = this.resolver.resolveComponentFactory(DeliveryOperationComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.deliveryId = opId;
    }

    addLanding(opId = null) {
        const childComponent = this.resolver.resolveComponentFactory(LandingOperationComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.landingId = opId;
    }

    addCashToMaster(opId = null) {
        console.log(opId);
        const childComponent = this.resolver.resolveComponentFactory(CashToMasterOperationComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.ctmId = opId;
    }

    addGarbageRemoval(opId = null) {
        console.log(opId);
        const childComponent = this.resolver.resolveComponentFactory(GarbageRemovalOperationComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.garbageId = opId;
    }

    addMedicalAttendance(opId = null) {
        console.log(opId);
        const childComponent = this.resolver.resolveComponentFactory(MedicalAttendanceOperationComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.medicalAttendanceId = opId;
    }

    addCertificateRenewal(opId = null) {
        console.log(opId);
        const childComponent = this.resolver.resolveComponentFactory(CertificateRenewalOperationComponent);
        this.componentRef = this.target.createComponent(childComponent);
        this.componentRef.instance.folderId = this.route.snapshot.params['id'];
        this.componentRef.instance.certificateRenewalId = opId;
    }
}
