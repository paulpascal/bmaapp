import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingOperationComponent } from './landing-operation.component';
import {SharedModule} from '../../shared.module';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {AddVesselModule} from '../../views/modals/add-vessel/add-vessel.module';
import {LandingOperationModalModule} from '../../views/modals/landing-operation-modal/landing-operation-modal.module';


@NgModule({
  declarations: [LandingOperationComponent],
  imports: [
    CommonModule,
      SharedModule,

      BsDropdownModule,
      AddVesselModule,
      LandingOperationModalModule
  ],
  exports: [LandingOperationComponent],

})
export class LandingOperationModule { }
