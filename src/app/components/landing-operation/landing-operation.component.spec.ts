import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingOperationComponent } from './landing-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: LandingOperationComponent;
  let fixture: ComponentFixture<LandingOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
