import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateRenewalOperationComponent } from './certificate-renewal-operation.component';

describe('CertificateRenewalOperationComponent', () => {
  let component: CertificateRenewalOperationComponent;
  let fixture: ComponentFixture<CertificateRenewalOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificateRenewalOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateRenewalOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
