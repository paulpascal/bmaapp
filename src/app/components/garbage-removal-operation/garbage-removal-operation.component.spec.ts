import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GarbageRemovalOperationComponent } from './garbage-removal-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: GarbageRemovalOperationComponent;
  let fixture: ComponentFixture<GarbageRemovalOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GarbageRemovalOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GarbageRemovalOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
