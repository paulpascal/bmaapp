import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashToMasterOperationComponent } from './cash-to-master-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: CashToMasterOperationComponent;
  let fixture: ComponentFixture<CashToMasterOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashToMasterOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashToMasterOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
