import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {GlobalProvider} from '../../global';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../_services';
import {Role} from '../../_models';
import {Router} from '@angular/router';

declare const $;

@Component({
  selector: 'app-cash-to-master-operation',
  templateUrl: './cash-to-master-operation.component.html',
  styleUrls: ['./cash-to-master-operation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CashToMasterOperationComponent implements OnInit {
    static title = 'CASH TO MASTER';

    @Input() folderId = null;
    @Input() ctmId = null;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    constructor(private globalProvider: GlobalProvider,  private apiProvider: ApiService, toasterService: ToasterService,
                private authService: AuthenticationService, private router: Router) {
        this.toasterService = toasterService;
    }

    color;
    boxId;
    ctm;
    isFolderFenced;

    isAccounter;

    ngOnInit() {
        this.boxId = 'CASH' + this.globalProvider.getRandomId();
        this.color = '#e5bbf7';
        this.listener();
        this.fetchCtm();
        this.fetchFolder();
        this.checkIfAccounting();
    }

    checkIfAccounting() {
        this.isAccounter = this.authService.hasRole(Role.Accounting);
    }

    listener() {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${this.boxId}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 2500);
    }


    fetchCtm() {
        this.apiProvider.getCTM(this.ctmId).subscribe((ctm) => {
            this.ctm = ctm[0];
            console.log('CTM:', ctm[0]);
        });
    }

    fetchFolder() {
        this.apiProvider.getFolder(this.folderId).subscribe((folder) => {
            this.isFolderFenced = folder[0]['isFenced'];
            console.log('FolderFenced:', this.isFolderFenced);
        });
    }

    removeCtm() {
        if (confirm('Etes vous sûr de vouloir supprimer?')) {
            this.apiProvider.deleteCTM(this.ctmId).subscribe((resp) => {
                window.location.reload();
                this.showSuccess('success', 'Notification', 'Operation supprimé avec succès!');
            });
        }
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    goToDetail() {
        this.router.navigate([`/dossier/detail/${this.folderId}/ctm/${this.ctmId}`]);
    }
}
