import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CertificateRenewalOperationDetailComponent } from './certificate-renewal-operation-detail.component';
import {SharedModule} from '../../shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToasterModule} from 'angular2-toaster';
import {NgSelectModule} from '@ng-select/ng-select';
import {LaddaModule} from 'angular2-ladda';
import {ModalsModule} from '../../views/modals';
import {AutoCompleteModule} from 'ng5-auto-complete';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {FileUploadModule} from 'ng2-file-upload';
import {AddVesselModule} from '../../views/modals/add-vessel/add-vessel.module';
import {AddHotelModule} from '../../views/modals/add-hotel/add-hotel.module';
import {AddCrewModule} from '../../views/modals/add-crew/add-crew.module';
import {OktbOperationModule} from '../../views/modals/oktb-operation/oktb-operation.module';
import {TransportOperationModule} from '../../views/modals/transport-operation/transport-operation.module';
import {VisaOperationModule} from '../../views/modals/visa-operation/visa-operation.module';

@NgModule({
  declarations: [CertificateRenewalOperationDetailComponent],
  imports: [
    CommonModule,
      SharedModule,

      BsDropdownModule,
      AddVesselModule,
  ],
  exports: [CertificateRenewalOperationDetailComponent],

})
export class CertificateRenewalOperationDetailModule { }
