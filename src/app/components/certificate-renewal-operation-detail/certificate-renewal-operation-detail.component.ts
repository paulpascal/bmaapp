import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {GlobalProvider} from '../../global';
import {ApiService, AuthenticationService} from '../../_services';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {Role} from '../../_models';

declare const $;

@Component({
  selector: 'app-certificate-renewal-operation-detail',
  templateUrl: './certificate-renewal-operation-detail.component.html',
  styleUrls: ['./certificate-renewal-operation-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CertificateRenewalOperationDetailComponent implements OnInit {

    static title = 'CERTIFICATE RENEWAL';

    @Input() folderId = null;
    @Input() certificateRenewalId = null;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    // tslint:disable-next-line:max-line-length
    private isFolderFenced: any;

    constructor(private globalProvider: GlobalProvider,  private apiProvider: ApiService, toasterService: ToasterService, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    color;
    boxId;
    certificate;
    isAccounter;

    ngOnInit() {
        this.boxId = 'CCRENEWAL' + this.globalProvider.getRandomId();
        this.color = '#b6d2bf';
        this.listener();
        this.fetchCertificateRenewal();
        this.checkIfAccounting();
        this.fetchFolder();
    }

    checkIfAccounting() {
        this.isAccounter = this.authService.hasRole(Role.Accounting);
    }

    listener() {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${this.boxId}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 5000);
    }


    fetchCertificateRenewal() {
        this.apiProvider.getCertificateRenewal(this.certificateRenewalId).subscribe((gb) => {
            this.certificate = gb[0];
            console.log('CCRENEWAL:', gb[0]);
        });
    }

    fetchFolder() {
        this.apiProvider.getFolder(this.folderId).subscribe((folder) => {
            this.isFolderFenced = folder[0]['isFenced'];
            console.log('FolderFenced:', this.isFolderFenced);
        });
    }

    removeCertificateRenewal() {
        if (confirm('Etes vous sûr de vouloir supprimer?')) {
            this.apiProvider.deleteCertificateRenewal(this.certificateRenewalId).subscribe((resp) => {
                window.location.reload();
                this.showSuccess('success', 'Notification', 'Operation supprimé avec succès!');
            });
        }
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

}
