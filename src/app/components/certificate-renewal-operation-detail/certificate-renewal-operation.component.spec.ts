import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertificateRenewalOperationDetailComponent } from './certificate-renewal-operation.component';

describe('CertificateRenewalOperationComponent', () => {
  let component: CertificateRenewalOperationDetailComponent;
  let fixture: ComponentFixture<CertificateRenewalOperationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertificateRenewalOperationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateRenewalOperationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
