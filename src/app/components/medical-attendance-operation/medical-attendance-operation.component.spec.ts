import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalAttendanceOperationComponent } from './medical-attendance-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: MedicalAttendanceOperationComponent;
  let fixture: ComponentFixture<MedicalAttendanceOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalAttendanceOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalAttendanceOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
