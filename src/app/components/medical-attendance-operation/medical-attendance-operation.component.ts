import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {GlobalProvider} from '../../global';

declare const $;

@Component({
  selector: 'app-medical-attendance-operation',
  templateUrl: './medical-attendance-operation.component.html',
  styleUrls: ['./medical-attendance-operation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class MedicalAttendanceOperationComponent implements OnInit {
    static title = 'MEDICAL ATTENDANCE';
    constructor(private globalProvider: GlobalProvider) {
    }

    showAddCrewModal = false;
    showAddVesselModal = false;
    showAddHotelModal = false;
    showOktbOpsModal = false;
    showVisaOpsModal = false;
    showTransportOpsModal = false;
    color;
    boxId;

    @Input() folderId = null;

    ngOnInit() {
        this.boxId = 'MEDICAL' + this.globalProvider.getRandomId();
        this.color =  this.color = '#ffcde4';
        this.listener();
    }


    toggleAddCrew = () => {
        this.showAddCrewModal = !this.showAddCrewModal;
    }
    toggleAddVessel = () => {
        this.showAddVesselModal = !this.showAddVesselModal;
    }
    toggleAddHotel = () => {
        this.showAddHotelModal = !this.showAddHotelModal;
    }
    toggleOktbOpsModal = () => {
        this.showOktbOpsModal = !this.showOktbOpsModal;
    }
    toggleVisaOpsModal = () => {
        this.showVisaOpsModal = !this.showVisaOpsModal;
    }
    toggleTransportOpsModal = () => {
        this.showTransportOpsModal = !this.showTransportOpsModal;
    }

    addVessel($event) {}
    addHotel($event) {}
    addCrew($event) {}

    listener() {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${this.boxId}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 1000);
    }

}
