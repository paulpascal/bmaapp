import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashToMasterOperationDetailComponent } from './cash-to-master-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: CashToMasterOperationDetailComponent;
  let fixture: ComponentFixture<CashToMasterOperationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashToMasterOperationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashToMasterOperationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
