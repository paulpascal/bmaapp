import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryOperationComponent } from './delivery-operation.component';
import {SharedModule} from '../../shared.module';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {AddVesselModule} from '../../views/modals/add-vessel/add-vessel.module';
import {DeliveryOperationModalModule} from '../../views/modals/delivery-operation-modal/delivery-operation-modal.module';


@NgModule({
  declarations: [DeliveryOperationComponent],
  imports: [
    CommonModule,
      SharedModule,

      BsDropdownModule,
      AddVesselModule,
      DeliveryOperationModalModule
  ],
  exports: [DeliveryOperationComponent],

})
export class DeliveryOperationModule { }
