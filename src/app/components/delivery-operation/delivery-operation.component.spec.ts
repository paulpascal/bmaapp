import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryOperationComponent } from './delivery-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: DeliveryOperationComponent;
  let fixture: ComponentFixture<DeliveryOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
