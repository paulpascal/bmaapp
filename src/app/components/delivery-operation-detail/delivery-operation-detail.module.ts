import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryOperationDetailComponent } from './delivery-operation-detail.component';
import {SharedModule} from '../../shared.module';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {AddVesselModule} from '../../views/modals/add-vessel/add-vessel.module';
import {DeliveryOperationModalModule} from '../../views/modals/delivery-operation-modal/delivery-operation-modal.module';


@NgModule({
  declarations: [DeliveryOperationDetailComponent],
  imports: [
    CommonModule,
      SharedModule,

      BsDropdownModule,
      AddVesselModule,
      DeliveryOperationModalModule
  ],
  exports: [DeliveryOperationDetailComponent],

})
export class DeliveryOperationDetailModule { }
