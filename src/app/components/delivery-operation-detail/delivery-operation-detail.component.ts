import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {GlobalProvider} from '../../global';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../_services';
import {Role} from '../../_models';

declare const $;

@Component({
  selector: 'app-delivery-operation-detail',
  templateUrl: './delivery-operation-detail.component.html',
  styleUrls: ['./delivery-operation-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class DeliveryOperationDetailComponent implements OnInit {

    static title = 'DELIVERY';

    @Input() folderId = null;
    @Input() deliveryId = null;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    // tslint:disable-next-line:max-line-length
    private isFolderFenced: any;
    // tslint:disable-next-line:max-line-length
    constructor(private globalProvider: GlobalProvider,  private apiProvider: ApiService, toasterService: ToasterService, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    color;
    boxId;
    delivery;
    isAccounter;

  ngOnInit() {
      this.boxId = 'DELIVERY' + this.globalProvider.getRandomId();
      this.color = '#fdf7e3';
      this.listener();
      this.fetchDelivery();
      this.checkIfAccounting();
      this.fetchFolder();
  }

    checkIfAccounting() {
        this.isAccounter = this.authService.hasRole(Role.Accounting);
    }

    fetchFolder() {
        this.apiProvider.getFolder(this.folderId).subscribe((folder) => {
            this.isFolderFenced = folder[0]['isFenced'];
            console.log('FolderFenced:', this.isFolderFenced);
        });
    }


  listener() {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${this.boxId}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 2000);
    }

    fetchDelivery() {
        this.apiProvider.getDelivery(this.deliveryId).subscribe((dl) => {
            this.delivery = dl[0];
            console.log('DELIVERY:', dl[0]);
        });
    }

    removeDelivery() {
        if (confirm('Etes vous sûr de vouloir supprimer?')) {
            this.apiProvider.deleteDelivery(this.deliveryId).subscribe((resp) => {
                window.location.reload();
                this.showSuccess('success', 'Notification', 'Operation supprimé avec succès!');
            });
        }
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }


}
