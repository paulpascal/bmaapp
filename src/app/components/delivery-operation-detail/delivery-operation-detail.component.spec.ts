import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryOperationDetailComponent } from './delivery-operation-detail.component';

describe('CrewChangeOperationComponent', () => {
  let component: DeliveryOperationDetailComponent;
  let fixture: ComponentFixture<DeliveryOperationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryOperationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryOperationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
