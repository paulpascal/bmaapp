import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {GlobalProvider} from '../../global';
import {ApiService, AuthenticationService} from '../../_services';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {Role} from '../../_models';
import {Router} from '@angular/router';

declare const $;

@Component({
  selector: 'app-crew-change-operation',
  templateUrl: './crew-change-operation.component.html',
  styleUrls: ['./crew-change-operation.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class CrewChangeOperationComponent implements OnInit {

    static title = 'CREW CHANGE';

    @Input() folderId = null;
    @Input() crewChangeId = null;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    // tslint:disable-next-line:max-line-length
    private isFolderFenced: any;
    // tslint:disable-next-line:max-line-length
    constructor(private globalProvider: GlobalProvider,  private apiProvider: ApiService, toasterService: ToasterService,
                private authService: AuthenticationService, private router: Router) {
        this.toasterService = toasterService;
    }

    showAddCrewModal = false;
    showAddVesselModal = false;
    showAddHotelModal = false;
    showOktbOpsModal = false;
    showVisaOpsModal = false;
    showTransportOpsModal = false;
    showTransfertOpsModal = false;
    showBoardingOpsModal = false;
    showDisembarkOpsModal = false;
    color;
    boxId;
    crewChange;
    isAccounter;

    ngOnInit() {
        this.boxId = 'CREWCHANGE' + this.globalProvider.getRandomId();
        this.color = '#acdeda';
        this.fetchCrewChange();
        this.listener();
        console.log(
            `FOLDER ID ${this.folderId} -  CROP ID ${this.crewChangeId}`
        );
        this.checkIfAccounting();
        this.fetchFolder();
    }

    checkIfAccounting() {
         this.isAccounter = this.authService.hasRole(Role.Accounting);
    }


    toggleAddCrew = () => {
        this.showAddCrewModal = !this.showAddCrewModal;
    }
    toggleAddVessel = () => {
        this.showAddVesselModal = !this.showAddVesselModal;
    }
    toggleAddHotel = () => {
        this.showAddHotelModal = !this.showAddHotelModal;
    }
    toggleOktbOpsModal = () => {
        this.showOktbOpsModal = !this.showOktbOpsModal;
    }
    toggleVisaOpsModal = () => {
        this.showVisaOpsModal = !this.showVisaOpsModal;
    }
    toggleTransportOpsModal = () => {
        this.showTransportOpsModal = !this.showTransportOpsModal;
    }
    toggleBoardingOpsModal = () => {
        this.showBoardingOpsModal = !this.showBoardingOpsModal;
    }
    toggleDisembarkOpsModal = () => {
        this.showDisembarkOpsModal = !this.showDisembarkOpsModal;
    }
    toggleTransfertOpsModal = () => {
        this.showTransfertOpsModal = !this.showTransfertOpsModal;
    }

    addVessel($event) {}
    addHotel($event) {}
    addCrew($event) {
        // window.location.reload();
    }
    addTransport($event) {
        window.location.reload();
    }

    listener() {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${this.boxId}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 500);
    }

    fetchCrewChange() {
        this.apiProvider.getCrewChange(this.crewChangeId).subscribe((cc) => {
            this.crewChange = cc;
            console.log('CREWCHANGE:', cc);
        });
    }

    fetchFolder() {
        this.apiProvider.getFolder(this.folderId).subscribe((folder) => {
            this.isFolderFenced = folder[0]['isFenced'];
            console.log('FolderFenced:', this.isFolderFenced);
        });
    }

    removeCrewChange() {
        if (confirm('Etes vous sûr de vouloir supprimer?')) {
            this.apiProvider.deleteCrewChange(this.crewChangeId).subscribe((resp) => {
                window.location.reload();
                this.showSuccess('success', 'Notification', 'Operation supprimé avec succès!');
            });
        }
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

    goToDetail() {
        console.log('DDDDDDDDDDDDDDDDD');
        this.router.navigate([`/dossier/detail/${this.folderId}/crew-change/${this.crewChangeId}`]);
    }
}
