import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrewChangeOperationComponent } from './crew-change-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: CrewChangeOperationComponent;
  let fixture: ComponentFixture<CrewChangeOperationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrewChangeOperationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrewChangeOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
