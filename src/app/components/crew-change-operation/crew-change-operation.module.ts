import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrewChangeOperationComponent } from './crew-change-operation.component';
import {SharedModule} from '../../shared.module';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {AddVesselModule} from '../../views/modals/add-vessel/add-vessel.module';
import {AddHotelModule} from '../../views/modals/add-hotel/add-hotel.module';
import {AddCrewModule} from '../../views/modals/add-crew/add-crew.module';
import {OktbOperationModule} from '../../views/modals/oktb-operation/oktb-operation.module';
import {TransportOperationModule} from '../../views/modals/transport-operation/transport-operation.module';
import {VisaOperationModule} from '../../views/modals/visa-operation/visa-operation.module';
import {TransfertOperationModule} from '../../views/modals/transfert-operation/transfert-operation.module';
import {BoardingOperationModule} from '../../views/modals/boarding-operation/boarding-operation.module';
import {DisembarkOperationModule} from '../../views/modals/disembark-operation/disembark-operation.module';

@NgModule({
  declarations: [CrewChangeOperationComponent],
  imports: [
    CommonModule,
      SharedModule,

      BsDropdownModule,
      AddVesselModule,
      AddHotelModule,
      AddCrewModule,
      OktbOperationModule,
      TransportOperationModule,
      TransfertOperationModule,
      BoardingOperationModule,
      DisembarkOperationModule,
      VisaOperationModule,
  ],
  exports: [CrewChangeOperationComponent],

})
export class CrewChangeOperationModule { }
