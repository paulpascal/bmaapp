import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsignationEditComponent } from './consignation-edit.component';
import {ModalsModule} from '../../views/modals';
import {AutoCompleteModule} from 'ng5-auto-complete';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {SharedModule} from '../../shared.module';
import {ToasterModule} from 'angular2-toaster';
import {ReactiveFormsModule} from '@angular/forms';
import {LaddaModule} from 'angular2-ladda';
import {NgSelectModule} from '@ng-select/ng-select';

@NgModule({
  declarations: [ConsignationEditComponent],
  imports: [
    CommonModule,
    ModalsModule,
    AutoCompleteModule,
    BsDropdownModule,
    BsDatepickerModule.forRoot(),
    ToasterModule,
    SharedModule,
    ReactiveFormsModule,
    LaddaModule,
    NgSelectModule
  ],
  exports: [
      ConsignationEditComponent
  ]
})
export class ConsignationEditModule { }
