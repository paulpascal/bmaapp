import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {ApiService, AuthenticationService} from '../../_services';
import {ActivatedRoute, Router} from '@angular/router';
import {GlobalProvider} from '../../global';

@Component({
  selector: 'app-consignation-edit',
  templateUrl: './consignation-edit.component.html',
  styleUrls: ['./consignation-edit.component.scss']
})
export class ConsignationEditComponent implements OnInit {

    form: FormGroup;

    error = false;
    loading = false;
    submitted = false;
    ship: any;
    customer: any;
    folder: any;
    date = new Date();
    lastDayDate = new Date(new Date().getFullYear(), 11, 31);

    consignation_types: any[];

    showCreateShipModal = false;
    showSelectShipModal = false;
    showCreateCustomerModal = false;
    showSelectCustomerModal = false;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });

    @Output() onCreate = new EventEmitter<any>();

    @ViewChild('shipChoiceModal') shipChoiceModal;

    constructor(private formBuilder: FormBuilder, private apiProvider: ApiService, private authService: AuthenticationService,
                toasterService: ToasterService, private router: Router,  private global: GlobalProvider, private route: ActivatedRoute) {
        this.toasterService = toasterService; }

    ngOnInit() {
        this.fetchConsignationType();
        this.fetchFolder();
        this.initForm();
    }

    initForm() {

        this.form = this.formBuilder.group({
            ship: [null, Validators.required],
            customer: [null, Validators.required],
            customerRef: [null, Validators.required],
            consignationType: [null, Validators.required],
            openDate: [null],
            closeDate: [null],
            shipArrivalDate: [null,  Validators.required],
            shipDepartureDate: [this.lastDayDate],
            isNominationReceived: [false],
            isPortInfoSent: [false],
            isNavyClearance: [false],
            isHusbandry: [false],
        });
    }

    onSubmit() {
        this.submitted = true;
        if (this.form.invalid) {
            return ;
        }

        this.loading = true;
        const consignationData = {
            ship: this.f.ship.value,
            customer: this.f.customer.value,
            customerRef: this.f.customerRef.value,
            consignation_type_id: this.f.consignationType.value,
            open_date: this.date,
            close_date: this.f.closeDate.value,
            shipArrivalDate: this.f.shipArrivalDate.value,
            shipDepartureDate: this.f.shipDepartureDate.value,
            isNominationReceived: this.f.isNominationReceived.value,
            isPortInfoSent: this.f.isPortInfoSent.value,
            isNavyClearance: this.f.isNavyClearance.value,
            isHusbandry: this.f.isHusbandry.value,
        };

        console.log(this.folder.consignation.id, consignationData);

        this.apiProvider.editConsignation(this.folder.consignation.id, consignationData)
            .pipe(first())
            .subscribe(folder => {
                    console.log(folder);
                    this.onCreate.emit(folder);
                    this.submitted = false;
                    this.loading = false;
                    this.error = null;
                    this.showSuccess('success', 'Opération réussie!', 'Consignation ajoutée avec succès!');
                    this.form.reset();
                },
                error => {
                    this.error = error;
                    this.loading = false;
                    this.submitted = false;
                });
    }

    fetchConsignationType() {
        this.apiProvider.getConsignationTypes()
            .pipe(first())
            .subscribe((consignation_types: any[]) => {
                this.consignation_types = consignation_types;
            });
    }

    fetchFolder() {
        this.apiProvider.getFolder(this.route.snapshot.params['id'])
            .pipe(first())
            .subscribe((folder: any[]) => {
                this.folder = folder[0];
                this.ship = this.folder.ship;
                this.customer = this.folder.customer;
                this.form.controls['ship'].setValue(this.folder.ship_id);
                this.form.controls['customer'].setValue(this.folder.customer_id);
                this.form.controls['consignationType'].setValue(this.folder.consignation.consignation_type_id);
                this.form.controls['customerRef'].setValue(this.folder.customerRef);
                this.form.controls['shipArrivalDate'].setValue(this.folder.shipArrivalDate);
                this.form.controls['shipDepartureDate'].setValue(this.folder.shipDepartureDate);
                this.form.controls['isNominationReceived'].setValue(this.folder.consignation.isNominationReceived);
                this.form.controls['isPortInfoSent'].setValue(this.folder.consignation.isPortInfoSent);
                this.form.controls['isNavyClearance'].setValue(this.folder.consignation.isNavyClearance);
                this.form.controls['isHusbandry'].setValue(this.folder.consignation.isHusbandry);
            }, err => {
                console.log(err);
                this.showSuccess('error', 'Notification', err.error.message);
                this.router.navigate(['/dossier/liste']);
            });
    }

    setConsignationType($event) {
        console.log('Cons Type got: ', $event);
        this.form.controls['consignationType'].setValue($event);

    }
    setCustomer($event) {
        console.log('Customer got: ', $event);
        this.form.controls['customer'].setValue($event);
        this.apiProvider.getCustomer($event)
            .pipe(first())
            .subscribe(customer => {
                this.customer = customer;
            });
    }
    setShip($event) {
        console.log('Ship got: ', $event);
        this.form.controls['ship'].setValue($event);
        this.apiProvider.getShip($event)
            .pipe(first())
            .subscribe(ship => {
                this.ship = ship;
            });
    }

    toggleLoading() {
        this.loading = !this.loading;
    }
    toggleCreateShip = () => {
        if (this.showSelectShipModal) {
            this.toggleSelectShip();
        }
        this.showCreateShipModal = !this.showCreateShipModal;
        return true;
    }
    toggleSelectShip = () => {
        this.showSelectShipModal = !this.showSelectShipModal;
        return true;
    }

    toggleCreateCustomer = () => {
        if (this.showSelectCustomerModal) {
            this.toggleSelectCustomer();
        }
        this.showCreateCustomerModal = !this.showCreateCustomerModal;
        return true;
    }
    toggleSelectCustomer = () => {
        this.showSelectCustomerModal = !this.showSelectCustomerModal;
        return true;
    }

    get f() {return this.form.controls; }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }
}
