import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignationEditComponent } from './consignation-edit.component';

describe('ConsignationEditComponent', () => {
  let component: ConsignationEditComponent;
  let fixture: ComponentFixture<ConsignationEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignationEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignationEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
