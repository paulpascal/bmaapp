import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GarbageRemovalOperationDetailComponent } from './garbage-removal-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: GarbageRemovalOperationDetailComponent;
  let fixture: ComponentFixture<GarbageRemovalOperationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GarbageRemovalOperationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GarbageRemovalOperationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
