import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {GlobalProvider} from '../../global';
import {ApiService, AuthenticationService} from '../../_services';
import {ToasterConfig, ToasterService} from 'angular2-toaster';
import {Role} from '../../_models';

declare const $;

@Component({
  selector: 'app-garbage-removal-operation-detail',
  templateUrl: './garbage-removal-operation-detail.component.html',
  styleUrls: ['./garbage-removal-operation-detail.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class GarbageRemovalOperationDetailComponent implements OnInit {
    static title = 'GARBAGE REMOVAL';

    @Input() folderId = null;
    @Input() garbageId = null;

    private toasterService: ToasterService;
    public toasterconfig: ToasterConfig = new ToasterConfig({
        tapToDismiss: true,
        timeout: 5000
    });
    private isFolderFenced: any;

    constructor(private globalProvider: GlobalProvider,  private apiProvider: ApiService, toasterService: ToasterService, private authService: AuthenticationService) {
        this.toasterService = toasterService;
    }

    showGarbageModal = false;
    garbage;
    color;
    boxId;
    isAccounter;

    ngOnInit() {
        this.boxId = 'GBREMOVAL' + this.globalProvider.getRandomId();
        this.color = '#e1ecf4';
        this.fetchGarbageRemoval();
        this.listener();
        console.log(
            `FOLDER ID ${this.folderId} -  GARBAGE ID ${this.garbageId}`
        );
        this.checkIfAccounting();
        this.fetchFolder();
    }

    checkIfAccounting() {
        this.isAccounter = this.authService.hasRole(Role.Accounting);
    }

    fetchFolder() {
        this.apiProvider.getFolder(this.folderId).subscribe((folder) => {
            this.isFolderFenced = folder[0]['isFenced'];
            console.log('FolderFenced:', this.isFolderFenced);
        });
    }


    toggleAddGarbage = () => {
        this.showGarbageModal = !this.showGarbageModal;
    }


    addGarbage($event) {}


    listener() {
        // Collapse ibox function
        setTimeout(() => {
            $(`a#collapse-link${this.boxId}`).click(function () {
                const ibox = $(this).closest('div.ibox');
                const button = $(this).find('i');
                const content = ibox.find('div.ibox-content');
                content.slideToggle(200);
                button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                // button.toggleClass('fa-plus').toggleClass('fa-minus');
                ibox.toggleClass('').toggleClass('border-bottom');
                setTimeout(function () {
                    ibox.resize();
                    ibox.find('[id^=map-]').resize();
                }, 50);
            });
        }, 1500);
    }

    fetchGarbageRemoval() {
        this.apiProvider.getGarbageRemoval(this.garbageId).subscribe((gb) => {
            this.garbage = gb[0];
            console.log('GARBAGE:', gb[0]);
        });
    }

    removeGarbageRemoval() {
        if (confirm('Etes vous sûr de vouloir supprimer?')) {
            this.apiProvider.deleteGarbageRemoval(this.garbageId).subscribe((resp) => {
                window.location.reload();
                this.showSuccess('success', 'Notification', 'Operation supprimé avec succès!');
            });
        }
    }

    showSuccess(type, title, body) {
        this.toasterService.pop(type, title, body);
    }

}
