import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingOperationDetailComponent } from './landing-operation-detail.component';

describe('CrewChangeOperationComponent', () => {
  let component: LandingOperationDetailComponent;
  let fixture: ComponentFixture<LandingOperationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingOperationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingOperationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
