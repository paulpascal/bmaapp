import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingOperationDetailComponent } from './landing-operation-detail.component';
import {SharedModule} from '../../shared.module';
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import {AddVesselModule} from '../../views/modals/add-vessel/add-vessel.module';
import {LandingOperationModalModule} from '../../views/modals/landing-operation-modal/landing-operation-modal.module';


@NgModule({
  declarations: [LandingOperationDetailComponent],
  imports: [
    CommonModule,
      SharedModule,
      BsDropdownModule,
      AddVesselModule,
      LandingOperationModalModule
  ],
  exports: [LandingOperationDetailComponent],

})
export class LandingOperationDetailModule { }
