import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsignationFormComponent } from './consignation-form.component';

describe('ConsignationFormComponent', () => {
  let component: ConsignationFormComponent;
  let fixture: ComponentFixture<ConsignationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsignationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsignationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
