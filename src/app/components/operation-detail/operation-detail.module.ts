import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OperationDetailComponent} from './operation-detail.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import {CrewChangeOperationModule} from '../crew-change-operation/crew-change-operation.module';
import {AddVesselModule} from '../../views/modals/add-vessel/add-vessel.module';
import {AddHotelModule} from '../../views/modals/add-hotel/add-hotel.module';
import {AddCrewModule} from '../../views/modals/add-crew/add-crew.module';
import {OktbOperationModule} from '../../views/modals/oktb-operation/oktb-operation.module';
import {TransportOperationModule} from '../../views/modals/transport-operation/transport-operation.module';
import {VisaOperationModule} from '../../views/modals/visa-operation/visa-operation.module';
import {DeliveryOperationModule} from '../delivery-operation/delivery-operation.module';
import {LandingOperationModule} from '../landing-operation/landing-operation.module';
import {GarbageRemovalOperationModule} from '../garbage-removal-operation/garbage-removal-operation.module';
import {MedicalAttendanceOperationModule} from '../medical-attendance-operation/medical-attendance-operation.module';
import {CertificateRenewalOperationModule} from '../certificate-renewal-operation/certificate-renewal-operation.module';
import {CashToMasterOperationModule} from '../cash-to-master-operation/cash-to-master-operation.module';
import {CrewChangeOperationComponent} from '../crew-change-operation/crew-change-operation.component';
import {DeliveryOperationComponent} from '../delivery-operation/delivery-operation.component';
import {MedicalAttendanceOperationComponent} from '../medical-attendance-operation/medical-attendance-operation.component';
import {GarbageRemovalOperationComponent} from '../garbage-removal-operation/garbage-removal-operation.component';
import {CertificateRenewalOperationComponent} from '../certificate-renewal-operation/certificate-renewal-operation.component';
import {CashToMasterOperationComponent} from '../cash-to-master-operation/cash-to-master-operation.component';
import {GarbageOperationModule} from '../../views/modals/garbage-operation/garbage-operation.module';
import {CtmOperationModule} from '../../views/modals/ctm-operation/ctm-operation.module';
import {CertificateRenewalModule} from '../../views/modals/certificate-renewal-modal/certificate-renewal-operation.module';
import {MedicalAttendanceModule} from '../../views/modals/medical-attendance-modal/medical-attendance.module';
import {DeliveryOperationModalModule} from '../../views/modals/delivery-operation-modal/delivery-operation-modal.module';
import {LandingOperationModalModule} from '../../views/modals/landing-operation-modal/landing-operation-modal.module';


@NgModule({
    declarations: [OperationDetailComponent],
    imports: [
        CommonModule,
        BsDropdownModule,
        CrewChangeOperationModule,
        DeliveryOperationModule,
        LandingOperationModule,
        GarbageRemovalOperationModule,
        MedicalAttendanceOperationModule,
        CertificateRenewalOperationModule,
        CashToMasterOperationModule,
        GarbageOperationModule,
        CertificateRenewalModule,
        CtmOperationModule,
        MedicalAttendanceModule,
        GarbageOperationModule,
        DeliveryOperationModalModule,
        LandingOperationModalModule,
    ],
    exports: [OperationDetailComponent],
    entryComponents: [
        CrewChangeOperationComponent,
        DeliveryOperationComponent,
        LandingOperationModule,
        GarbageRemovalOperationComponent,
        MedicalAttendanceOperationComponent,
        CertificateRenewalOperationComponent,
        CashToMasterOperationComponent,
    ]
})
export class OperationDetailModule { }
