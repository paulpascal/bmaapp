import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalAttendanceOperationDetailComponent } from './medical-attendance-operation-detail.component';

describe('CrewChangeOperationComponent', () => {
  let component: MedicalAttendanceOperationDetailComponent;
  let fixture: ComponentFixture<MedicalAttendanceOperationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalAttendanceOperationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalAttendanceOperationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
