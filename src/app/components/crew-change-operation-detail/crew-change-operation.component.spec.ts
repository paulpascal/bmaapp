import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrewChangeOperationDetailComponent } from './crew-change-operation.component';

describe('CrewChangeOperationComponent', () => {
  let component: CrewChangeOperationDetailComponent;
  let fixture: ComponentFixture<CrewChangeOperationDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrewChangeOperationDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrewChangeOperationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
