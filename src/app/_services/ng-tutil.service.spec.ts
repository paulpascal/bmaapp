import { TestBed } from '@angular/core/testing';

import { NgTUtilService } from './ng-tutil.service';

describe('NgTUtilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgTUtilService = TestBed.get(NgTUtilService);
    expect(service).toBeTruthy();
  });
});
