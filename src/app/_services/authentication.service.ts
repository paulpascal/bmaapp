import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

import {GlobalProvider} from '../global';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {
    private readonly currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(private httpClient: HttpClient, private global: GlobalProvider) {
        this.currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserObject(): any {
        return this.currentUserSubject.value;
    }

    login(uid: string, password: string) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/auth/login`, {uid, password})
            .pipe(map(user => {
                // login is successful if there is jwt token in the response
                if (user && user.token) {
                    // store the new logged user and his token in the storage to keep him connected between pages
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }
                // ..
                return user;
            }));
    }

    register(data) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/auth/register`, data)
            .pipe(map(user => {
                // ..
                return user;
            }));
    }

    logout() {
        // remove the user from local storage to log him out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    hasRole(role) {
        return this.currentUserObject.roles.some((ur) => ur.label === role);
    }
}
