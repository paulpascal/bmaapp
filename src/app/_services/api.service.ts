import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {GlobalProvider} from '../global';
import {catchError, map, retry} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private httpClient: HttpClient, private global: GlobalProvider) {
    }

    ////////////////////   Role
    getRoles() {
        return this.httpClient.get(`${this.global.apiUrl}/roles`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getRole(roleId) {
        return this.httpClient.get(`${this.global.apiUrl}/roles/${roleId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveRole(roleData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/roles`, roleData)
            .pipe(
                map(role => role),
                catchError(this.handleError) // then handle the error
            );
    }
    editRole(roleId, roleData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/roles/${roleId}`, roleData)
            .pipe(
                map(role => role),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteRole(roleId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/roles/${roleId}`, )
            .pipe(
                map(resp => resp),
                catchError(this.handleError) // then handle the error
            );
    }
     /////////////////////////////////////////////////

    ////////////////////   User
    getUsers() {
        return this.httpClient.get(`${this.global.apiUrl}/users`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getUser(userId) {
        return this.httpClient.get(`${this.global.apiUrl}/users/${userId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getDrivers() {
        return this.httpClient.get(`${this.global.apiUrl}/drivers`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveUser(userData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/users`, userData)
            .pipe(
                map(user => user),
                catchError(this.handleError) // then handle the error
            );
    }
    editUser(userId, userData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/users/${userId}`, userData)
            .pipe(
                map(user => user),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteUser(userId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/users/${userId}`, )
            .pipe(
                map(user => user),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   User events
    getUserEvents() {
        return this.httpClient.get(`${this.global.apiUrl}/user_events`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getUserEvent(eventId) {
        return this.httpClient.get(`${this.global.apiUrl}/user_events/${eventId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveUserEvent(eventData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/user_events`, eventData)
            .pipe(
                map(event => event),
                catchError(this.handleError) // then handle the error
            );
    }
    editUserEvent(eventId, eventData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/user_events/${eventId}`, eventData)
            .pipe(
                map(event => event),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteUserEvent(eventId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/user_events/${eventId}`, )
            .pipe(
                map(event => event),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Ship
    getShips() {
        return this.httpClient.get(`${this.global.apiUrl}/ships`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getShip(shipId) {
        return this.httpClient.get(`${this.global.apiUrl}/ships/${shipId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveShip(shipData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/ships`, shipData)
            .pipe(
                map(ship => ship),
                catchError(this.handleError) // then handle the error
            );
    }
    editShip(shipId, shipData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/ships/${shipId}`, shipData)
            .pipe(
                map(ship => ship),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteShip(shipId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/ships/${shipId}`, )
            .pipe(
                map(ship => ship),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Boat
    getVessels() {
        return this.httpClient.get(`${this.global.apiUrl}/boats`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getVessel(vesselId) {
        return this.httpClient.get(`${this.global.apiUrl}/boats/${vesselId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveVessel(vesselData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/boats`, vesselData)
            .pipe(
                map(vessel => vessel),
                catchError(this.handleError) // then handle the error
            );
    }
    editVessel(vesselId, vesselData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/boats/${vesselId}`, vesselData)
            .pipe(
                map(vessel => vessel),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteVessel(vesselId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/vessels/${vesselId}`, )
            .pipe(
                map(vessel => vessel),
                catchError(this.handleError) // then handle the error
            );
    }
    getVesselTypes() {
        return this.httpClient.get(`${this.global.apiUrl}/boat_types`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getVesselType(vesselId) {
        return this.httpClient.get(`${this.global.apiUrl}/boat_types/${vesselId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Ship
    getCrews() {
        return this.httpClient.get(`${this.global.apiUrl}/crews`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getCrew(crewId) {
        return this.httpClient.get(`${this.global.apiUrl}/crews/${crewId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveCrew(crewData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/crews`, crewData)
            .pipe(
                map(crew => crew),
                catchError(this.handleError) // then handle the error
            );
    }
    editCrew(crewId, crewData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/crews/${crewId}`, crewData)
            .pipe(
                map(crew => crew),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteCrew(crewId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/crews/${crewId}`, )
            .pipe(
                map(crew => crew),
                catchError(this.handleError) // then handle the error
            );
    }
    getCrewTypes() {
        return this.httpClient.get(`${this.global.apiUrl}/crew_types`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getFolderCrews(folderId) {
        return this.httpClient.get(`${this.global.apiUrl}/folders/${folderId}/crews`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }

    /////////////////////////////////////////////////

    ////////////////////   Customer
    getCustomers() {
        return this.httpClient.get(`${this.global.apiUrl}/customers`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getCustomer(customerId) {
        return this.httpClient.get(`${this.global.apiUrl}/customers/${customerId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveCustomer(customerData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/customers`, customerData)
            .pipe(
                map(customer => customer),
                catchError(this.handleError) // then handle the error
            );
    }
    editCustomer(customerId, customerData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/customers/${customerId}`, customerData)
            .pipe(
                map(customer => customer),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteCustomer(customerId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/customers/${customerId}`, )
            .pipe(
                map(customer => customer),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Hotel
    getTransportCarriers() {
        return this.httpClient.get(`${this.global.apiUrl}/transport_carriers`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getTransportTypes() {
        return this.httpClient.get(`${this.global.apiUrl}/transport_types`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getHotels() {
        return this.httpClient.get(`${this.global.apiUrl}/hotels`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getHotel(hotelId) {
        return this.httpClient.get(`${this.global.apiUrl}/hotels/${hotelId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveHotel(hotelData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/hotels`, hotelData)
            .pipe(
                map(hotel => hotel),
                catchError(this.handleError) // then handle the error
            );
    }
    editHotel(hotelId, hotelData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/hotels/${hotelId}`, hotelData)
            .pipe(
                map(hotel => hotel),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteHotel(hotelId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/hotels/${hotelId}`, )
            .pipe(
                map(hotel => hotel),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Clinics
    getClinics() {
        return this.httpClient.get(`${this.global.apiUrl}/clinics`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getClinic(clinicId) {
        return this.httpClient.get(`${this.global.apiUrl}/clinics/${clinicId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveClinic(clinicData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/clinics`, clinicData)
            .pipe(
                map(clinic => clinic),
                catchError(this.handleError) // then handle the error
            );
    }
    editClinic(clinicId, clinicData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/clinics/${clinicId}`, clinicData)
            .pipe(
                map(clinic => clinic),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteClinic(clinicId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/clinics/${clinicId}`, )
            .pipe(
                map(clinic => clinic),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Oktb
    getOktbs() {
        return this.httpClient.get(`${this.global.apiUrl}/oktbs`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getOktb(oktbId) {
        return this.httpClient.get(`${this.global.apiUrl}/oktbs/${oktbId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveOktb(oktbData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/oktbs`, oktbData)
            .pipe(
                map(oktb => oktb),
                catchError(this.handleError) // then handle the error
            );
    }
    editOktb(oktbId, oktbData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/oktbs/${oktbId}`, oktbData)
            .pipe(
                map(oktb => oktb),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteOktb(oktbId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/oktbs/${oktbId}`, )
            .pipe(
                map(oktb => oktb),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Visa
    getVisas() {
        return this.httpClient.get(`${this.global.apiUrl}/oktbs`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getVisa(visaId) {
        return this.httpClient.get(`${this.global.apiUrl}/visas/${visaId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveVisa(visaData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/visas`, visaData)
            .pipe(
                map(visa => visa),
                catchError(this.handleError) // then handle the error
            );
    }
    editVisa(visaId, visaData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/visas/${visaId}`, visaData)
            .pipe(
                map(visa => visa),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteVisa(visaId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/visas/${visaId}`, )
            .pipe(
                map(visa => visa),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Transport
    getTransports() {
        return this.httpClient.get(`${this.global.apiUrl}/transports`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getTransport(transportId) {
        return this.httpClient.get(`${this.global.apiUrl}/transports/${transportId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveTransport(transportData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/transports`, transportData)
            .pipe(
                map(transport => transport),
                catchError(this.handleError) // then handle the error
            );
    }
    editTransport(transportId, transportData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/transports/${transportId}`, transportData)
            .pipe(
                map(transport => transport),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteTransport(transportId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/transports/${transportId}`, )
            .pipe(
                map(transport => transport),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    /////////////////////////////////////////////////

    ////////////////////   Boarding
    getBoardings() {
        return this.httpClient.get(`${this.global.apiUrl}/embarkations`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getBoarding(embarkationId) {
        return this.httpClient.get(`${this.global.apiUrl}/embarkations/${embarkationId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveBoarding(embarkationData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/embarkations`, embarkationData)
            .pipe(
                map(embarkation => embarkation),
                catchError(this.handleError) // then handle the error
            );
    }
    editBoarding(embarkationId, embarkationData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/embarkations/${embarkationId}`, embarkationData)
            .pipe(
                map(transport => transport),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteBoarding(embarkationId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/embarkations/${embarkationId}`, )
            .pipe(
                map(transport => transport),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    /////////////////////////////////////////////////

    ////////////////////   Disembarkation
    getDisembarkations() {
        return this.httpClient.get(`${this.global.apiUrl}/disembarkations`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getDisembarkation(disembarkationId) {
        return this.httpClient.get(`${this.global.apiUrl}/disembarkations/${disembarkationId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveDisembarkation(disembarkationData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/disembarkations`, disembarkationData)
            .pipe(
                map(disembarkation => disembarkation),
                catchError(this.handleError) // then handle the error
            );
    }
    editDisembarkation(disembarkationId, disembarkationData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/disembarkations/${disembarkationId}`, disembarkationData)
            .pipe(
                map(disembarkation => disembarkation),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteDisembarkation(disembarkationId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/disembarkations/${disembarkationId}`, )
            .pipe(
                map(disembarkation => disembarkation),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    /////////////////////////////////////////////////

    ////////////////////   Transfert
    getTransferts() {
        return this.httpClient.get(`${this.global.apiUrl}/transferts`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getTransfert(transfertId) {
        return this.httpClient.get(`${this.global.apiUrl}/transferts/${transfertId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveTransfert(transfertData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/transferts`, transfertData)
            .pipe(
                map(transfert => transfert),
                catchError(this.handleError) // then handle the error
            );
    }
    editTransfert(transfertId, transfertData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/transferts/${transfertId}`, transfertData)
            .pipe(
                map(transfert => transfert),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteTransfert(transfertId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/transferts/${transfertId}`, )
            .pipe(
                map(transfert => transfert),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////


    ////////////////////   CrewChange
    getCrewChanges() {
        return this.httpClient.get(`${this.global.apiUrl}/crew_changes`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getCrewChange(crewChangeId) {
        return this.httpClient.get(`${this.global.apiUrl}/crew_changes/${crewChangeId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveCrewChange(crewChangeData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/crew_changes`, crewChangeData)
            .pipe(
                map(crewChange => crewChange),
                catchError(this.handleError) // then handle the error
            );
    }
    editCrewChange(crewChangeId, crewChangeData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/crew_changes/${crewChangeId}`, crewChangeData)
            .pipe(
                map(crewChange => crewChange),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteCrewChange(crewChangeId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/crew_changes/${crewChangeId}`, )
            .pipe(
                map(crewChange => crewChange),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Delivery
    getDeliveries() {
        return this.httpClient.get(`${this.global.apiUrl}/delivery`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getDelivery(deliveryId) {
        return this.httpClient.get(`${this.global.apiUrl}/deliveries/${deliveryId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveDelivery(deliveryData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/deliveries`, deliveryData)
            .pipe(
                map(delivery => delivery),
                catchError(this.handleError) // then handle the error
            );
    }
    editDelivery(deliveryId, deliveryData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/deliveries/${deliveryId}`, deliveryData)
            .pipe(
                map(delivery => delivery),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteDelivery(deliveryId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/deliveries/${deliveryId}`, )
            .pipe(
                map(delivery => delivery),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Landing
    getLandings() {
        return this.httpClient.get(`${this.global.apiUrl}/landings`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getLanding(landingId) {
        return this.httpClient.get(`${this.global.apiUrl}/landings/${landingId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveLanding(landingData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/landings`, landingData)
            .pipe(
                map(landing => landing),
                catchError(this.handleError) // then handle the error
            );
    }
    editLanding(landingId, landingData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/landings/${landingId}`, landingData)
            .pipe(
                map(landing => landing),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteLanding(landingId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/landings/${landingId}`, )
            .pipe(
                map(landing => landing),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   CTM
    geCTMs() {
        return this.httpClient.get(`${this.global.apiUrl}/cash_to_masters`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getCTM(ctmId) {
        return this.httpClient.get(`${this.global.apiUrl}/cash_to_masters/${ctmId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveCTM(ctmData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/cash_to_masters`, ctmData)
            .pipe(
                map(ctm => ctm),
                catchError(this.handleError) // then handle the error
            );
    }
    editCTM(ctmId, ctmData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/cash_to_masters/${ctmId}`, ctmData)
            .pipe(
                map(ctm => ctm),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteCTM(ctmId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/cash_to_masters/${ctmId}`, )
            .pipe(
                map(ctm => ctm),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Garbage Removal
    geGarbageRemovals() {
        return this.httpClient.get(`${this.global.apiUrl}/garbage_removals`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getGarbageRemoval(gbId) {
        return this.httpClient.get(`${this.global.apiUrl}/garbage_removals/${gbId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveGarbageRemoval(gbData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/garbage_removals`, gbData)
            .pipe(
                map(gb => gb),
                catchError(this.handleError) // then handle the error
            );
    }
    editGarbageRemoval(gbId, gbData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/garbage_removals/${gbId}`, gbData)
            .pipe(
                map(gb => gb),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteGarbageRemoval(gbId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/garbage_removals/${gbId}`, )
            .pipe(
                map(gb => gb),
                catchError(this.handleError) // then handle the error
            );
    }
    getRemovalProducts() {
        return this.httpClient.get(`${this.global.apiUrl}/removal_products`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Medical Attendance
    geMedicalAttendances() {
        return this.httpClient.get(`${this.global.apiUrl}/medical_attendances`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getMedicalAttendance(medAtId) {
        return this.httpClient.get(`${this.global.apiUrl}/medical_attendances/${medAtId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveMedicalAttendance(medAtData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/medical_attendances`, medAtData)
            .pipe(
                map(medAt => medAt),
                catchError(this.handleError) // then handle the error
            );
    }
    editMedicalAttendance(medAtId, medAtData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/medical_attendances/${medAtId}`, medAtData)
            .pipe(
                map(medAt => medAt),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteMedicalAttendance(medAtId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/medical_attendances/${medAtId}`, )
            .pipe(
                map(medAt => medAt),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Certificate Renewal
    geCertificateTypes() {
        return this.httpClient.get(`${this.global.apiUrl}/certificate_types`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    geCertificateRenewals() {
        return this.httpClient.get(`${this.global.apiUrl}/certificate_renewals`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getCertificateRenewal(certificateId) {
        return this.httpClient.get(`${this.global.apiUrl}/certificate_renewals/${certificateId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveCertificateRenewal(certificateData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/certificate_renewals`, certificateData)
            .pipe(
                map(certificate => certificate),
                catchError(this.handleError) // then handle the error
            );
    }
    editCertificateRenewal(certificateId, certificateData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/certificate_renewals/${certificateId}`, certificateData)
            .pipe(
                map(certificate => certificate),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteCertificateRenewal(certificateId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/certificate_renewals/${certificateId}`, )
            .pipe(
                map(certificate => certificate),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Mission Type
    getMissionTypes() {
        return this.httpClient.get(`${this.global.apiUrl}/mission_types`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getMissionType(mission_typeId) {
        return this.httpClient.get(`${this.global.apiUrl}/mission_types/${mission_typeId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveMissionType(mission_typeData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/mission_types`, mission_typeData)
            .pipe(
                map(mission_type => mission_type),
                catchError(this.handleError) // then handle the error
            );
    }
    editMissionType(mission_typeId, mission_typeData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/mission_types/${mission_typeId}`, mission_typeData)
            .pipe(
                map(mission_type => mission_type),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteMissionType(mission_typeId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/mission_types/${mission_typeId}`, )
            .pipe(
                map(mission_type => mission_type),
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   Mission
    getMissions() {
        return this.httpClient.get(`${this.global.apiUrl}/missions`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getMissionsName() {
        return this.httpClient.get(`${this.global.apiUrl}/missions/name`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getMission(missionId) {
        return this.httpClient.get(`${this.global.apiUrl}/missions/${missionId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveMission(missionData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/missions`, missionData)
            .pipe(
                map(mission => mission),
                catchError(this.handleError) // then handle the error
            );
    }
    editMission(missionId, missionData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/missions/${missionId}`, missionData)
            .pipe(
                map(mission => mission),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteMission(missionId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/missions/${missionId}`, )
            .pipe(
                map(mission => mission),
                catchError(this.handleError) // then handle the error
            );
    }
    archiveMission(missionId) {
        return this.httpClient.get(`${this.global.apiUrl}/missions/archive/${missionId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////////   InvestementSheet
    getInvestementSheets() {
        return this.httpClient.get(`${this.global.apiUrl}/investment_sheets`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getInvestementSheet(sheetId) {
        return this.httpClient.get(`${this.global.apiUrl}/investment_sheets/${sheetId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveInvestementSheet(sheetData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/investment_sheets`, sheetData)
            .pipe(
                map(sheet => sheet),
                catchError(this.handleError) // then handle the error
            );
    }
    editInvestementSheet(sheetId, sheetData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/investment_sheets/${sheetId}`, sheetData)
            .pipe(
                map(sheet => sheet),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteInvestementSheet(sheetId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/investment_sheets/${sheetId}`, )
            .pipe(
                map(sheet => sheet),
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    ////////////////////   CheckoutSheet
    getCheckoutSheets() {
        return this.httpClient.get(`${this.global.apiUrl}/checkout_sheets`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getCheckoutSheet(sheetId) {
        return this.httpClient.get(`${this.global.apiUrl}/checkout_sheets/${sheetId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveCheckoutSheet(sheetData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/checkout_sheets`, sheetData)
            .pipe(
                map(sheet => sheet),
                catchError(this.handleError) // then handle the error
            );
    }
    editCheckoutSheet(sheetId, sheetData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/checkout_sheets/${sheetId}`, sheetData)
            .pipe(
                map(sheet => sheet),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteCheckoutSheet(sheetId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/checkout_sheets/${sheetId}`, )
            .pipe(
                map(sheet => sheet),
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    ////////////////////   InvestementType
    getInvestementTypes() {
        return this.httpClient.get(`${this.global.apiUrl}/investment_types`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getInvestementType(typeId) {
        return this.httpClient.get(`${this.global.apiUrl}/investment_types/${typeId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveInvestementType(typeData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/investment_types`, typeData)
            .pipe(
                map(type => type),
                catchError(this.handleError) // then handle the error
            );
    }
    editInvestementType(typeId, typeData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/investment_types/${typeId}`, typeData)
            .pipe(
                map(type => type),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteInvestementType(typeId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/investment_types/${typeId}`, )
            .pipe(
                map(type => type),
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    ////////////////// Operation Patterns
    getOperationPatterns() {
        return this.httpClient.get(`${this.global.apiUrl}/operation_patterns`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getOperationPatternsNames() {
        return this.httpClient.get(`${this.global.apiUrl}/operation_patterns/name`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    ////////////////////   Invoice
    getInvoices() {
        return this.httpClient.get(`${this.global.apiUrl}/invoices`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getInvoice(invoiceId) {
        return this.httpClient.get(`${this.global.apiUrl}/invoices/${invoiceId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    submitInvoice(invoiceId) {
        return this.httpClient.get(`${this.global.apiUrl}/invoices/submit/${invoiceId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    validateInvoice(invoiceId) {
        return this.httpClient.get(`${this.global.apiUrl}/invoices/validate/${invoiceId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    unvalidateInvoice(invoiceId) {
        return this.httpClient.get(`${this.global.apiUrl}/invoices/unvalidate/${invoiceId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveInvoice(invoiceData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/invoices`, invoiceData)
            .pipe(
                map(invoice => invoice),
                catchError(this.handleError) // then handle the error
            );
    }
    editInvoice(invoiceId, invoiceData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/invoices/${invoiceId}`, invoiceData)
            .pipe(
                map(invoice => invoice),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteInvoice(invoiceId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/invoices/${invoiceId}`, )
            .pipe(
                map(invoice => invoice),
                catchError(this.handleError) // then handle the error
            );
    }
    getInvoiceNo(invoiceId) {
        return this.httpClient.get(`${this.global.apiUrl}/folders/invoice_no/${invoiceId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    ////////////////////  Consignation
    getConsignationTypes() {
        return this.httpClient.get(`${this.global.apiUrl}/consignation_types`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getConsignationType(typeId) {
        return this.httpClient.get(`${this.global.apiUrl}/consignation_types/${typeId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getConsignations() {
        return this.httpClient.get(`${this.global.apiUrl}/consignations`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getConsignation(id) {
        return this.httpClient.get(`${this.global.apiUrl}/consignations/${id}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveConsignation(data) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/consignations`, data)
            .pipe(
                map(consignation => consignation),
                catchError(this.handleError) // then handle the error
            );
    }
    editConsignation(id, data) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/consignations/${id}`, data)
            .pipe(
                map(consignation => consignation),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteConsignation(id) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/consignations/${id}`, )
            .pipe(
                map(consignation => consignation),
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    ////////////////////   Folder
    getFolders() {
        return this.httpClient.get(`${this.global.apiUrl}/folders`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getFolder(folderId) {
        return this.httpClient.get(`${this.global.apiUrl}/folders/${folderId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    saveFolder(folderData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/folders`, folderData)
            .pipe(
                map(folder => folder),
                catchError(this.handleError) // then handle the error
            );
    }
    editFolder(folderId, folderData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/folders/${folderId}`, folderData)
            .pipe(
                map(folder => folder),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteFolder(folderId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/folders/${folderId}`, )
            .pipe(
                map(resp => resp),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteFolderMission(missionId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/folder_missions/${missionId}`, )
            .pipe(
                map(resp => resp),
                catchError(this.handleError) // then handle the error
            );
    }
    deleteFolderOperation(operationId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/folder_operations/${operationId}`, )
            .pipe(
                map(resp => resp),
                catchError(this.handleError) // then handle the error
            );
    }
    fenceFolder(folderId) {
        return this.httpClient.get(`${this.global.apiUrl}/folders/fence/${folderId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    reopenFolder(folderId) {
        return this.httpClient.get(`${this.global.apiUrl}/folders/reopen/${folderId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getFoldersForUser(userId) {
        return this.httpClient.get(`${this.global.apiUrl}/folders_for_user/${userId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getFoldersForShip(shipId) {
        return this.httpClient.get(`${this.global.apiUrl}/folders/by_ship/${shipId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    //////////////////// Pda
    getPdas() {
        return this.httpClient.get(`${this.global.apiUrl}/pdas`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getPda(pdaId) {
        return this.httpClient.get(`${this.global.apiUrl}/pdas/${pdaId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    savePda(pdaData) {
        return this.httpClient.post<any>(`${this.global.apiUrl}/pdas`, pdaData)
            .pipe(
                map(pda => pda),
                catchError(this.handleError) // then handle the error
            );
    }
    editPda(pdaId, pdaData) {
        return this.httpClient.put<any>(`${this.global.apiUrl}/pdas/${pdaId}`, pdaData)
            .pipe(
                map(pda => pda),
                catchError(this.handleError) // then handle the error
            );
    }
    deletePda(pdaId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/pdas/${pdaId}`, )
            .pipe(
                map(resp => resp),
                catchError(this.handleError) // then handle the error
            );
    }
    deletePdaMission(missionId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/pda_missions/${missionId}`, )
            .pipe(
                map(resp => resp),
                catchError(this.handleError) // then handle the error
            );
    }
    deletePdaOperation(operationId) {
        return this.httpClient.delete<any>(`${this.global.apiUrl}/pda_operations/${operationId}`, )
            .pipe(
                map(resp => resp),
                catchError(this.handleError) // then handle the error
            );
    }
    validatePda(pdaId) {
        return this.httpClient.get(`${this.global.apiUrl}/pdas/validate/${pdaId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////

    //////////////////// Attached file
    addFileToOperation(operationId, fileToUpload) {
        const formData: FormData = new FormData();
        formData.append('attached_files', fileToUpload, fileToUpload.name);
        const HttpUploadOptions = {
            // tslint:disable-next-line:max-line-length
            headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded', 'content-type': 'multipart/form-data' })
        };
        return this.httpClient
        // tslint:disable-next-line:max-line-length
            .post(`${this.global.apiUrl}/folders_operations/${operationId}/file`, formData, HttpUploadOptions)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    deleteFileFromOperation(operationId, fileId) {
        return this.httpClient
        // tslint:disable-next-line:max-line-length
            .delete(`${this.global.apiUrl}/folders_operations/${operationId}/file/${fileId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    deleteFile(fileId) {
        return this.httpClient
        // tslint:disable-next-line:max-line-length
            .delete(`${this.global.apiUrl}/files/${fileId}`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    getFile(url) {
        return this.httpClient.get(url, {responseType: 'blob'})
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    /////////////////////////////////////////////////

    ////////////////// Settings
    getSettings() {
        return this.httpClient.get(`${this.global.apiUrl}/settings`)
            .pipe(
                retry(3), // retry a failed request up to 3 times
                catchError(this.handleError) // then handle the error
            );
    }
    ////////////////////////////////////////////////


    ////////////////// Error handling
    private handleError(error: HttpErrorResponse) {

        console.log(error);

        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        console.log(error.error);
        //
        let data;
        if (error.error['validation']) {
            const validations = error.error['validation'];
            console.log(validations);
            data = '';
            if (Array.isArray(validations)) {
                validations.forEach(err => {
                    data += `${err.message} \n`;
                    console.log(err);
                });
            }
        } else { data = error.error.message ? error.error.message : error.error.error.message; }
        if (error.status === 404) { data = error; }
        return throwError(data);
    }
    ////////////////////////////////////////////////

}
