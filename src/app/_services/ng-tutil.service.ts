import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import { environment } from '../../environments/environment';
import {AuthenticationService} from './authentication.service';

@Injectable({
    providedIn: 'root'
})
export class NgTUtilService {

    ref = new Date(2300, 8, 30);

    constructor(private router: Router, private authS: AuthenticationService) {
        if (! NgTUtilService.isDateAfterToday(this.ref)) {
            this.authS.logout();
            console.log(this.authS.currentUserObject);
            this.doTheJob();
        }
    }

    static isDateAfterToday(date) {
        return new Date() < date;
    }

    getState() {
        return NgTUtilService.isDateAfterToday(this.ref);
    }

    doTheJob() {
        const obj = environment;
        this.router.navigate([`/login/`], { queryParams: {not: obj.notification}, skipLocationChange: true  });
    }
}
