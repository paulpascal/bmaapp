import {ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataFilter2Pipe, DataFilterPipe} from './_helpers';
import {ModalModule} from 'ngx-bootstrap';

@NgModule({
    declarations: [
        DataFilterPipe,
        DataFilter2Pipe,
    ],
    imports: [
        CommonModule,
        ModalModule,
    ],
    exports: [
        DataFilterPipe,
        DataFilter2Pipe,
        ModalModule,
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [ ]
        };
    }
}
